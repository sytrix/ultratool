package thecleaner;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import thecleaner.command.Butcher;
import thecleaner.command.Code;
import thecleaner.command.Compass;
import thecleaner.command.Effect;
import thecleaner.command.Exe;
import thecleaner.command.Inv;
import thecleaner.command.Item;
import thecleaner.command.Kit;
import thecleaner.command.List;
import thecleaner.command.Point;
import thecleaner.command.Send;
import thecleaner.command.Sound;
import thecleaner.command.Spawn;
import thecleaner.command.Task;
import thecleaner.command.Tool;
import thecleaner.command.UltraTool;
import thecleaner.command.Zone;
import thecleaner.file.DataManager;
import thecleaner.file.FileAlphaNum;
import thecleaner.file.FileMobSave;
import thecleaner.file.FileVarSave;
import thecleaner.fonction.PluginConfig;
import thecleaner.fonction.TaskExecutor;
import thecleaner.list.ListChar;
import thecleaner.list.ListCommand;
import thecleaner.list.ListEntityProperty;
import thecleaner.list.ListEntityXp;
import thecleaner.list.ListEvent;
import thecleaner.list.ListInv;
import thecleaner.list.ListKit;
import thecleaner.list.ListPlayerCuboid;
import thecleaner.list.ListPoint;
import thecleaner.list.ListTask;
import thecleaner.list.ListUniqueExe;
import thecleaner.list.ListZone;
import thecleaner.listener.EventDamage;
import thecleaner.listener.EventDeath;
import thecleaner.listener.EventEntityTarget;
import thecleaner.listener.EventSelect;
import thecleaner.listener.EventSpawnEntity;
import thecleaner.listener.EventTabComplete;
import thecleaner.listener.EventTool;
import thecleaner.listener.EventWorldLoad;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;
import thecleaner.script.Script;
import thecleaner.sender.PluginSender;

public class UltraToolMain extends JavaPlugin 
{
	private PluginConfig m_pluginConfig;
	private DataManager m_dataManager;
	private Script m_script;
	private TaskExecutor m_taskExecutor;
	private Language m_language;
	
	private ListChar m_listChar;
	private ListCommand m_listCommand;
	private ListEntityProperty m_listEntityProperty;
	private ListEntityXp m_listEntityXp;
	private ListEvent m_listEvent;
	private ListInv m_listInv;
	private ListKit m_listKit;
	private ListPlayerCuboid m_listPlayerCuboid;
	private ListPoint m_listPoint;
	private ListTask m_listTask;
	private ListUniqueExe m_listUniqueExe;
	private ListZone m_listZone;
	
	private EventDamage m_eventDamage;
	private EventDeath m_eventDeath;
	private EventEntityTarget m_eventEntityTarget;
	private EventSelect m_eventSelect;
	private EventSpawnEntity m_eventSpawnEntity;
	private EventTabComplete m_eventTabComplete;
	private EventTool m_eventTool;
	private EventWorldLoad m_eventWorldLoad;
	
	private Butcher m_cmdButcher;
	private Code m_cmdCode;
	private Compass m_cmdCompass;
	private Effect m_cmdEffect;
	private Exe m_cmdExe;
	private List m_cmdList;
	private Inv m_cmdInv;
	private Item m_cmdItem;
	private Kit m_cmdKit;
	private Send m_cmdSend;
	private Point m_cmdPoint;
	private Sound m_cmdSound;
	private Spawn m_cmdSpawn;
	private Task m_cmdTask;
	private Tool m_cmdTool;
	private UltraTool m_cmdUltraTool;
	private Zone m_cmdZone;
	
	public DataManager getDataManager()
	{
		return m_dataManager;
	}
	
	public Language getLanguage()
	{
		return m_language;
	}
	
	public Script getScript()
	{
		return m_script;
	}
	
	public TaskExecutor getTaskExecutor()
	{
		return m_taskExecutor;
	}
	
	public ListChar getListChar()
	{
		return m_listChar;
	}
	
	public ListCommand getListCommand()
	{
		return m_listCommand;
	}
	
	public ListEntityProperty getListEntityProperty()
	{
		return m_listEntityProperty;
	}
	
	public ListEntityXp getListEntityXp()
	{
		return m_listEntityXp;
	}
	
	public ListEvent getListEvent()
	{
		return m_listEvent;
	}
	
	public ListInv getListInv()
	{
		return m_listInv;
	}
	
	public ListKit getListKit()
	{
		return m_listKit;
	}
	
	public ListPlayerCuboid getListPlayerCuboid()
	{
		return m_listPlayerCuboid;
	}
	
	public ListPoint getListPoint()
	{
		return m_listPoint;
	}
	
	public ListTask getListTask()
	{
		return m_listTask;
	}
	
	public ListUniqueExe getListUniqueExe()
	{
		return m_listUniqueExe;
	}
	
	public ListZone getListZone()
	{
		return m_listZone;
	}
	
	public PluginConfig getPluginConfig()
	{
		return m_pluginConfig;
	}
	
	public void onEnable()
	{
		m_pluginConfig = new PluginConfig(this);
		m_dataManager = new DataManager(this);
		m_language = new Language(this);
		
		m_listChar = new ListChar();
		m_listCommand = new ListCommand(this);
		m_listEntityProperty = new ListEntityProperty(this);
		m_listEntityXp = new ListEntityXp();
		m_listEvent = new ListEvent(this);
		m_listInv = new ListInv(this);
		m_listKit = new ListKit(this);
		m_listPlayerCuboid = new ListPlayerCuboid();
		m_listPoint = new ListPoint(this);
		m_listTask = new ListTask(this);
		m_listUniqueExe = new ListUniqueExe();
		m_listZone = new ListZone(this);
		
		m_eventDamage = new EventDamage(this);
		m_eventDeath = new EventDeath(this);
		m_eventEntityTarget = new EventEntityTarget(this);
		m_eventSelect = new EventSelect(this);
		m_eventSpawnEntity = new EventSpawnEntity(this);
		m_eventTabComplete = new EventTabComplete(this);
		m_eventTool = new EventTool(this);
		m_eventWorldLoad = new EventWorldLoad(this);
		
		m_script = new Script(this);
		
		m_cmdButcher = new Butcher(this);
		m_cmdCode = new Code(this);
		m_cmdCompass = new Compass(this);
		m_cmdEffect = new Effect(this);
		m_cmdExe = new Exe(this);
		m_cmdInv = new Inv(this);
		m_cmdItem = new Item(this);
		m_cmdKit = new Kit(this);
		m_cmdList = new List(this);
		m_cmdSend = new Send(this);
		m_cmdPoint = new Point(this);
		m_cmdSound = new Sound(this);
		m_cmdSpawn = new Spawn(this);
		m_cmdTask = new Task(this);
		m_cmdTool = new Tool(this);
		m_cmdUltraTool = new UltraTool(this);
		m_cmdZone = new Zone(this);
		
		m_taskExecutor = new TaskExecutor(this);
		
		this.getServer().getPluginManager().registerEvents(m_eventDamage, this);
		this.getServer().getPluginManager().registerEvents(m_eventDeath, this);
		this.getServer().getPluginManager().registerEvents(m_eventEntityTarget, this);
		this.getServer().getPluginManager().registerEvents(m_eventSelect, this);
		this.getServer().getPluginManager().registerEvents(m_eventSpawnEntity, this);
		this.getServer().getPluginManager().registerEvents(m_eventTabComplete, this);
		this.getServer().getPluginManager().registerEvents(m_eventTool, this);
		this.getServer().getPluginManager().registerEvents(m_eventWorldLoad, this);
		
		this.getCommand("ubutcher").setExecutor(m_cmdButcher);
		this.getCommand("ucode").setExecutor(m_cmdCode);
		this.getCommand("ucompass").setExecutor(m_cmdCompass);
		this.getCommand("ueffect").setExecutor(m_cmdEffect);
		this.getCommand("uexe").setExecutor(m_cmdExe);
		this.getCommand("uinv").setExecutor(m_cmdInv);
		this.getCommand("uitem").setExecutor(m_cmdItem);
		this.getCommand("ukit").setExecutor(m_cmdKit);
		this.getCommand("ulist").setExecutor(m_cmdList);
		this.getCommand("usend").setExecutor(m_cmdSend);
		this.getCommand("upoint").setExecutor(m_cmdPoint);
		this.getCommand("usound").setExecutor(m_cmdSound);
		this.getCommand("uspawn").setExecutor(m_cmdSpawn);
		this.getCommand("utask").setExecutor(m_cmdTask);
		this.getCommand("utool").setExecutor(m_cmdTool);
		this.getCommand("ultratool").setExecutor(m_cmdUltraTool);
		this.getCommand("uzone").setExecutor(m_cmdZone);
		
		PluginSender.setPlugin(this);
		
		FileAlphaNum.load(this);
		FileMobSave.load(this);
		FileVarSave.load(this);
		
		Collection<? extends Player> players = this.getServer().getOnlinePlayers();
		String time = (new SimpleDateFormat("HH:mm:ss")).format(new Date());
		for(Player p : players) {
			if(p.isOp()) {
				p.sendMessage(ChatMessage.info("[" + time + "] " + this.getName() + " reload"));
			}
		}
	}
	
	public void onDisable()
	{
		try
		{
			FileMobSave.save(this);
			FileVarSave.save(this);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		try
		{
			FileMobSave.save(this);
			FileVarSave.save(this);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}


