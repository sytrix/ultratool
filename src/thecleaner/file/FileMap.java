package thecleaner.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

import thecleaner.UltraToolMain;
import thecleaner.worldedit.Cuboid;

public class FileMap 
{
	@SuppressWarnings("deprecation")
	public static boolean loadCuboid(UltraToolMain plugin, String key, Cuboid cuboid)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getMapFolder() + key.toLowerCase() + ".pmap";
		
		try 
		{
			FileInputStream fis = new FileInputStream(filePath);
			World world = cuboid.getWorld();
			int posX = cuboid.getPosX();
			int posY = cuboid.getPosY();
			int posZ = cuboid.getPosZ();
			int sizeX = cuboid.getSizeX();
			int sizeY = cuboid.getSizeY();
			int sizeZ = cuboid.getSizeZ();
			
			byte[] buf = new byte[5];
			int id = 0;
			
	        while (fis.read(buf) >= 0) 
	        {
	        	int idBlock = 0;
				byte data = buf[4];
				
				for(int i = 3; i >= 0; i--)
				{
					idBlock *= 256;
					idBlock += buf[i];
				}
				
				int idY = id / sizeX;
				int idZ = idY / sizeY;
				
				Block block = world.getBlockAt(posX + id % sizeX, posY + idY % sizeY, posZ + idZ % sizeZ);
				Material mat = Material.getMaterial(idBlock);
				
				
				if(mat != null)
				{
					if(!block.getType().equals(mat))
						block.setType(mat);
				}
				
				if(block.getData() != data)
					block.setData(data);
				
				buf = new byte[5];
				id++;
	        }
			
			fis.close(); 
		} 
		catch (IOException e) {e.printStackTrace(); return false;}
	
		return true;
	}
	
	@SuppressWarnings("deprecation")
	public static boolean saveCuboid(UltraToolMain plugin, String key, Cuboid cuboid)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getMapFolder() + key.toLowerCase() + ".pmap";
		
		try 
		{
			FileOutputStream fos = new FileOutputStream(filePath);
			World world = cuboid.getWorld();
			int posX = cuboid.getPosX();
			int posY = cuboid.getPosY();
			int posZ = cuboid.getPosZ();
			int sizeX = cuboid.getSizeX();
			int sizeY = cuboid.getSizeY();
			int sizeZ = cuboid.getSizeZ();
			
			byte[] buf = new byte[5];
			
			for(int z = 0; z < sizeZ; z++)
			{
				for(int y = 0; y < sizeY; y++)
				{
					for(int x = 0; x < sizeX; x++)
					{
						Block block = world.getBlockAt(posX + x, posY + y, posZ + z);
						int idBlock = block.getType().getId();
						byte data = block.getData();
						
						for(int i = 0; i < 4; i++)
						{
							buf[i] = (byte)(idBlock % 256);
							idBlock /= 256;
						}
						
						buf[4] = data;
						
						fos.write(buf);
						
						buf = new byte[5];
					}
				}
			}
			
			fos.close(); 
		} 
		catch (IOException e) {e.printStackTrace(); return false;}
		
		return true;
	}
}
