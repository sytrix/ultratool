package thecleaner.file;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

import thecleaner.UltraToolMain;
import thecleaner.worldedit.Cuboid;

public class FileCuboid 
{
	static private String fileName = "cuboid.yml";
	
	public static Map<String, Cuboid> Load(UltraToolMain plugin)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlCreateIfNotExist(filePath);
		
		Map<String, Cuboid> mapCuboid = new Hashtable<String, Cuboid>();
		
		Set<String> pointKey = fileConfig.getKeys(false);
		Iterator<String> iteratorKeys = pointKey.iterator();
		
		while(iteratorKeys.hasNext())
		{
			String keySelect = iteratorKeys.next();
			String nameWorld = fileConfig.getString(keySelect + ".w");
			Integer pos1x = fileConfig.getInt(keySelect + ".1.x");
			Integer pos1y = fileConfig.getInt(keySelect + ".1.y");
			Integer pos1z = fileConfig.getInt(keySelect + ".1.z");
			Integer pos2x = fileConfig.getInt(keySelect + ".2.x");
			Integer pos2y = fileConfig.getInt(keySelect + ".2.y");
			Integer pos2z = fileConfig.getInt(keySelect + ".2.z");
			
			World world = plugin.getServer().getWorld(nameWorld);
			if(world != null)
			{
				Cuboid cuboid = new Cuboid(world, pos1x, pos1y, pos1z, pos2x, pos2y, pos2z);
				mapCuboid.put(keySelect, cuboid);
			}
		}	
		
		return mapCuboid;
	}
	
	public static boolean saveNewCuboid(UltraToolMain plugin, String key, Cuboid cuboid)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlCreateIfNotExist(filePath);
		
		fileConfig.createSection(key);
		
		Location pos1 = cuboid.getPosition1();
		Location pos2 = cuboid.getPosition2();
		fileConfig.set(key + ".w", pos1.getWorld().getName());
		fileConfig.set(key + ".1.x", pos1.getX());
		fileConfig.set(key + ".1.y", pos1.getY());
		fileConfig.set(key + ".1.z", pos1.getZ());
		fileConfig.set(key + ".2.x", pos2.getX());
		fileConfig.set(key + ".2.y", pos2.getY());
		fileConfig.set(key + ".2.z", pos2.getZ());
		
		dataManager.saveYaml(fileConfig, filePath);
		
		return true;
		
	}
	
	public static boolean saveCuboid(UltraToolMain plugin, String key, Cuboid cuboid)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlCreateIfNotExist(filePath);
		
		Location pos1 = cuboid.getPosition1();
		Location pos2 = cuboid.getPosition2();
		fileConfig.set(key + ".w", pos1.getWorld().getName());
		fileConfig.set(key + ".1.x", pos1.getX());
		fileConfig.set(key + ".1.y", pos1.getY());
		fileConfig.set(key + ".1.z", pos1.getZ());
		fileConfig.set(key + ".2.x", pos2.getX());
		fileConfig.set(key + ".2.y", pos2.getY());
		fileConfig.set(key + ".2.z", pos2.getZ());
		
		dataManager.saveYaml(fileConfig, filePath);
		
		return true;
	}
	
	public static boolean delPoint(UltraToolMain plugin, Map<String, Cuboid> mapCuboid)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlClear(filePath);
		
		Set<Entry<String, Cuboid>> setCuboid = mapCuboid.entrySet();
		Iterator<Entry<String, Cuboid>> iterator = setCuboid.iterator();
		
		while(iterator.hasNext())
		{
			Entry<String, Cuboid> entry = iterator.next();
			
			String key = entry.getKey();
			Cuboid cuboid = entry.getValue();
			
			fileConfig.createSection(key);
			
			Location pos1 = cuboid.getPosition1();
			Location pos2 = cuboid.getPosition2();
			fileConfig.set(key + ".w", pos1.getWorld().getName());
			fileConfig.set(key + ".1.x", pos1.getX());
			fileConfig.set(key + ".1.y", pos1.getY());
			fileConfig.set(key + ".1.z", pos1.getZ());
			fileConfig.set(key + ".2.x", pos2.getX());
			fileConfig.set(key + ".2.y", pos2.getY());
			fileConfig.set(key + ".2.z", pos2.getZ());
		}
		
		dataManager.saveYaml(fileConfig, filePath);
		
		return true;
		
	}
}
