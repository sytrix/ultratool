package thecleaner.file;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import thecleaner.UltraToolMain;

public class DataManager 
{
	private UltraToolMain m_plugin;
	
	public DataManager(UltraToolMain plugin)
	{
		m_plugin = plugin;
		
		createDirectoryIfNotExist(this.getRootFolder());
		createDirectoryIfNotExist(this.getSaveFolder());
		createDirectoryIfNotExist(this.getBackupFolder());
		createDirectoryIfNotExist(this.getMapFolder());
		createDirectoryIfNotExist(this.getLanguageFolder());
	}
	
	public FileConfiguration getYamlClear(String path)
	{
		File fichier = new File(path);
		
		if(fichier.exists())
			fichier.delete();
		
		try {fichier.createNewFile();}
		catch (IOException e1) {e1.printStackTrace();}
		
		return YamlConfiguration.loadConfiguration(fichier);
	}
	
	public FileConfiguration getYamlCreateIfNotExist(String path)
	{
		File fichier = new File(path);
		
		if(!fichier.exists())
		{
			try {fichier.createNewFile();}
			catch (IOException e1) {e1.printStackTrace();}
		}
		
		return YamlConfiguration.loadConfiguration(fichier);
	}
	
	public FileConfiguration getYamlFromPlugin(String path)
	{
		File fichier_config = new File(path);
		
		if(!fichier_config.exists())
		{
			m_plugin.saveResource(fichier_config.getName(), false);
		}
		
		return YamlConfiguration.loadConfiguration(fichier_config);
	}
	
	public boolean saveYaml(FileConfiguration config, String filePath)
	{
		try 
		{
			config.save(filePath);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private void createDirectoryIfNotExist(String directory)
	{
		File dossier_config = new File(directory);
		
		if(!dossier_config.exists())
		{
			dossier_config.mkdir();
		}
	}
	
	public String getRootFolder()
	{
		return m_plugin.getDataFolder().toString() + "/";
	}
	
	public String getSaveFolder()
	{
		return m_plugin.getDataFolder().toString() + "/save/";
	}
	
	public String getBackupFolder()
	{
		return m_plugin.getDataFolder().toString() + "/backup/";
	}
	
	public String getMapFolder()
	{
		return m_plugin.getDataFolder().toString() + "/map/";
	}
	
	public String getLanguageFolder()
	{
		return m_plugin.getDataFolder().toString() + "/lang/";
	}
}
