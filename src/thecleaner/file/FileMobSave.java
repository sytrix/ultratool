package thecleaner.file;

import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;

import thecleaner.UltraToolMain;
import thecleaner.list.ListEntityProperty;

public class FileMobSave 
{
	static private String fileName = "entity_attrib.yml";
	
	public static boolean load(UltraToolMain plugin)
	{
		
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getSaveFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlCreateIfNotExist(filePath);
		ListEntityProperty listEntityProperty = plugin.getListEntityProperty();
		
		List<String> listMob = fileConfig.getStringList("properties");
		
		
		listEntityProperty.loadLines(listMob);
		
		return true;
	}
	
	public static boolean save(UltraToolMain plugin)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getSaveFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlClear(filePath);
		ListEntityProperty listEntityProperty = plugin.getListEntityProperty();
		
		fileConfig.set("properties", listEntityProperty.getLinesToSave());
		
		
		dataManager.saveYaml(fileConfig, filePath);
		
		
		return true;
	}
}
