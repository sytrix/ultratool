package thecleaner.file;

import thecleaner.UltraToolMain;
import thecleaner.fonction.PluginConfig;

public class FileConfig 
{
	public static void load(UltraToolMain plugin, PluginConfig pluginConfig)
	{
		plugin.getConfig().options().copyDefaults(true);
		plugin.saveConfig();
		
		String language = plugin.getConfig().getString("language");
		Boolean lootDefault = plugin.getConfig().getBoolean("loot.default");
		Boolean lootKit = plugin.getConfig().getBoolean("loot.kit");
		Boolean lootXp = plugin.getConfig().getBoolean("loot.xp");
		Integer spawnQuantityMax = plugin.getConfig().getInt("spawn.quantity-max");
		Boolean spawnPlayerTeleport = plugin.getConfig().getBoolean("spawn.player-teleport");
		Boolean spawnEntityTeleport = plugin.getConfig().getBoolean("spawn.entity-teleport");
		Integer zoneToolId = plugin.getConfig().getInt("zone.tool");
		Integer toolEffectRadiusMax = plugin.getConfig().getInt("tool.effect-radius-max");
		
		if(language != null)
			pluginConfig.setLanguage(language);
		if(lootDefault != null)
			pluginConfig.setLootDefault(lootDefault);
		if(lootKit != null)
			pluginConfig.setLootKit(lootKit);
		if(lootXp != null)
			pluginConfig.setLootXp(lootXp);
		if(spawnQuantityMax != null)
			pluginConfig.setSpawnQuantityMax(spawnQuantityMax);
		if(spawnPlayerTeleport != null)
			pluginConfig.setSpawnPlayerTeleport(spawnPlayerTeleport);
		if(spawnEntityTeleport != null)
			pluginConfig.setSpawnEntityTeleport(spawnEntityTeleport);
		if(zoneToolId != null)
			pluginConfig.setZoneToolId(zoneToolId);
		if(toolEffectRadiusMax != null)
			pluginConfig.setToolEffectRadiusMax(toolEffectRadiusMax);
		
	}
}
