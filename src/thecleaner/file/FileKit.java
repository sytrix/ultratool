package thecleaner.file;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import thecleaner.UltraToolMain;

public class FileKit 
{
	static private String fileName = "kit.yml";
	
	public static Map<String, ItemStack[]> Load(UltraToolMain plugin)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlCreateIfNotExist(filePath);
		
		Map<String, ItemStack[]> mapEquipment = new Hashtable<String, ItemStack[]>();
		
		Set<String> kitKey = fileConfig.getKeys(false);
		Iterator<String> iteratorKeys = kitKey.iterator();
		
		while(iteratorKeys.hasNext())
		{
			String keySelect = iteratorKeys.next();
			ItemStack[] equipment = 
			{
				new ItemStack(Material.AIR), 
				new ItemStack(Material.AIR), 
				new ItemStack(Material.AIR), 
				new ItemStack(Material.AIR), 
				new ItemStack(Material.AIR),
				new ItemStack(Material.AIR)
			};
			
			equipment[0] = fileConfig.getItemStack(keySelect + ".h");
			equipment[1] = fileConfig.getItemStack(keySelect + ".c");
			equipment[2] = fileConfig.getItemStack(keySelect + ".l");
			equipment[3] = fileConfig.getItemStack(keySelect + ".b");
			equipment[4] = fileConfig.getItemStack(keySelect + ".m");
			equipment[5] = fileConfig.getItemStack(keySelect + ".o");
			
			mapEquipment.put(keySelect, equipment);
		}
		
		return mapEquipment;
	}
	
	public static boolean saveNewEquipment(UltraToolMain plugin, String key, ItemStack[] equipment)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlCreateIfNotExist(filePath);
		
		fileConfig.createSection(key);
		
		fileConfig.set(key + ".h", equipment[0]);
		fileConfig.set(key + ".c", equipment[1]);
		fileConfig.set(key + ".l", equipment[2]);
		fileConfig.set(key + ".b", equipment[3]);
		fileConfig.set(key + ".m", equipment[4]);
		fileConfig.set(key + ".o", equipment[5]);
		
		dataManager.saveYaml(fileConfig, filePath);
		
		return true;
		
	}
	
	public static boolean saveEquipment(UltraToolMain plugin, String key, ItemStack[] equipment)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlCreateIfNotExist(filePath);
		
		fileConfig.set(key + ".h", equipment[0]);
		fileConfig.set(key + ".c", equipment[1]);
		fileConfig.set(key + ".l", equipment[2]);
		fileConfig.set(key + ".b", equipment[3]);
		fileConfig.set(key + ".m", equipment[4]);
		fileConfig.set(key + ".o", equipment[5]);
		
		dataManager.saveYaml(fileConfig, filePath);
		
		return true;
	}
	
	public static boolean delEquipment(UltraToolMain plugin, Map<String, ItemStack[]> mapEquipment)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlClear(filePath);
		
		Set<Entry<String, ItemStack[]>> setKit = mapEquipment.entrySet();
		Iterator<Entry<String, ItemStack[]>> iterator = setKit.iterator();
		
		while(iterator.hasNext())
		{
			Entry<String, ItemStack[]> entry = iterator.next();
			
			String key = entry.getKey();
			ItemStack[] equipment = entry.getValue();
			
			fileConfig.createSection(key);
			
			fileConfig.set(key + ".h", equipment[0]);
			fileConfig.set(key + ".c", equipment[1]);
			fileConfig.set(key + ".l", equipment[2]);
			fileConfig.set(key + ".b", equipment[3]);
			fileConfig.set(key + ".m", equipment[4]);
			fileConfig.set(key + ".o", equipment[5]);
		}
		
		dataManager.saveYaml(fileConfig, filePath);
		
		return true;
		
	}
}
