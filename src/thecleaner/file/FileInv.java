package thecleaner.file;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import thecleaner.UltraToolMain;

public class FileInv 
{
	static private String fileName = "inventory.yml";
	
	public static Map<String, ItemStack[]> Load(UltraToolMain plugin)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlCreateIfNotExist(filePath);
		
		Map<String, ItemStack[]> mapEquipment = new Hashtable<String, ItemStack[]>();
		
		Set<String> invKey = fileConfig.getKeys(false);
		Iterator<String> iteratorKeys = invKey.iterator();
		
		while(iteratorKeys.hasNext())
		{
			String keySelect = iteratorKeys.next();
			Integer numberItem = fileConfig.getInt(keySelect + ".lenght");
			
			if(numberItem != null)
			{
				ItemStack[] inventory = new ItemStack[numberItem];
				
				for(int i = 0; i < numberItem; i++)
				{
					inventory[i] = new ItemStack(Material.AIR);
					inventory[i] = fileConfig.getItemStack(keySelect + "." + i);
				}
				
				mapEquipment.put(keySelect, inventory);
			}
		}
		
		return mapEquipment;
	}
	
	public static boolean saveNewEquipment(UltraToolMain plugin, String key, ItemStack[] inv)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlCreateIfNotExist(filePath);
		
		fileConfig.createSection(key);
		
		fileConfig.set(key + ".lenght", inv.length);
		for(int i = 0; i < inv.length; i++)
		{
			fileConfig.set(key + "." + i, inv[i]);
		}
		
		dataManager.saveYaml(fileConfig, filePath);
		
		return true;
		
	}
	
	public static boolean saveEquipment(UltraToolMain plugin, String key, ItemStack[] inv)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlCreateIfNotExist(filePath);
		
		fileConfig.set(key + ".lenght", inv.length);
		for(int i = 0; i < inv.length; i++)
		{
			fileConfig.set(key + "." + i, inv[i]);
		}
		
		dataManager.saveYaml(fileConfig, filePath);
		
		return true;
	}
	
	public static boolean delEquipment(UltraToolMain plugin, Map<String, ItemStack[]> mapInv)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlClear(filePath);
		
		Set<Entry<String, ItemStack[]>> setInv = mapInv.entrySet();
		Iterator<Entry<String, ItemStack[]>> iterator = setInv.iterator();
		
		while(iterator.hasNext())
		{
			Entry<String, ItemStack[]> entry = iterator.next();
			
			String key = entry.getKey();
			ItemStack[] inv = entry.getValue();
			
			fileConfig.createSection(key);
			
			fileConfig.set(key + ".lenght", inv.length);
			for(int i = 0; i < inv.length; i++)
			{
				fileConfig.set(key + "." + i, inv[i]);
			}
		}
		
		dataManager.saveYaml(fileConfig, filePath);
		
		return true;
		
	}
}
