package thecleaner.file;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

import thecleaner.UltraToolMain;

public class FilePoint 
{
	static private String fileName = "point.yml";
	
	public static Map<String, Location> Load(UltraToolMain plugin)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlCreateIfNotExist(filePath);
		
		Map<String, Location> mapPoint = new Hashtable<String, Location>();
		
		Iterator<String> iteratorKeys = fileConfig.getKeys(false).iterator();
		
		while(iteratorKeys.hasNext())
		{
			String keySelect = iteratorKeys.next();
			String nameWorld = fileConfig.getString(keySelect + ".w");
			double posx = fileConfig.getDouble(keySelect + ".x");
			double posy = fileConfig.getDouble(keySelect + ".y");
			double posz = fileConfig.getDouble(keySelect + ".z");
			float yaw = (float) fileConfig.getDouble(keySelect + ".d");
			float pitch = (float) fileConfig.getDouble(keySelect + ".p");
			
			World world = plugin.getServer().getWorld(nameWorld);
			
			if(world != null)
			{
				mapPoint.put(keySelect, new Location(world, posx, posy, posz, yaw, pitch));
			}
		}	
		
		return mapPoint;
	}
	
	public static boolean saveNewPoint(UltraToolMain plugin, String key, Location location)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlCreateIfNotExist(filePath);
		
		fileConfig.createSection(key);
		
		fileConfig.set(key + ".w", location.getWorld().getName());
		fileConfig.set(key + ".x", location.getX());
		fileConfig.set(key + ".y", location.getY());
		fileConfig.set(key + ".z", location.getZ());
		fileConfig.set(key + ".d", location.getYaw());
		fileConfig.set(key + ".p", location.getPitch());
		
		dataManager.saveYaml(fileConfig, filePath);
		
		return true;
		
	}
	
	public static boolean savePoint(UltraToolMain plugin, String key, Location location)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlCreateIfNotExist(filePath);
		
		fileConfig.set(key + ".w", location.getWorld().getName());
		fileConfig.set(key + ".x", location.getX());
		fileConfig.set(key + ".y", location.getY());
		fileConfig.set(key + ".z", location.getZ());
		fileConfig.set(key + ".d", location.getYaw());
		fileConfig.set(key + ".p", location.getPitch());
		
		dataManager.saveYaml(fileConfig, filePath);
		
		return true;
	}
	
	public static boolean delPoint(UltraToolMain plugin, Map<String, Location> mapPoint)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlClear(filePath);
		
		Iterator<Entry<String, Location>> iterator = mapPoint.entrySet().iterator();
		
		while(iterator.hasNext())
		{
			Entry<String, Location> entry = iterator.next();
			
			String key = entry.getKey();
			Location location = entry.getValue();
			
			fileConfig.createSection(key);
			
			fileConfig.set(key + ".w", location.getWorld().getName());
			fileConfig.set(key + ".x", location.getX());
			fileConfig.set(key + ".y", location.getY());
			fileConfig.set(key + ".z", location.getZ());
			fileConfig.set(key + ".d", location.getYaw());
			fileConfig.set(key + ".p", location.getPitch());
		}
		
		dataManager.saveYaml(fileConfig, filePath);
		
		return true;
		
	}
}
