package thecleaner.file;

import java.util.Iterator;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

import thecleaner.UltraToolMain;
import thecleaner.list.ListTask;

public class FileTask
{
	static private String fileName = "task.yml";
	
	public static boolean load(UltraToolMain plugin, ListTask listTask)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlCreateIfNotExist(filePath);
		
		Iterator<String> iteratorKeys = fileConfig.getKeys(false).iterator();
		
		while(iteratorKeys.hasNext())
		{
			String keySelect = iteratorKeys.next();
			Integer time = fileConfig.getInt(keySelect + ".time");
			List<String> listEventName = fileConfig.getStringList(keySelect + ".event");
			String command = fileConfig.getString(keySelect + ".command");
			
			String nameWorld = fileConfig.getString(keySelect + ".location.w");
			double posx = fileConfig.getDouble(keySelect + ".location.x");
			double posy = fileConfig.getDouble(keySelect + ".location.y");
			double posz = fileConfig.getDouble(keySelect + ".location.z");
			float yaw = (float) fileConfig.getDouble(keySelect + ".location.d");
			float pitch = (float) fileConfig.getDouble(keySelect + ".location.p");
			
			World world = plugin.getServer().getWorld(nameWorld);
			
			if(world != null)
			{
				Location location = new Location(world, posx, posy, posz, yaw, pitch);
				listTask.put(keySelect, location, command);
				
				if(time != null && time > 0)
					listTask.setRepeat(keySelect, time);
				
				if(listEventName != null)
				{
					for(int i = 0; i < listEventName.size(); i++)
					{
						listTask.addEvent(keySelect, listEventName.get(i));
						plugin.getListEvent().enable(listEventName.get(i));
					}
				}
			}
		}
		
		return true;
	}
	
	public static boolean save(UltraToolMain plugin)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + fileName;
		FileConfiguration fileConfig = dataManager.getYamlClear(filePath);
		
		ListTask listTask = plugin.getListTask();
		Iterator<String> iterator = listTask.iterator();
		
		while(iterator.hasNext())
		{
			String key = iterator.next();
			
			Location location = listTask.getLocation(key);
			List<String> listEventName = listTask.getEvents(key);
			Integer tick = listTask.getTick(key);
			
			if(tick != null && tick > 0)
				fileConfig.set(key + ".time", tick);
			if(listEventName != null && listEventName.size() > 0)
				fileConfig.set(key + ".event", listEventName);
			
			fileConfig.set(key + ".command", listTask.getCommand(key));
			fileConfig.set(key + ".location.w", location.getWorld().getName());
			fileConfig.set(key + ".location.x", location.getX());
			fileConfig.set(key + ".location.y", location.getY());
			fileConfig.set(key + ".location.z", location.getZ());
			fileConfig.set(key + ".location.d", location.getYaw());
			fileConfig.set(key + ".location.p", location.getPitch());
		}
		
		dataManager.saveYaml(fileConfig, filePath);
		
		return true;
	}
}
