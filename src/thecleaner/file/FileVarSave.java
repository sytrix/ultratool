package thecleaner.file;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import thecleaner.UltraToolMain;
import thecleaner.script.Script;

public class FileVarSave 
{
	static private String fileName = "ucode.script";
	
	public static boolean load(UltraToolMain plugin)
	{
		Script script = plugin.getScript(); 
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getSaveFolder() + fileName;
		
		File f = new File(filePath);
		if(f.exists()) { 
			try {
				byte[] encoded = Files.readAllBytes(Paths.get(filePath));
				String str = new String(encoded, StandardCharsets.UTF_8);
				
				try {
					script.execute(str);
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
				
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}
	
		return true;
	}
	
	public static boolean save(UltraToolMain plugin)
	{
		Script script = plugin.getScript();
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getSaveFolder() + fileName;
		
		String code = script.getMemory().toScript();
		
		try {
			byte[] encoded = code.getBytes(StandardCharsets.UTF_8);
			Files.write(Paths.get(filePath), encoded);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
}
