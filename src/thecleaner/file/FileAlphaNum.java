package thecleaner.file;

import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;

import thecleaner.UltraToolMain;
import thecleaner.list.ListChar;

public class FileAlphaNum 
{
	public static boolean load(UltraToolMain plugin)
	{
		DataManager dataManager = plugin.getDataManager();
		String filePath = dataManager.getRootFolder() + "alpha-num.yml";
		FileConfiguration alphaNumConfig = dataManager.getYamlFromPlugin(filePath);
		
		Integer char_width = alphaNumConfig.getInt("char_size.width");
		Integer char_height = alphaNumConfig.getInt("char_size.height");
		
		List<String> alpha = alphaNumConfig.getStringList("alpha");
		List<String> number = alphaNumConfig.getStringList("number");
		String listCharSpecial = alphaNumConfig.getString("special.list");
		List<String> charSpecial = alphaNumConfig.getStringList("special.code");
		
		ListChar listChar = plugin.getListChar();
		listChar.setSize(char_width, char_height);
		listChar.setAlpha(alpha);
		listChar.setNumber(number);
		listChar.setCharSpecial(charSpecial, listCharSpecial);
		
		return true;
		
	}
}
