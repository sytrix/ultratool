package thecleaner.script;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import sytrix.parser.Program;
import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptFunctionRunnable;
import sytrix.parser.langages.script.ScriptMemory;
import sytrix.parser.langages.script.ScriptParser;
import thecleaner.UltraToolMain;
import thecleaner.file.FileVarSave;

public class Script {
	
	static private UltraToolMain m_plugin = null;
	static private ScriptParser m_parser = null;
	static private ScriptMemory m_memory = null;
	static private ScriptFunction m_functions = null; 
	static private Map<String, Object> m_bundle = null;
	
	static public ScriptMemory getVariables() {
		return m_memory;
	}
	
	static public ScriptFunction getFunctions() {
		return m_functions;
	}
	
	public Script(UltraToolMain plugin) {
		if(m_plugin == null) {
			m_plugin = plugin;
		}
		if(m_parser == null) {
			m_parser = new ScriptParser();
		}
		if(m_memory == null) {
			m_memory = new ScriptMemory(null);
		}
		if(m_functions == null) {
			m_functions = new ScriptFunction();
			
			m_functions.add(new sytrix.parser.langages.script.ScriptFunctionConfig(
				"save",
				new Class<?>[] {},
				new ScriptFunctionRunnable() {
					@Override
					public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
						return (FileVarSave.save(m_plugin)) ? new Integer(1) : new Integer(0);
					}
				}));
			m_functions.add(new sytrix.parser.langages.script.ScriptFunctionConfig(
				"load",
				new Class<?>[] {},
				new ScriptFunctionRunnable() {
					@Override
					public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
						return (FileVarSave.load(m_plugin)) ? new Integer(1) : new Integer(0);
					}
				}));
		}
		
		if(m_bundle == null) {
			m_bundle = new HashMap<>();
			m_bundle.put("memory", m_memory);
			m_bundle.put("functions", m_functions);
		}
	}
	
	public String replace(String line)
	{
		boolean find = false;
		String code = "";
		String res = "";
		
		for(int i = 0; i < line.length(); i++)
		{
			if(line.charAt(i) == '$')
			{
				find = !find;
				
				if(!find)
				{
					Object obj = null;
					
					try {
						Program program = m_parser.compile(code);
						if(program != null) {
							obj = program.execute(m_bundle);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						System.err.println("Compilation error with this code : " + code);
						e.printStackTrace();
						return "ERROR";
					}
					
					code = "";
					
					if(obj != null)
						res += obj.toString();
				}
			}
			else if(find)
			{
				code += line.charAt(i);
			}
			else
			{
				res += line.charAt(i);
			}
		}
		
		return res;
	}
	
	public String[] replace(String arg[])
	{
		String res = "";
		
		for(int i = 0; i < arg.length; i++)
			res += arg[i] + " ";
			
		return this.replace(res).split(" ");
	}
	
	/*
	public void putListVar(Map<String, Object> listVar) {
		m_memory = null;
	}

	public Map<String, Object> getListVar() {
		return m_memory;
	}
	*/
	
	public ScriptMemory getMemory() {
		return m_memory;
	}

	public Object execute(String code) throws Exception {
		Program program = m_parser.compile(code);
		if(program != null) {
			 return program.execute(m_bundle);
		}
		return null;
	}
	
}
