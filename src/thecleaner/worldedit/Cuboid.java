package thecleaner.worldedit;

import org.bukkit.Location;
import org.bukkit.World;

public class Cuboid 
{
	private Location m_pos1;
	private Location m_pos2;
	
	public Cuboid()
	{
		m_pos1 = null;
		m_pos2 = null;
	}
	
	public Cuboid(Location pos, Location size)
	{
		m_pos1 = pos;
		m_pos2 = size;
	}
	
	public Cuboid(World world, int posx, int posy, int posz, int sizex, int sizey, int sizez)
	{
		m_pos1 = new Location(world, posx, posy, posz);
		m_pos2 = new Location(world, sizex, sizey, sizez);
	}
	
	public Cuboid clone()
	{
		if(this.isValid())
			return new Cuboid(m_pos1.clone(), m_pos2.clone());
		
		return null;
	}
	
	public Cuboid move(int x, int y, int z)
	{
		if(this.isValid())
			return new Cuboid(this.getPosition1().add(x, y, z), this.getPosition2().add(x, y, z));
		
		return null;
	}
	
	public Location getPosition1()
	{
		if(this.isValid())
			return m_pos1.clone();
		
		return null;
	}
	
	public Location getPosition2()
	{
		if(this.isValid())
			return m_pos2.clone();
		
		return null;
	}
	
	public World getWorld()
	{
		if(this.isValid())
			return m_pos1.getWorld();
		
		return null;
	}
	
	public void setPosition1(Location pos1)
	{
		m_pos1 = pos1.clone();
	}
	
	public void setPosition2(Location pos2)
	{
		m_pos2 = pos2.clone();
	}
	
	public Integer getSizeX()
	{
		if(this.isValid())
			return Math.abs(m_pos1.getBlockX() - m_pos2.getBlockX()) + 1;
		
		return null;
	}
	
	public Integer getSizeY()
	{
		if(this.isValid())
			return Math.abs(m_pos1.getBlockY() - m_pos2.getBlockY()) + 1;
		
		return null;
	}
	
	public Integer getSizeZ()
	{
		if(this.isValid())
			return Math.abs(m_pos1.getBlockZ() - m_pos2.getBlockZ()) + 1;
		
		return null;
	}
	
	public Integer getPosX()
	{
		if(this.isValid())
			return Math.min(m_pos1.getBlockX(), m_pos2.getBlockX());
		
		return null;
	}
	
	public Integer getPosY()
	{
		if(this.isValid())
			return Math.min(m_pos1.getBlockY(), m_pos2.getBlockY());
		
		return null;
	}
	
	public Integer getPosZ()
	{
		if(this.isValid())
			return Math.min(m_pos1.getBlockZ(), m_pos2.getBlockZ());
		
		return null;
	}
	
	public Integer getVolume()
	{
		if(this.isValid())
		{
			int sizex = Math.abs(m_pos1.getBlockX() - m_pos2.getBlockX()) + 1;
			int sizey = Math.abs(m_pos1.getBlockY() - m_pos2.getBlockY()) + 1;
			int sizez = Math.abs(m_pos1.getBlockZ() - m_pos2.getBlockZ()) + 1;
			
			return sizex * sizey * sizez;
		}
		
		return null;
	}
	
	public boolean isValid()
	{
		if(m_pos1 != null && m_pos2 != null)
		{
			return m_pos1.getWorld().equals(m_pos2.getWorld());
		}
		
		return false;
	}
}
