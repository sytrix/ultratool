package thecleaner.worldedit;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

import thecleaner.fonction.Argument;
import thecleaner.fonction.ProcessString;

public class MaterialManager 
{
	List<Material> m_listMaterial;
	List<Byte> m_listData;
	
	public MaterialManager()
	{
		m_listMaterial = new ArrayList<Material>();
		m_listData = new ArrayList<Byte>();
	}
	
	@SuppressWarnings("deprecation")
	public void processing(String string)
	{
		m_listMaterial.clear();
		m_listData.clear();
		
		String[] listBlock = string.split(",");
		
		for(int i = 0; i < listBlock.length; i++)
		{
			String blockId = Argument.getKey(listBlock[i]);
			String blockData = Argument.getValue(listBlock[i]);
			
			if(blockId == null)
				blockId = listBlock[i];
			
			if(blockId != null)
			{
				Integer id = null;
				Byte data = null;
				
				id = ProcessString.parseInt(blockId);
				
				if(blockData != null)
				{
					data = ProcessString.parseByte(blockData);
				}
				
				Material mat = null;
				
				if(id != null)
				{
					mat = Material.getMaterial(id);
				}
				else
					mat = Material.getMaterial(blockId.toUpperCase());
				
				if(mat != null)
				{
					if(blockData == null || data == null)
					{
						data = -1;
					}
				}
				
				m_listMaterial.add(mat);
				m_listData.add(data);
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public ItemStack[] getItemStack()
	{
		ItemStack[] items = null;
		int nbItem = m_listMaterial.size();
		
		if(nbItem > 0)
		{
			items = new ItemStack[nbItem];
			
			for(int i = 0; i < nbItem; i++)
			{
				if(m_listData.get(i) == -1)
					items[i] = new ItemStack(m_listMaterial.get(i));
				else
					items[i] = new ItemStack(m_listMaterial.get(i), 1, (short)0, m_listData.get(i));
			}
		}
		
		return items;
	}
	
	@SuppressWarnings("deprecation")
	public Block setBlock(Block block)
	{
		if(m_listMaterial.size() > 0)
		{
			int randomId = (int) (Math.random() * m_listMaterial.size());
			
			Material mat = m_listMaterial.get(randomId);
			if(mat != null)
			{
				block.setType(mat);
				
				if(m_listData.get(randomId) != -1)
					block.setData(m_listData.get(randomId));
				else
					block.setData((byte) 0);
			}
		}
		
		return block;
	}
	
	@SuppressWarnings("deprecation")
	public boolean contain(Block block)
	{
		if(m_listMaterial.size() > 0)
		{
			Material material = block.getType();
			Byte data = block.getData();
			
			for(int i = 0; i < m_listMaterial.size(); i++)
			{
				if(m_listMaterial.get(i).equals(material))
				{
					Byte iData = m_listData.get(i);
					
					if(iData == -1)
						return true;
					
					if(data.equals(iData))
						return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean isValid()
	{
		if(m_listMaterial.size() <= 0)
			return false;
		
		for(int i = 0; i < m_listMaterial.size(); i++)
		{
			if(m_listMaterial.get(i) == null)
			{
				return false;
			}
		}
		
		return true;
	}
}
