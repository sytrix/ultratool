package thecleaner.mob.process;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import thecleaner.UltraToolMain;
import thecleaner.fonction.ProcessString;
import thecleaner.mob.MobData;

public class ProcessMarchandise
{
	public static String set(UltraToolMain plugin, MobData mobData, String value)
	{
		String error = null;
		String[] marchandiseArg = value.split(",");
		
		if(marchandiseArg.length == 2)
		{
			ItemStack itemSell = ProcessMarchandise.parseArgument(plugin, marchandiseArg[0]);
			ItemStack itemBuy = ProcessMarchandise.parseArgument(plugin, marchandiseArg[1]);
			
			mobData.addMarchandise(itemSell, itemBuy);
		}
		else
		{
			error = "nombre d'argument incorrecte";
		}
		
		return error;
	}
	
	@SuppressWarnings("deprecation")
	private static ItemStack parseArgument(UltraToolMain plugin, String arg)
	{
		ItemStack itemRes = null;
		Integer intMat = ProcessString.parseInt(arg);
		
		if(intMat != null)
		{
			Material[] listMat = Material.values();
			Material mat = null;
			
			for(int i = 0; i < listMat.length; i++)
			{
				if(listMat[i].getId() == intMat)
					mat = listMat[i];
			}
			
			itemRes = new ItemStack(mat);
		}
		else
		{
			ItemStack[] itemStack = plugin.getListInv().get(arg, null);
			
			if(itemStack != null && itemStack.length >= 1)
			{
				itemRes = itemStack[0];
			}
			else
			{
				itemRes = new ItemStack(Material.getMaterial(arg));
			}
		}
		
		return itemRes;
	}
}
