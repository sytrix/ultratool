package thecleaner.mob.process;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import thecleaner.UltraToolMain;
import thecleaner.mob.MobData;

public class ProcessKit
{
	public static String set(UltraToolMain plugin, MobData mobData, String value, Player sender)
	{
		String error = null;
		ItemStack item[] = null;
		
		item = plugin.getListKit().get(value, sender);
		if(item == null)
			error = "aucun kit n'a de nom similaire � '" + value + "'";
		
		if(item != null)
			mobData.setEquipment(item);
		
		return error;
	}
}
