package thecleaner.mob;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

import thecleaner.UltraToolMain;
import thecleaner.fonction.PassengerManager;
import thecleaner.list.ListEntityProperty.EntityProperty;
import thecleaner.mob.param.ParamKit;
import thecleaner.mob.param.ParamLife;

@SuppressWarnings("unused")
public class FindMob 
{
	public static List<Entity> find(UltraToolMain plugin, GroupMobData groupMobData, List<String> errorLog)
	{
		Location spawnLocation = groupMobData.getSpawnLocation();
		List<MobData> listMobData = groupMobData.getListMobData();
		//Vector velocity = groupMobData.getVelocity();
		Integer quantiter = groupMobData.getQuantity();
		Integer rayon = groupMobData.getRadius();
		//Integer xp = groupMobData.getXp();
		
		List<Entity> listEntityFind = new ArrayList<Entity>();
		List<Entity> listWorldEntity = spawnLocation.getWorld().getEntities();
		int countEntityScan = 0;
		int nbEntityInWorld = listWorldEntity.size();
		
		while(countEntityScan < nbEntityInWorld)
		{
			Entity entityTest = listWorldEntity.get(countEntityScan);
			List<Entity> listPileEntity = new ArrayList<Entity>();
			boolean isFind = true;
			
			if(spawnLocation != null)
			{
				if(rayon != null)
				{
					if(entityTest.getLocation().distance(spawnLocation) > rayon)
					{
						isFind = false;
					}
				}
			}
			
			if(isFind)
			{
				for(int mobId = 0; mobId < listMobData.size(); mobId++)
				{
					if(entityTest == null) {
						isFind = false;
					} else if(isFind) {
						Collection<PotionEffect> collectionPotionEffect = listMobData.get(mobId).getCollectionPotion();
						String inventory = listMobData.get(mobId).getInventoryName();
						ItemStack[] equipment = listMobData.get(mobId).getEquipment(); // ...
						Player player = listMobData.get(mobId).getPlayer(); //...
						Entity entity = listMobData.get(mobId).getEntity(); //...
						EntityType entityType = listMobData.get(mobId).getEntityType(); //...
						Double life = listMobData.get(mobId).getLife(); //...
						Double damage = listMobData.get(mobId).getDamage(); //...
						String name = listMobData.get(mobId).getName(); //...
						String dynamicPoint = listMobData.get(mobId).getDynamicPoint(); //...
						Integer type = listMobData.get(mobId).getType();
						Integer style = listMobData.get(mobId).getStyle();
						Integer color = listMobData.get(mobId).getColor();
						Integer power = listMobData.get(mobId).getPower();
						Integer fire = listMobData.get(mobId).getFire();
						Boolean isFriend = listMobData.get(mobId).isFriend();
						Boolean isInvincible = listMobData.get(mobId).isInvincible(); //...
						Boolean isPassive = listMobData.get(mobId).isPassive(); //...
						Byte isBaby = listMobData.get(mobId).isBaby();
						Boolean havePassenger = listMobData.get(mobId).havePassenger(); // ...
						
						EntityProperty entityProperty = plugin.getListEntityProperty().getProperty(entityTest);
						
						if(entityType != null && !entityTest.getType().equals(entityType))
							isFind = false;
						else if(player != null && player != entityTest)
							isFind = false;
						else if(entity != null && entity != entityTest)
							isFind = false;
						
						if(entityTest instanceof LivingEntity)
						{
							LivingEntity livingEntity = (LivingEntity)entityTest;
							
							if(equipment != null && !ParamKit.cmp(plugin, livingEntity, equipment)) {
								isFind = false;
							}
							if(name != null && !livingEntity.getName().equals(name)) {
								isFind = false;
							}
						}
						
						if(entityTest instanceof Damageable) 
						{
							Damageable damageable = (Damageable)entityTest;
							if(life != null && ParamLife.cmp(damageable, life)) {
								isFind = false;
							}
						}
						
						if(entityProperty == null) {
							if(damage != null || dynamicPoint != null || isInvincible != null || isPassive != null) {
								isFind = false;
							}
						} else {
							if(damage != null && damage.equals(entityProperty.getDamage())) {
								isFind = false;
							}
							if(dynamicPoint != null && !dynamicPoint.equals(entityProperty.getReference())) {
								isFind = false;
							}
							if(isInvincible != null && isInvincible.equals(entityProperty.isInvincible())) {
								isFind = false;
							}
							if(isPassive != null && isPassive.equals(entityProperty.isPassive())) {
								isFind = false;
							}
						}
						
						if(havePassenger != null) {
							boolean isVehicle = PassengerManager.haveEntityPassenger(entityTest);
							
							if(isVehicle != havePassenger.booleanValue()) {
								isFind = false;
							}
						}
						
						if(isFind)
						{
							listPileEntity.add(entityTest);
							entityTest = entityTest.getVehicle();
						}
					}
				}
				
				if(isFind)
				{
					for(int i = 0; i < listPileEntity.size(); i++)
						listEntityFind.add(listPileEntity.get(i));
				}
			}
			
			countEntityScan++;
		}
		
		if(quantiter != null && listEntityFind.size() > quantiter)
		{
			List<Entity> newListEntity = new ArrayList<Entity>();
			
			for(int i = 0; i < quantiter; i++)
			{
				int id = (int)(Math.random() * listEntityFind.size());
				
				newListEntity.add(listEntityFind.get(id));
				listEntityFind.remove(id);
			}
			
			listEntityFind = newListEntity;
		}
		
		return listEntityFind;
	}
}
