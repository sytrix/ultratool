package thecleaner.mob;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.DragonFireball;
import org.bukkit.entity.EnderSignal;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.EvokerFangs;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Explosive;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Horse;
import org.bukkit.entity.LeashHitch;
import org.bukkit.entity.LightningStrike;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.SmallFireball;
import org.bukkit.entity.Tameable;
import org.bukkit.entity.WitherSkull;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

import thecleaner.UltraToolMain;
import thecleaner.fonction.CheckClass;
import thecleaner.fonction.PassengerManager;
import thecleaner.list.ListEntityProperty.EntityProperty;
import thecleaner.mob.param.ParamBaby;
import thecleaner.mob.param.ParamColor;
import thecleaner.mob.param.ParamEntity;
import thecleaner.mob.param.ParamKit;
import thecleaner.mob.param.ParamLife;
import thecleaner.mob.param.ParamName;
import thecleaner.mob.param.ParamPower;
import thecleaner.mob.param.ParamStyle;
import thecleaner.mob.param.ParamType;

public class SpawnMob 
{
	public static boolean spawn(UltraToolMain plugin, GroupMobData groupMobData, List<String> errorLog)
	{
		Location spawnLocation = groupMobData.getSpawnLocation();
		List<MobData> listMobData = groupMobData.getListMobData();
		Vector velocity = groupMobData.getVelocity();
		Integer quantiter = groupMobData.getQuantity();
		Integer rayon = groupMobData.getRadius();
		Integer xp = groupMobData.getXp();
		
		if(quantiter == null)
			quantiter = 1;
		
		for(int i = 0; i < quantiter; i++)
		{
			Entity lastEntity = null;
			List<LivingEntity> listNewEntityXp = new ArrayList<LivingEntity>();
			List<Entity> listNewEntity = new ArrayList<Entity>();
			
			for(int mobId = 0; mobId < listMobData.size(); mobId++)
			{
				Location spawnMob = null;
				int spawnErrorCount = 0;
				boolean spawnError = true;
				
				if(spawnLocation != null)
				{
					spawnMob = spawnLocation.clone();
					
					if(rayon != null)
					{
						do
						{
							double distance = Math.random() * rayon;
					    	double degree = Math.random() * Math.PI * 2;
					    	
					    	double posx = distance * Math.cos(degree);
					    	double posz = distance * Math.sin(degree);
					    	
					    	spawnMob.setX((int)(spawnLocation.getX() + posx) + 0.5);
					    	spawnMob.setY(spawnLocation.getY());
					    	spawnMob.setZ((int)(spawnLocation.getZ() + posz) + 0.5);
					    	
					    	if(spawnMob.getBlock().getType().equals(Material.AIR))
					    		spawnError = false;
					    	
	
							spawnErrorCount++;
						}
						while(spawnErrorCount < 8 && spawnError);
					}
					else
						spawnError = false;
						
				}
				
				if(!spawnError)
				{
					MobData modData = listMobData.get(mobId);
					
					Collection<PotionEffect> collectionPotionEffect = modData.getCollectionPotion();
					String inventory = modData.getInventoryName();
					ItemStack[] equipment = modData.getEquipment();
					Player player = modData.getPlayer();
					Entity entity = modData.getEntity();
					EntityType entityType = modData.getEntityType();
					Double life = modData.getLife();
					Double damage = modData.getDamage();
					String name = modData.getName();
					String dynamicPoint = modData.getDynamicPoint();
					Integer type = modData.getType();
					Integer style = modData.getStyle();
					Integer color = modData.getColor();
					Integer power = modData.getPower();
					Integer fire = modData.getFire();
					Boolean isFriend = modData.isFriend();
					Boolean isInvicible = modData.isInvincible();
					Boolean isPassive = modData.isPassive();
					Byte isBaby = modData.isBaby();
					Boolean havePassenger = modData.havePassenger();
					
					if(entity != null)
					{
						if(spawnMob != null)
						{
							if(plugin.getPluginConfig().getSpawnEntityTeleport())
							{
								Location local = entity.getLocation();
								local.setX(spawnMob.getX());
								local.setY(spawnMob.getY());
								local.setZ(spawnMob.getZ());
								
							    entity.teleport(local);
							}
						}
					}
					else if(entityType != null)
					{
						entity = ParamEntity.set(spawnMob, listMobData.get(mobId));
						
						if(entity == null)
							errorLog.add("L'entit� " + mobId + " n'a pas pue spawn");
					}
					else if(player != null)
					{
						entity = player;
						
						if(spawnMob != null)
						{
							if(plugin.getPluginConfig().getSpawnPlayerTeleport())
							{
								Location local = entity.getLocation();
								local.setX(spawnMob.getX());
								local.setY(spawnMob.getY());
								local.setZ(spawnMob.getZ());
								
							    entity.teleport(local);
							}
						}
						
						/*
						if(entity == null)
							errorLog.add("L'entit� " + mobId + " n'a pas �t� trouv� (joueur:"+ player.getName() +")");
						*/
					}
					
					if(entity != null) 
					{
						EntityProperty entityProperty = null;
						if(SpawnMob.entityIsRemovable(entity)) {
							plugin.getListEntityProperty().addEntity(entity);
						    entityProperty = plugin.getListEntityProperty().getProperty(entity);
						}
						
						listNewEntity.add(entity);
						
						
						if(lastEntity != null)
						{
							if(havePassenger == null || havePassenger)
							{
								if(CheckClass.exist("org.bukkit.entity.Horse") 
										&& entity instanceof Horse)
								{
									Horse horse = (Horse)entity;
									horse.setTamed(true);
								}
								
								PassengerManager.setEntityPassenger(entity, lastEntity);
							}
							else
							{
								PassengerManager.clearEntityPassenger(entity);
							}
						}
						
						if(dynamicPoint != null)
						{
							if(entityProperty != null) {
								plugin.getListEntityProperty().addReference(entity, dynamicPoint);
							}
						}
						
						if(velocity != null)
						{
							if(mobId == listMobData.size() - 1)
							{
								if(entity instanceof Fireball)
								{
									Fireball fireball = (Fireball)entity;
									fireball.setDirection(velocity);
								}
								else
								{
									entity.setVelocity(velocity);
								}
							}
						}
						
						lastEntity = entity;
						
						if(entity instanceof LivingEntity)
						{
							LivingEntity livingEntity = (LivingEntity) entity;
							if(!(entity instanceof Player)) {
								livingEntity.setCanPickupItems(false);
							}
							
							if(xp != null)
							{
								if(SpawnMob.entityIsRemovable(entity)) {
									listNewEntityXp.add(livingEntity);
								}
							}
							
							if(equipment != null)
								ParamKit.set(plugin, livingEntity, equipment);
							
							if(collectionPotionEffect != null)
							{
								livingEntity.addPotionEffects(collectionPotionEffect);
							}
							
							if(entityProperty != null) {
								entityProperty.setLivingEntity(true);
							}
						}
						
						if(name != null)
							ParamName.set(plugin, entity, name);
						if(type != null)
							ParamType.set(entity, type);
						if(style != null)
							ParamStyle.set(entity, style);
						if(color != null)
							ParamColor.set(entity, color);
						if(power != null)
							ParamPower.set(entity, power);
						
						if(fire != null)
						{
							if(entity instanceof Explosive)
							{
								Explosive explosive = (Explosive)entity;
								explosive.setIsIncendiary(fire > 0);
							}
							entity.setFireTicks(fire);
						}
						
						if(damage != null)
						{
							if(entityProperty != null) {
								entityProperty.setDamage(damage);
							}
						}
						
						if(inventory != null)
						{
							if(entityProperty != null) {
								entityProperty.setInvToDrop(inventory);
							}
							
							if(entity instanceof InventoryHolder)
							{
								/*
								if(CheckClass.exist("org.bukkit.entity.Horse") && entity instanceof Horse)
								{
									Horse horse = (Horse) entity;
									horse.setCarryingChest(true);
								}*/
								
								InventoryHolder entityInventoryHolder = (InventoryHolder) entity;
								Inventory entityInventory = entityInventoryHolder.getInventory();
								
								ItemStack[] items = plugin.getListInv().get(inventory, null);
								for(int invId = 0; invId < items.length; invId++)
									entityInventory.addItem(items[invId]);
							}
						}
						
						if(isInvicible != null)
						{
							if(isInvicible)
							{
								if(entityProperty != null) {
									entityProperty.setInvincible(true);
								}
							}
						}
						
						if(isPassive != null)
						{
							if(isPassive)
							{
								if(entityProperty != null) {
									entityProperty.setPassive(true);
								}
							}
						}
							
						if(isFriend != null)
						{
							if(entity instanceof Tameable)
							{
								Tameable tame = (Tameable)entity;
								tame.setTamed(isFriend);
							}
						}
						
						if(isBaby != null)
						{
							ParamBaby.set(entity, isBaby);
						}
						
						if(entity instanceof Damageable)
						{
							Damageable damageable = (Damageable) entity;
							
							if(life != null)
							{
								ParamLife.set(damageable, life);
							}
						}
					}
				}
			}
			
			if(CheckClass.exist("org.bukkit.entity.LeashHitch"))
			{
				for(int mobId = 0; mobId < listNewEntity.size(); mobId++)
				{
					Integer entityLeadId = listMobData.get(mobId).getEntityLead();
					
					if(entityLeadId != null)
					{
						Entity entity = listNewEntity.get(mobId);
						
						if(entity instanceof LivingEntity)
						{
							LivingEntity livingEntity = (LivingEntity) entity;
							
							if(entityLeadId >= 0 && entityLeadId < listNewEntity.size())
							{
								livingEntity.setLeashHolder(listNewEntity.get(entityLeadId));
							}
						}
					}
				}
			}
			
			if(xp != null)
			{
				plugin.getListEntityXp().add(listNewEntityXp, xp);
			}
		}
		
		return true;
	}
	
	private static boolean entityIsRemovable(Entity entity) {
		if((entity instanceof LightningStrike) || (entity instanceof ExperienceOrb) || (entity instanceof AreaEffectCloud) 
				|| (entity instanceof LeashHitch) || (entity instanceof EnderSignal) || (entity instanceof DragonFireball) 
				|| (entity instanceof EvokerFangs) || (entity instanceof WitherSkull) || (entity instanceof SmallFireball)
				 || (entity instanceof Fireball)) {
			return false;
		}
		
		return true;
	}
}
