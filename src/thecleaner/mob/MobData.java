package thecleaner.mob;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

public class MobData 
{
	private Collection<PotionEffect> m_collectionPotionEffect;
	private List<ItemStack> m_marchandise;
	private String m_inventoryName;
	private ItemStack[] m_equipment;
	private EntityType m_entityType;
	private Player m_player;
	private Entity m_entity;
	private String m_name;
	private String m_dynamicPoint;
	private Double m_life;
	private Double m_damage;
	private Integer m_type;
	private Integer m_style;
	private Integer m_color;
	private Integer m_entityLead;
	private Integer m_power;
	private Integer m_fire;
	private Boolean m_friend;
	private Boolean m_invincible;
	private Boolean m_passive;
	private Byte m_baby;
	private Boolean m_passager;	
	
	
	public MobData()
	{
		m_collectionPotionEffect = new ArrayList<PotionEffect>();
		m_marchandise = new ArrayList<ItemStack>();
		m_inventoryName = null;
		m_equipment = null;
		m_marchandise = null;
		m_entityType = null;
		m_player = null;
		m_entity = null;
		m_name = null;
		m_dynamicPoint = null;
		m_life = null;
		m_damage = null;
		m_type = null;
		m_style = null;
		m_color = null;
		m_entityLead = null;
		m_power = null;
		m_fire = null;
		m_friend = null;
		m_invincible = null;
		m_baby = null;
		m_passager = null;
		m_passive = null;
	}
	
	public Collection<PotionEffect> getCollectionPotion()
	{ 
		return m_collectionPotionEffect; 
	}
	
	public List<ItemStack> getMarchandise()
	{
		return m_marchandise;
	}
	
	public String getInventoryName()
	{ 
		return m_inventoryName; 
	}
	
	public ItemStack[] getEquipment()
	{ 
		return m_equipment; 
	}
	
	public EntityType getEntityType()
	{
		return m_entityType;
	}
	
	public Player getPlayer()
	{
		return m_player;
	}
	
	public Entity getEntity()
	{
		return m_entity;
	}
	
	public String getName()
	{ 
		return m_name; 
	}
	
	public String getDynamicPoint()
	{ 
		return m_dynamicPoint; 
	}
	
	public Double getLife()
	{ 
		return m_life; 
	}
	
	public Double getDamage()
	{
		return m_damage;
	}
	
	public Integer getType()
	{ 
		return m_type; 
	}
	
	public Integer getStyle()
	{ 
		return m_style; 
	}
	
	public Integer getColor()
	{
		return m_color; 
	}
	
	public Integer getEntityLead()
	{
		return m_entityLead;
	}
	
	public Integer getPower()
	{
		return m_power;
	}
	
	public Integer getFire()
	{
		return m_fire;
	}
	
	public Boolean isFriend()
	{
		return m_friend;
	}
	
	public Boolean isInvincible()
	{
		return m_invincible;
	}
	
	public Boolean isPassive()
	{
		return m_passive;
	}
	
	public Byte isBaby()
	{ 
		return m_baby; 
	}
	
	public Boolean havePassenger()
	{
		return m_passager; 
	}
	
	public void addEffectPotion(PotionEffect potionEffect)
	{ 
		m_collectionPotionEffect.add(potionEffect); 
	}
	
	public void setInventoryName(String inventoryName)
	{ 
		m_inventoryName = inventoryName; 
	}
	
	public void setEquipment(ItemStack[] equipment)
	{ 
		m_equipment = equipment; 
	}
	
	public void setEquipment(ItemStack item, int id)
	{
		if(m_equipment == null)
		{
			m_equipment = new ItemStack[6];
			for(int i = 0; i < m_equipment.length; i++)
			{
				m_equipment[i] = new ItemStack(Material.AIR);
			}
		}
		
		if(id >= 0 && id < 6)
			return;
		
		m_equipment[id] = item;
	}
	
	public void addMarchandise(ItemStack sell, ItemStack buy)
	{
		m_marchandise.add(sell);
		m_marchandise.add(buy);
	}
	
	public void setEntityType(EntityType entityType)
	{
		m_entityType = entityType;
	}
	
	public void setPlayer(Player player)
	{
		m_player = player;
	}
	
	public void setEntity(Entity entity)
	{
		m_entity = entity;
	}
	
	public void setName(String name)
	{ 
		m_name = name; 
	}
	
	public void setDynamicPoint(String dynamicPoint)
	{ 
		m_dynamicPoint = dynamicPoint; 
	}
	
	public void setLife(Double life)
	{ 
		m_life = life;
	}
	
	public void setDamage(Double damage)
	{
		m_damage = damage;
	}
	
	public void setType(Integer type)
	{ 
		m_type = type;
	}
	
	public void setStyle(Integer style)
	{ 
		m_style = style;
	}
	
	public void setColor(Integer color)
	{ 
		m_color = color;
	}
	
	public void setEntityLead(Integer entityId)
	{
		m_entityLead = entityId;
	}
	
	public void setPower(Integer power)
	{
		m_power = power;
	}
	
	public void setFire(Integer fire)
	{
		m_fire = fire;
	}
	
	public void setFriend(Boolean friend)
	{
		m_friend = friend;
	}
	
	public void setInvincible(Boolean invincible)
	{
		m_invincible = invincible;
	}
	
	public void setPassive(Boolean passive)
	{
		m_passive = passive;
	}
	
	public void setBaby(Byte baby)
	{ 
		m_baby = baby;
	}
	
	public void setHavePassenger(Boolean passager)
	{
		m_passager = passager; 
	}
}
