package thecleaner.mob.param;

import org.bukkit.entity.Ageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Zombie;

public class ParamBaby 
{
	public static String set(Entity entity, Byte isBaby)
	{
		if(entity instanceof Zombie)
		{
			Zombie zombie = (Zombie) entity;
			
			if(isBaby == -1)
			{
				zombie.setBaby((Math.random() * 2) > 1);
			}
			else
			{
				zombie.setBaby(isBaby == 1);
			}
			
		}
		else if(entity instanceof Ageable)
		{
			Ageable mobAge = (Ageable) entity;
			
			if(isBaby == -1)
			{
				if((Math.random() * 2) > 1)
					mobAge.setBaby();
				else
					mobAge.setAdult();
			}
			else
			{
				if(isBaby == 1)
					mobAge.setBaby();
				else
					mobAge.setAdult();
			}
		}
		
		return null;
	}
}
