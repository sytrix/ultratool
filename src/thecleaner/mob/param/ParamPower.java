package thecleaner.mob.param;

import org.bukkit.entity.Boat;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Fish;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.inventory.meta.FireworkMeta;

@SuppressWarnings("deprecation")
public class ParamPower 
{
	public static String set(Entity entity, Integer power)
	{
		if(entity instanceof Horse)
		{
			Horse horse = (Horse) entity;
			horse.setJumpStrength(power);
		}
		else if(entity instanceof Firework)
		{
			Firework firework = (Firework) entity;
			FireworkMeta meta = firework.getFireworkMeta();
			meta.setPower(power);
			firework.setFireworkMeta(meta);
		}
		else if(entity instanceof ExperienceOrb)
		{
			ExperienceOrb xpOrb = (ExperienceOrb) entity;
			xpOrb.setExperience(power);
		}
		else if(entity instanceof Boat)
		{
			Boat boat = (Boat) entity;
			boat.setMaxSpeed(power);
		}
		else if(entity instanceof Minecart)
		{
			Minecart minecart = (Minecart) entity;
			minecart.setMaxSpeed(power);
		}
		else if(entity instanceof Fish)
		{
			Fish fish = (Fish) entity;
			fish.setBiteChance((double)power / 100f);
		}
		else if(entity instanceof ThrownPotion)
		{
			
		}
		
		return null;
	}
}



