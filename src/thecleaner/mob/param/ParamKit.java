package thecleaner.mob.param;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

import thecleaner.UltraToolMain;
import thecleaner.fonction.CheckClass;
import thecleaner.fonction.CheckMethod;
import thecleaner.fonction.KitManager;

public class ParamKit 
{
	static public String set(UltraToolMain plugin, LivingEntity livingEntity, ItemStack[] equipment)
	{
		KitManager.setKit(livingEntity, equipment);
		boolean armorStandExist = CheckClass.exist("org.bukkit.entity.ArmorStand");
		
		
		if(!armorStandExist || (armorStandExist && !(livingEntity instanceof ArmorStand)))
		{
			EntityEquipment entityEquipment = livingEntity.getEquipment();
			
			if(entityEquipment != null && !plugin.getPluginConfig().getLootKit() && !(livingEntity instanceof Player))
			{
				entityEquipment.setHelmetDropChance(0);
				entityEquipment.setChestplateDropChance(0);
				entityEquipment.setLeggingsDropChance(0);
				entityEquipment.setBootsDropChance(0);
				
				if(CheckMethod.existDoubleHand()) {
					entityEquipment.setItemInMainHandDropChance(0);
					entityEquipment.setItemInOffHandDropChance(0);
				} else {
					ParamKit.setItemInHandDropChance(entityEquipment, 0);
				}
					
			}
		}
		
		return null;
	}
	
	static public boolean cmp(UltraToolMain plugin, LivingEntity livingEntity, ItemStack[] equipment)
	{
		ItemStack[] listItemStack = KitManager.getKit(livingEntity);
		int nbEquipment = 5;
		
		if(CheckMethod.existDoubleHand())
			nbEquipment = 6;
		
		for(int i = 0; i < nbEquipment; i++)
		{
			if(!listItemStack[i].equals(equipment[i]))
				return false;
		}
		
		return true;
	}
	
	@SuppressWarnings("deprecation")
	static private void setItemInHandDropChance(EntityEquipment entityEquipment, int value) {
		entityEquipment.setItemInHandDropChance(value);
	}
}
