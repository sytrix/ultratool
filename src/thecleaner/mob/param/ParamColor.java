package thecleaner.mob.param;

import org.bukkit.DyeColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Llama;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Wolf;

import thecleaner.fonction.CheckClass;

public class ParamColor 
{
	public static String set(Entity entity, Integer color)
	{
		if(entity instanceof Horse)
		{
			Horse horse = (Horse) entity;
			Horse.Color horseColor[] = Horse.Color.values();
			int taille = horseColor.length;
			if(color == -1)
			{
				int aleat = (int) (Math.random() * horseColor.length);
				horse.setColor(horseColor[aleat]);
			}
			else if(color >= 0 && color < taille)
				horse.setColor(horseColor[color]);
			
		}
		else if(entity instanceof Sheep)
		{
			Sheep sheep = (Sheep) entity;
			DyeColor[] sheepColor = DyeColor.values();
			int taille = sheepColor.length;
			if(color == -1)
			{
				int aleat = (int) (Math.random() * sheepColor.length);
				sheep.setColor(sheepColor[aleat]);
			}
			else if(color >= 0 && color < taille)
				sheep.setColor(sheepColor[color]);
		}
		else if(entity instanceof Wolf)
		{
			Wolf wolf = (Wolf) entity;
			DyeColor[] wolfColor = DyeColor.values();
			int taille = wolfColor.length;
			if(color == -1)
			{
				int aleat = (int) (Math.random() * wolfColor.length);
				wolf.setCollarColor(wolfColor[aleat]);
			}
			else if(color >= 0 && color < taille)
				wolf.setCollarColor(wolfColor[color]);
		}
		else if(CheckClass.existLlama() && entity instanceof Llama)
		{
			Llama lama = (Llama)entity;
			Llama.Color lamaColor[] = Llama.Color.values();
			int taille = lamaColor.length;
			if(color == -1)
			{
				int aleat = (int) (Math.random() * lamaColor.length);
				lama.setColor(lamaColor[aleat]);
			}
			else if(color >= 0 && color < taille)
				lama.setColor(lamaColor[color]);
			
		}
		
		
		return null;
	}
}
