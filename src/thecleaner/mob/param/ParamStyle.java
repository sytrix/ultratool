package thecleaner.mob.param;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;

public class ParamStyle 
{
	public static String set(Entity entity, Integer style)
	{
		if(style != null)
		{
			if(entity instanceof Horse)
			{
				Horse horse = (Horse) entity;
				Horse.Style[] horseStyle = Horse.Style.values();
				if(style == -1)
				{
					int aleat = (int) (Math.random() * horseStyle.length);
					horse.setStyle(horseStyle[aleat]);
				}
				else if(style >= 0 && style < horseStyle.length)
					horse.setStyle(horseStyle[style]);
			}
		}
		
		return null;
	}
}
