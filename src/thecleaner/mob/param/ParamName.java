package thecleaner.mob.param;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import thecleaner.UltraToolMain;
import thecleaner.fonction.StringGen;

public class ParamName 
{
	public static String set(UltraToolMain plugin, Entity entity, String name)
	{
		if(name.equals("?"))
		{
			name = StringGen.name();
		}
		
		if(entity instanceof LivingEntity)
		{
			if(entity instanceof Player)
			{
				
			}
			else
			{
				LivingEntity livingEntity = (LivingEntity) entity;
				
				if(name.equals("#"))
				{
					livingEntity.setCustomName("");
					livingEntity.setCustomNameVisible(false);
				}
				else
				{
					livingEntity.setCustomName(name);
					livingEntity.setCustomNameVisible(true);
				}
			}
		}
		else if(entity instanceof Item)
		{
			Item item = (Item) entity;
			ItemStack itemStack = item.getItemStack();
			ItemMeta itemMeta = itemStack.getItemMeta();
			itemMeta.setDisplayName(name);
			itemStack.setItemMeta(itemMeta);
			item.setItemStack(itemStack);
		}
		
		
		return name;
	}
}
