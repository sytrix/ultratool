package thecleaner.mob.param;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.Item;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Pig;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Slime;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Zombie;
import org.bukkit.entity.Ocelot.Type;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import thecleaner.fonction.CheckClass;

public class ParamType 
{
	@SuppressWarnings("deprecation")
	public static String set(Entity entity, Integer type)
	{
		if(entity instanceof Zombie)
		{
			Zombie zombie = (Zombie) entity;
			
			if(!CheckClass.existWitherSkeleton()) {
				if(type == -1)
				{
					Random rand = new Random();
					zombie.setVillager(rand.nextBoolean());
				}
				else 
					zombie.setVillager(type == 1);
			}
		}
		else if(entity instanceof Skeleton)
		{
			Skeleton skeleton = (Skeleton) entity;
			if(!CheckClass.existWitherSkeleton()) {
				org.bukkit.entity.Skeleton.SkeletonType skeletonType[] = org.bukkit.entity.Skeleton.SkeletonType.values();
				if(type == -1)
				{
					int aleat = (int) (Math.random() * skeletonType.length);
					skeleton.setSkeletonType(skeletonType[aleat]);
				}
				else if(type >= 0 && type < skeletonType.length)
					skeleton.setSkeletonType(skeletonType[type]);
			}
		}
		else if(entity instanceof PigZombie)
		{
			PigZombie pigZombie = (PigZombie) entity;
			if(!CheckClass.existWitherSkeleton()) {
				if(type == -1)
				{
					Random rand = new Random();
					pigZombie.setVillager(rand.nextBoolean());
				}
				else 
					pigZombie.setVillager(type == 1);
			}
		}
		else if(entity instanceof Sheep)
		{
			Sheep sheep = (Sheep) entity;
			if(type == -1)
			{
				Random rand = new Random();
				sheep.setSheared(rand.nextBoolean());
			}
			else if(type == 0)
			{
				sheep.setSheared(false);
			}
			else if(type == 1)
			{
				sheep.setSheared(true);
			}
		}
		else if(entity instanceof Pig)
		{
			Pig pig = (Pig) entity;
			if(type == -1)
			{
				Random rand = new Random();
				pig.setSaddle(rand.nextBoolean());
			}
			else
				pig.setSaddle(type == 1);
		}

		else if(entity instanceof Creeper)
		{
			Creeper creeper = (Creeper) entity;
			if(type == -1)
			{
				Random rand = new Random();
				creeper.setPowered(rand.nextBoolean());
			}
			else
				creeper.setPowered(type == 1);
		}
		else if(entity instanceof Ocelot)
		{
			Ocelot ocelot = (Ocelot) entity;
			Type[] ocelotType = Ocelot.Type.values();
			if(type == -1)
			{
				int aleat = (int) (Math.random() * ocelotType.length);
				ocelot.setCatType(ocelotType[aleat]);
			}
			else if(type >= 0 && type < ocelotType.length)
				ocelot.setCatType(ocelotType[type]);
		}
		else if(CheckClass.exist("org.bukkit.entity.Horse") 
				&& entity instanceof Horse)
		{
			Horse horse = (Horse) entity;
			if(!CheckClass.existWitherSkeleton()) {
				Horse.Variant[] horseVariant = Horse.Variant.values();
				if(type == -1)
				{
					int aleat = (int) (Math.random() * horseVariant.length);
					horse.setVariant(horseVariant[aleat]);
				}
				else if(type >= 0 && type < horseVariant.length)
					horse.setVariant(horseVariant[type]);
			}
		}
		else if(entity instanceof Villager)
		{
			Villager villager = (Villager) entity;
			Profession[] listProfession = Profession.values();
			
			if(type == -1)
			{
				int aleat = (int) (Math.random() * listProfession.length);
				villager.setProfession(listProfession[aleat]);
			}
			if(type >= 0 && type < listProfession.length)
				villager.setProfession(listProfession[type]);
		}
		else if(entity instanceof Enderman)
		{
			Material[] listMaterial = Material.values();
			
			if(type == -1)
			{
				int aleat = (int) (Math.random() * listMaterial.length);
				if(listMaterial[aleat].isBlock())
				{
					MaterialData matData = listMaterial[aleat].getNewData((byte) 0);
					Enderman enderman = (Enderman) entity;
					enderman.setCarriedMaterial(matData);
				}
			}
			else if(type >= 0 && type < listMaterial.length)
			{
				if(listMaterial[type].isBlock())
				{
					MaterialData matData = listMaterial[type].getNewData((byte) 0);
					Enderman enderman = (Enderman) entity;
					enderman.setCarriedMaterial(matData);
				}
			}
		}
		else if(entity instanceof IronGolem)
		{
			IronGolem ironGolem = (IronGolem) entity;
			
			if(type == -1)
			{
				Random rand = new Random();
				ironGolem.setPlayerCreated(rand.nextBoolean());
			}
			else
				ironGolem.setPlayerCreated((type == 1));
		}
		else if(entity instanceof Slime)
		{
			Slime slime = (Slime) entity;
			
			if(type == -1)
			{
				slime.setSize(type);
			}
		}
		else if(entity instanceof Item)
		{
			Material material[] = Material.values();
			
			if((type > 0 && type < material.length) || type == -1)
			{
				Item item = (Item) entity;
				ItemStack itemStack = item.getItemStack();
				if(type == -1)
				{
					int aleat = (int) (Math.random() * (material.length - 1)) + 1;
					itemStack.setType(material[aleat]);
				}
				else
					itemStack.setType(material[type]);
				item.setItemStack(itemStack);
				
				
			}
			
		}
		else if(entity instanceof ThrownPotion)
		{
			PotionType listPotion[] = PotionType.values();
			
			if((type > 0 && type < listPotion.length) || type == -1)
			{
				ThrownPotion potion = (ThrownPotion) entity;
				ItemStack itemStack = potion.getItem();
				int newType = type;
				
				if(type == -1)
					newType = (int) (Math.random() * (listPotion.length - 1)) + 1;
				
				
				PotionMeta potionMeta = (PotionMeta) itemStack.getItemMeta();
				potionMeta.setBasePotionData(new PotionData(listPotion[newType], false, false));
				itemStack.setItemMeta(potionMeta);
				
				/*
				Potion pot = new Potion(listPotion[newType]); 
				pot.setHasExtendedDuration(true);
				pot.setSplash(true);
				pot.apply(itemStack);
		        
		        */
				
				potion.setItem(itemStack);
				
			}
			
			
		}
		else if(CheckClass.existArmorStand() && entity instanceof ArmorStand)
		{
			ArmorStand armorStand = (ArmorStand)entity;
			boolean arms = (type & 0x01) == 0x01;
			boolean basePlate = (type & 0x02) != 0x02;
			boolean gravity = (type & 0x04) != 0x04;
			boolean small = (type & 0x08) == 0x08;
			boolean visible = (type & 0x10) != 0x10;
			
			armorStand.setArms(arms);
			armorStand.setBasePlate(basePlate);
			armorStand.setGravity(gravity);
			armorStand.setSmall(small);
			armorStand.setVisible(visible);
		}
		
		return null;
	}
}
