package thecleaner.mob.param;


import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.LeashHitch;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import thecleaner.fonction.CheckClass;
import thecleaner.mob.MobData;

public class ParamEntity 
{
	@SuppressWarnings("deprecation")
	public static Entity set(Location spawnLocation, MobData mobData)
	{
		Entity entity = null;
		World world = spawnLocation.getWorld();
		EntityType entityType = mobData.getEntityType();
		Integer type = mobData.getType();
		Integer power = mobData.getPower();
		
		if(CheckClass.exist("org.bukkit.entity.LeashHitch") && entityType.equals(EntityType.LEASH_HITCH))
		{
			boolean fenceExist = false;
			
			if(spawnLocation.getBlock().getType().equals(Material.FENCE))
				fenceExist = true;
			
			if(!fenceExist)
				spawnLocation.getBlock().setType(Material.FENCE);
			
			entity = world.spawn(spawnLocation.getBlock().getLocation(), LeashHitch.class);
			
			if(!fenceExist)
				spawnLocation.getBlock().setType(Material.AIR);
			
			return entity;
		}
		
		if(entityType.equals(EntityType.PLAYER))
		{
			
		}
		else if(entityType.equals(EntityType.PAINTING) || entityType.equals(EntityType.FISHING_HOOK) || entityType.equals(EntityType.COMPLEX_PART) || entityType.equals(EntityType.UNKNOWN))
		{
			
		}
		else if(entityType.equals(EntityType.DROPPED_ITEM))
		{
			Material mat = Material.STONE;
			
			if(type != null)
			{
				Material[] listMaterial = Material.values();
				
				if(type >= 0 && type < listMaterial.length)
				{
					mat = listMaterial[type];
				}
			}
			
			ItemStack item = new ItemStack(mat, 1);
			entity = world.dropItem(spawnLocation, item);
		}
		else if(entityType.equals(EntityType.FALLING_BLOCK))
		{
			Material mat = Material.SAND;
			
			if(type != null)
			{
				Material[] listMaterial = Material.values();
				
				if(type >= 0 && type < listMaterial.length)
				{
					if(listMaterial[type].isBlock())
					{
						mat = listMaterial[type];
					}
				}
				else if(type == -1)
				{
					Random rand = new Random();
					int randomNumber = (int) (rand.nextFloat() * listMaterial.length);
					
					while(!listMaterial[randomNumber].isBlock())
					{
						randomNumber++;
						
						if(randomNumber == listMaterial.length)
							randomNumber = 0;
					}
					
					mat = listMaterial[randomNumber];
				}
			}
			
			entity = world.spawnFallingBlock(spawnLocation, mat, (byte) 0);
		}
		else if(entityType.equals(EntityType.ARROW))
		{
			float arrowPower = 0;
			
			if(power != null)
			{
				arrowPower = (float)power / 15f;
			}
			
			entity = world.spawnArrow(spawnLocation, new Vector(0, 1, 0), arrowPower, 0.5f);
		}
		else if(entityType.equals(EntityType.LIGHTNING) || entityType.equals(EntityType.WEATHER))
		{
			entity = world.strikeLightning(spawnLocation);
		}
		else if(entityType.equals(EntityType.ITEM_FRAME))
		{
			boolean fenceExist = false;
			
			if(spawnLocation.getBlock().getType().equals(Material.COBBLE_WALL))
				fenceExist = true;
			
			if(!fenceExist)
				spawnLocation.getBlock().setType(Material.COBBLE_WALL);
			
			entity = world.spawn(spawnLocation.getBlock().getLocation(), ItemFrame.class);
			
			if(!fenceExist)
				spawnLocation.getBlock().setType(Material.AIR);
		}
		else
		{
			entity = world.spawnEntity(spawnLocation, entityType);
		}
		
		return entity;
	}
}
