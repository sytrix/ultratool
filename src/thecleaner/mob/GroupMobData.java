package thecleaner.mob;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import thecleaner.UltraToolMain;
import thecleaner.fonction.Argument;

public class GroupMobData 
{
	List<MobData> m_listMobData;
	Location m_spawnLocation;
	Location m_pointDirection;
	Vector m_velocity;
	boolean m_oneMob;
	Integer m_quantity;
	Integer m_rayon;
	Integer m_xp;
	
	public GroupMobData()
	{
		m_listMobData = new ArrayList<MobData>();
		m_spawnLocation = null;
		m_pointDirection = null;
		m_velocity = null;
		m_oneMob = false;
		m_quantity = null;
		m_rayon = null;
		m_xp = null;
	}
	
	public boolean moreThanOneMob()
	{
		return m_oneMob;
	}
	
	public void setSpawnLocation(Location spawnLocation)
	{
		m_spawnLocation = spawnLocation;
	}
	
	public Location getSpawnLocation()
	{
		return m_spawnLocation;
	}
	
	public Vector getVelocity()
	{
		return m_velocity;
	}
	
	public List<MobData> getListMobData()
	{
		return m_listMobData;
	}
	
	public Integer getQuantity()
	{
		return m_quantity;
	}
	
	public Integer getRadius()
	{
		return m_rayon;
	}
	
	public Integer getXp()
	{
		return m_xp;
	}
	
	public void setRadius(Integer radius)
	{
		m_rayon = radius;
	}
	
	public List<String> processing(UltraToolMain plugin, CommandSender sender, String arg[])
	{
		List<String> listError = new ArrayList<String>();
		MobData mobData = new MobData();
		
		for(int i = 0; i < arg.length; i++)
		{
			String key = Argument.getKey(arg[i]);
			String error = null;
			
			if(key != null)
			{
				key = key.toLowerCase();
				
				if(key.equals("o"))
				{
					if(Argument.getValue(arg[i]) != null)
					{
						if(m_oneMob)
						{
							m_listMobData.add(mobData);
							mobData = new MobData();
						}
						else
						{
							m_oneMob = true;
						}
					}
				}
				
				if(key.equals("p"))
				{
					m_spawnLocation = plugin.getListPoint().get(Argument.getValue(arg[i]), sender);
					
					if(m_spawnLocation == null)
						error = "'" + Argument.getValue(arg[i]) + "' n'est pas un point définie!";
				}
				else if(key.equals("q"))
				{
					Integer value = Argument.getValueInt(arg[i]);
					if(value != null)
					{
						Integer maxQuantity = plugin.getPluginConfig().getSpawnQuantityMax();
						if(value > maxQuantity)
						{
							error = "'" + Argument.getValue(arg[i]) + "' dépasse la limit fixer � " + maxQuantity;
							value = maxQuantity;
						}
						
						m_quantity = value;
					}
					else
						error = "'" + Argument.getValue(arg[i]) + "' n'est pas un entier valide";
				}
				else if(key.equals("r"))
				{
					Integer value = Argument.getValueInt(arg[i]);
					if(value != null)
						m_rayon = value;
					else
						error = "'" + Argument.getValue(arg[i]) + "' n'est pas un entier valide";
				}
				else if(key.equals("xp"))
				{
					Integer value = Argument.getValueInt(arg[i]);
					if(value != null)
						m_xp = value;
					else
						error = "'" + Argument.getValue(arg[i]) + "' n'est pas un entier valide";
				}
				else if(key.equals("velocity"))
				{
					String value = Argument.getValue(arg[i]);
					
					String[] vec = value.split(",");
					
					if(vec.length == 3)
					{
						Double x = Double.parseDouble(vec[0]);
						Double y = Double.parseDouble(vec[1]);
						Double z = Double.parseDouble(vec[2]);
						
						if(x != null && y != null && z != null)
						{
							m_velocity = new Vector(x, y, z);
						}
					}
					else if(vec.length == 1)
					{
						Location direction = plugin.getListPoint().get(value, sender);
						
						if(direction == null)
							error = "'" + value + "' n'est pas un point définie!";
						else
						{
							m_pointDirection = direction;
						}
					}
					else
					{
						error = "nombre d'argument incorrecte. Exemple: velocity:0,1,0";
					}
				}
				else
				{
					Player playerSender = null;
					if(sender instanceof Player) {
						playerSender = (Player)sender;
					}
					error = ProcessingMobData.process(plugin, mobData, arg[i], playerSender);
				}
				
				if(error != null)
					error = arg[i] + " -> " + error;
			}
			else
			{
				System.out.println("arg[i] : " + arg[i] + " - " + i);
				
				if(arg[i].equals("@e"))
				{
					if(m_spawnLocation != null)
					{
						List<Entity> listEntity = m_spawnLocation.getWorld().getEntities();
						Entity entityMin = null;
						Double distanceMin = null;
						
						for(int j = 0; j < listEntity.size(); j++)
						{
							Entity entity = listEntity.get(j);
							if(!entity.getType().equals(EntityType.PLAYER))
							{
								Location entityLoc = entity.getLocation();
								
								if(distanceMin == null || m_spawnLocation.distance(entityLoc) < distanceMin)
								{
									entityMin = entity;
									distanceMin = m_spawnLocation.distance(entityLoc);
								}
							}
						}
						
						if(entityMin != null)
						{
							if(m_oneMob)
							{
								m_listMobData.add(mobData);
								mobData = new MobData();
							}
							else
							{
								m_oneMob = true;
							}
							
							mobData.setEntity(entityMin);
						}
					}
				}
				else
				{
					List<Entity> listWorldEntity = m_spawnLocation.getWorld().getEntities();
					boolean findTheEntity = false;
					
					for(int entityId = 0; entityId < listWorldEntity.size(); entityId++)
					{
						Entity entity = listWorldEntity.get(entityId);
						
						if(entity.getUniqueId().toString().equals(arg[i]))
						{
							if(m_oneMob)
							{
								m_listMobData.add(mobData);
								mobData = new MobData();
							}
							else
							{
								m_oneMob = true;
							}
							
							mobData.setEntity(entity);
							
							findTheEntity = true;
						}
						else if(entity instanceof Player && ((Player)entity).getName().equals(arg[i]))
						{
							Player player = (Player)entity;
							
							boolean playerAlreadyDefine = false;
							
							for(int iEntity = 0; iEntity < m_listMobData.size(); iEntity++)
							{
								if(m_listMobData.get(iEntity).getPlayer() != null)
								{
									if(m_listMobData.get(iEntity).getPlayer().equals(player))
									{
										playerAlreadyDefine = true;
									}
								}
							}
							
							if(mobData.getPlayer() != null)
							{
								if(mobData.getPlayer().equals(player))
								{
									playerAlreadyDefine = true;
								}
							}
							
							if(!playerAlreadyDefine)
							{
								if(m_oneMob)
								{
									m_listMobData.add(mobData);
									mobData = new MobData();
								}
								else
								{
									m_oneMob = true;
								}
								
								mobData.setPlayer(player);
							}
							else
							{
								error = "'" + arg[i] + "' est définie plusieurs fois dans la commande";
							}
							
							findTheEntity = true;
						}
					}
					
					if(!findTheEntity)
					{
						error = "'" + arg[i] + "' ne respecte pas la syntaxe 'clef:value'";
					}
				}
			}
			
			if(error != null)
			{
				listError.add(error);
			}
		}
		m_listMobData.add(mobData);
		
		
		return listError;
	}
	
	public void compile()
	{
		if(m_pointDirection != null && m_spawnLocation != null)
		{
			double power = 1.0;
			if(m_listMobData.size() > 0)
			{
				Integer mobPow = m_listMobData.get(m_listMobData.size() - 1).getPower();
				
				if(mobPow != null)
					power = (double)mobPow;
			}
			
			m_velocity = m_pointDirection.subtract(m_spawnLocation).toVector();
			m_velocity.multiply(power/m_velocity.length());
			
			
		}
	}
	
	
}
