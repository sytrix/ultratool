package thecleaner.mob;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import thecleaner.UltraToolMain;
import thecleaner.fonction.Argument;
import thecleaner.fonction.ProcessString;
import thecleaner.mob.process.ProcessKit;

public class ProcessingMobData 
{
	public static String process(UltraToolMain plugin, MobData mobData, String argument, Player sender)
	{
		String key = Argument.getKey(argument);
		String error = null;
		
		if(key != null)
		{
			key = key.toLowerCase();
			
			if(key.equals("o"))
			{
				EntityType[] listEntity = EntityType.values();
				Integer integerEntity = Argument.getValueInt(argument);
				if(integerEntity != null)
				{
					int intEntity = integerEntity.intValue();
					if(intEntity >= 0 && intEntity < listEntity.length)
					{
						mobData.setEntityType(listEntity[intEntity]);
					}
					else
					{
						error = "cette id ne correspond à aucune entiter connue";
					}
				}
				
				String stringEntity = Argument.getValue(argument);
				if(stringEntity != null)
				{
					String entityName = stringEntity.toLowerCase();
					for(int i = 0; i < listEntity.length; i++)
					{
						if(listEntity[i].name().toLowerCase().equals(entityName))
						{
							mobData.setEntityType(listEntity[i]);
						}
					}
				}
				
				if(mobData.getEntity() == null || (mobData.getEntityType() == null && error == null))
				{
					//error = "la valeur: '" + stringEntity + "' ne correspond pas a une entit� ou r�ference vers une entit�";
				}
			}
			else if(key.equals("drop"))
			{
				ItemStack item[] = null;
				String value = Argument.getValue(argument);
				
				item = plugin.getListInv().get(value, sender);
				if(item != null)
					mobData.setInventoryName(value);
				
				if(item == null)
					error = "aucun inventaire et kit n'a de nom similaire � '" + value + "'";
			}
			else if(key.equals("k"))
			{
				String value = Argument.getValue(argument);
				error = ProcessKit.set(plugin, mobData, value, sender);
			}
			else if(key.equals("l"))
			{
				Double life = Argument.getValueDouble(argument);
				if(life != null)
					mobData.setLife(life);
				else
					error = "'" + Argument.getValue(argument) + "' n'est pas un nombre de vie valide";
			}
			else if(key.equals("d"))
			{
				Double damage = Argument.getValueDouble(argument);
				if(damage != null)
					mobData.setDamage(damage);
				else
					error = "'" + Argument.getValue(argument) + "' n'est pas un nombre de d�gats valide";
			}
			else if(key.equals("n"))
			{
				String name = Argument.getValue(argument);
				name = name.replace('&', '�');
				name = name.replace('_', ' ');
				mobData.setName(name);
			}
			else if(key.equals("ref"))
			{
				String dynamicPoint = Argument.getValue(argument);
				mobData.setDynamicPoint(dynamicPoint);
			}
			else if(key.equals("t"))
			{
				Integer valueInt = Argument.getValueInt(argument);
				if(valueInt != null)
				{
					mobData.setType(valueInt);
				}
				else
				{
					String valueString = Argument.getValue(argument);
					if(valueString.equals("?"))
						mobData.setType(-1);
					else 
						error = "'" + Argument.getValue(argument) + "' n'est pas un nombre entier";
					
				}
			}
			else if(key.equals("s"))
			{
				Integer valueInt = Argument.getValueInt(argument);
				if(valueInt != null)
				{
					mobData.setStyle(valueInt);
				}
				else
				{
					String valueString = Argument.getValue(argument);
					if(valueString.equals("?"))
						mobData.setStyle(-1);
					else 
						error = "'" + Argument.getValue(argument) + "' n'est pas un nombre entier";
					
				}
			}
			else if(key.equals("c"))
			{
				Integer valueInt = Argument.getValueInt(argument);
				if(valueInt != null)
				{
					mobData.setColor(valueInt);
				}
				else
				{
					String valueString = Argument.getValue(argument);
					if(valueString.equals("?"))
						mobData.setColor(-1);
					else 
						error = "'" + Argument.getValue(argument) + "' n'est pas un nombre entier";
					
				}
			}
			else if(key.equals("a"))
			{
				Integer value = Argument.getValueInt(argument);
				if(value != null)
					mobData.setEntityLead(value);
				else
					error = Argument.getValue(argument) + " n'est pas un nombre entier";
			}
			else if(key.equals("pow"))
			{
				Integer value = Argument.getValueInt(argument);
				if(value != null)
					mobData.setPower(value);
				else
					error = Argument.getValue(argument) + " n'est pas un nombre entier";
			}
			else if(key.equals("fire"))
			{
				Integer value = Argument.getValueInt(argument);
				if(value != null)
					mobData.setFire(value);
				else
					error = Argument.getValue(argument) + " n'est pas un nombre entier";
			}
			else if(key.equals("b"))
			{
				Byte valueByte = Argument.getValueByte(argument);
				
				if(valueByte != null)
				{
					mobData.setBaby(valueByte);
				}
				else
				{
					String value = Argument.getValue(argument);
					
					if(value.equals("?"))
					{
						valueByte = -1;
						mobData.setBaby(valueByte);
					}
					else
					{
						error = "'" + value + "' est une valeur invalide";
					}
				}
			}
			else if(key.equals("v"))
			{
				mobData.setHavePassenger(Argument.getValue(argument).equals("1"));
			}
			else if(key.equals("f"))
			{
				mobData.setFriend(Argument.getValue(argument).equals("1"));
			}
			else if(key.equals("i"))
			{
				mobData.setInvincible(Argument.getValue(argument).equals("1"));
			}
			else if(key.equals("passive"))
			{
				mobData.setPassive(Argument.getValue(argument).equals("1"));
			}
			/*
			else if(key.equals("m"))
			{
				String value = Argument.getValue(argument);
				error = ProcessMarchandise.set(plugin, mobData, value);
			}
			*/
			else if(key.equals("e"))
			{
				String effectValue = Argument.getValue(argument);
				String[] effectArg = effectValue.split(",");
				
				if(effectArg.length >= 1 && effectArg.length < 4)
				{
					PotionEffectType[] listPotionEffect = PotionEffectType.values();
					Integer effectId = null;
					Integer lvl = 1;
					Integer time = 17280000;
					
					effectId = ProcessString.parseInt(effectArg[0]);
					
					if(effectId == null) {
						String potionName = effectArg[0].toLowerCase();
						for(int i = 1; i < listPotionEffect.length; i++) {
							if(listPotionEffect[i].getName().toLowerCase().equals(potionName)) {
								effectId = i;
							}
						}
					}
					
					if(effectArg.length >= 2)
						lvl = ProcessString.parseInt(effectArg[1]);
					if(effectArg.length >= 3)
						time = ProcessString.parseInt(effectArg[2]);
					
					
					if(effectId != null)
					{
						if(effectId > 0 && effectId < listPotionEffect.length)
						{
							if(lvl != null)
							{
								if(time != null)
								{
									mobData.addEffectPotion(listPotionEffect[effectId].createEffect(time, lvl));
								}
								else
								{
									error = "le temps de l'effet n'est pas d�finie correctement";
								}
							}
							else
							{
								error = "le niveau de l'effect n'est pas d�finie correctement";
							}
						}
						else
						{
							error = "l'id ne correspond pas a un effet de potion";
						}
					}
					else
					{
						error = "l'id est incorrecte";
					}
				}
				else
				{
					error = "nombre d'argument incorrecte";
				}
				
			}
			else
			{
				error = "clef inconnue: " + key;
			}
		}
		
		if(error != null)
			return error;
		
		return null;
	}
}
