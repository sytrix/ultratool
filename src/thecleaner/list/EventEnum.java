package thecleaner.list;

import org.bukkit.event.Listener;

import thecleaner.UltraToolMain;

public enum EventEnum 
{
	PLUGIN_START, PLUGIN_STOP, ENTITY_DEATH, PLAYER_MESSAGE;
	
	public static int indexOf(String eventName)
	{
		eventName = eventName.toUpperCase();
		EventEnum[] listEvent = EventEnum.values();
		
		for(int i = 0; i < listEvent.length; i++)
		{
			if(eventName.equals(listEvent[i].toString()))
				return i;
		}
		
		return -1;
	}
	
	public static Listener getEvent(UltraToolMain plugin, String eventName)
	{
		eventName = eventName.toLowerCase();
		
		EventEnum event = get(eventName);
		
		if(event.equals(EventEnum.PLUGIN_START))
			return null;
		else if(event.equals(EventEnum.PLUGIN_STOP))
			return null;
		else if(event.equals(EventEnum.ENTITY_DEATH))
			return null;
		else if(event.equals(EventEnum.PLAYER_MESSAGE))
			return null;
		
		return null;
	}
	
	public static EventEnum get(String eventName)
	{
		int id = indexOf(eventName);
		
		if(id > -1)
			return EventEnum.values()[id];
			
		return null;
	}
	
	public static boolean contains(String eventName)
	{
		return get(eventName) != null;
	}
}
