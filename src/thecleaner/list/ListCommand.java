package thecleaner.list;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

import thecleaner.UltraToolMain;

public class ListCommand {
	
	private UltraToolMain m_plugin;
	private Map<String, Object> m_listCommandSlash;
	private Map<String, Object> m_listCommandSpawn;
	private Map<String, Object> m_listCommandNoSlash;
	private Map<String, Object> m_listInv;
	private Map<String, Object> m_listInvPlayer;
	private Map<String, Object> m_listInvPoint;
	private Map<String, Object> m_listKit;
	private Map<String, Object> m_listKitPlayer;
	private Map<String, Object> m_listPlayer;
	private Map<String, Object> m_listPoint;
	private Map<String, Object> m_listPointPlayer;
	private Map<String, Object> m_listPointPoint;
	private Map<String, Object> m_listTask;
	private Map<String, Object> m_listZone;
	private Map<String, Object> m_listZoneItem;
	private Map<String, Object> m_listZoneItemItem;
	
	private Map<String, Object> m_listMaterial;
	private Map<String, Object> m_listMaterialMaterial;
	
	public ListCommand(UltraToolMain plugin) {
		m_plugin = plugin;
		
		m_listCommandSlash = null;
		m_listCommandNoSlash = null;
		
		// variables
		m_listCommandSpawn = new HashMap<String, Object>();
		m_listInv = new HashMap<String, Object>();
		m_listInvPlayer = new HashMap<String, Object>();
		m_listInvPoint = new HashMap<String, Object>();
		m_listKit = new HashMap<String, Object>();
		m_listKitPlayer = new HashMap<String, Object>();
		m_listPlayer = new HashMap<String, Object>();
		m_listPoint = new HashMap<String, Object>();
		m_listPointPlayer = new HashMap<String, Object>();
		m_listPointPoint = new HashMap<String, Object>();
		m_listTask = new HashMap<String, Object>();
		m_listZone = new HashMap<String, Object>();
		m_listZoneItem = new HashMap<String, Object>();
		m_listZoneItemItem = new HashMap<String, Object>();
		
		// constantes
		m_listMaterial = new HashMap<String, Object>();
		m_listMaterialMaterial = new HashMap<String, Object>();
	}
	
	public void reloadCommand() {
		if(m_listCommandSlash == null) {
			m_listCommandSlash = new HashMap<String, Object>();
			m_listCommandNoSlash = new HashMap<String, Object>();
			
			List<String> listInv = m_plugin.getListInv().getListComplete();
			List<String> listKit = m_plugin.getListKit().getListComplete();
			List<String> listPlayer = this.listPlayer();
			List<String> listPoint = m_plugin.getListPoint().getListComplete();
			List<String> listTask = m_plugin.getListTask().getList();
			List<String> listZone = m_plugin.getListZone().getList();
			
			m_listMaterial = mapMaterials(null);
			m_listMaterialMaterial = mapMaterials(m_listMaterial);
			
			this.reloadListToMap(m_listInv, listInv, null);
			this.reloadListToMap(m_listInvPlayer, listInv, m_listPlayer);
			this.reloadListToMap(m_listInvPoint, listInv, m_listPoint);
			this.reloadListToMap(m_listKit, listKit, null);
			this.reloadListToMap(m_listKitPlayer, listKit, m_listPlayer);
			this.reloadListToMap(m_listPlayer, listPlayer, null);
			this.reloadListToMap(m_listPoint, listPoint, null);
			this.reloadListToMap(m_listPointPlayer, listPoint, m_listPlayer);
			this.reloadListToMap(m_listPointPoint, listPoint, m_listPoint);
			this.reloadListToMap(m_listTask, listTask, null);
			this.reloadListToMap(m_listZone, listZone, m_listPoint);
			this.reloadListToMap(m_listZoneItem, listPoint, m_listMaterial);
			this.reloadListToMap(m_listZoneItemItem, listPoint, m_listMaterialMaterial);
			
			
			this.reloadRootCommand();
		} else {
			List<String> listPlayer = this.listPlayer();
			
			this.reloadListToMap(m_listPlayer, listPlayer, null);
		}
	}
	
	public Map<String, Object> getSlashCommand() {
		return m_listCommandSlash;
	}
	
	public Map<String, Object> getNoSlashCommand() {
		return m_listCommandNoSlash;
	}
	
	public void updateInv() {
		List<String> listInv = m_plugin.getListInv().getListComplete();
		
		this.reloadListToMap(m_listInv, listInv, null);
		this.reloadListToMap(m_listInvPlayer, listInv, m_listPlayer);
		this.reloadListToMap(m_listInvPoint, listInv, m_listPoint);
		
		this.reloadRootCommand();
	}
	
	public void updateKit() {
		List<String> listKit = m_plugin.getListKit().getListComplete();
		
		this.reloadListToMap(m_listKit, listKit, null);
		this.reloadListToMap(m_listKitPlayer, listKit, m_listPlayer);
		
		this.updateInv();
	}
	
	public void updatePoint() {
		List<String> listPoint = m_plugin.getListPoint().getListComplete();
		
		this.reloadListToMap(m_listPoint, listPoint, null);
		this.reloadListToMap(m_listPointPlayer, listPoint, m_listPlayer);
		this.reloadListToMap(m_listPointPoint, listPoint, m_listPoint);
		this.reloadListToMap(m_listZoneItem, listPoint, m_listMaterial);
		this.reloadListToMap(m_listZoneItemItem, listPoint, m_listMaterialMaterial);
		
		this.reloadRootCommand();
	}
	
	public void updateTask() {
		List<String> listTask = m_plugin.getListTask().getList();
		
		this.reloadListToMap(m_listTask, listTask, null);
	}
	
	public void updateZone() {
		List<String> listZone = m_plugin.getListZone().getList();
		
		this.reloadListToMap(m_listZone, listZone, m_listPoint);
		
		this.reloadRootCommand();
	}
	
	private void reloadListToMap(Map<String, Object> listCommand, List<String> list, Object dest)
	{
		listCommand.clear();
		
		Iterator<String> iterator = list.iterator();
		
		while(iterator.hasNext()) {
			listCommand.put(iterator.next(), dest);
		}
	}
	
	private List<String> listPlayer()
	{
		Iterator<? extends Player> iterator = m_plugin.getServer().getOnlinePlayers().iterator();
		List<String> players = new ArrayList<>();
		
		while(iterator.hasNext()) {
			Player player = iterator.next();
			players.add(player.getName());
		}
		
		return players;
	}
	
	private Map<String, Object> mapMaterials(Object dest)
	{
		Map<String, Object> listCommand = new HashMap<String, Object>();
		Material[] materials = Material.values();
		
		for(int i = 0; i < materials.length; i++) {
			listCommand.put(materials[i].name().toLowerCase(), dest);
		}
		
		return listCommand;
	}
	
	private Map<String, Object> mapItemEnchant(Object dest)
	{
		Map<String, Object> listCommand = new HashMap<String, Object>();
		Enchantment[] enchants = Enchantment.values();
		
		for(int i = 0; i < enchants.length; i++) {
			listCommand.put(enchants[i].getName().toLowerCase(), dest);
		}
		
		return listCommand;
	}
	
	private Map<String, Object> mapSounds(Object dest)
	{
		Map<String, Object> listCommand = new HashMap<String, Object>();
		Sound[] enchants = Sound.values();
		
		for(int i = 0; i < enchants.length; i++) {
			listCommand.put(enchants[i].name().toLowerCase(), dest);
		}
		
		return listCommand;
	}
	
	private Map<String, Object> mapPotionEffects(Object dest)
	{
		Map<String, Object> listCommand = new HashMap<String, Object>();
		PotionEffectType[] potionEffects = PotionEffectType.values();
		
		for(int i = 1; i < potionEffects.length; i++) {
			listCommand.put(potionEffects[i].getName().toLowerCase(), dest);
		}
		
		return listCommand;
	}
	
	private Map<String, Object> listCommandCompass()
	{
		Map<String, Object> listCommand = new HashMap<String, Object>();

		Map<String, Object> listPointsPlayers = m_listPointPlayer;
		
		listCommand.put("set", listPointsPlayers);
		
		return listCommand;
	}
	
	private Map<String, Object> listCommandEffect()
	{
		Map<String, Object> listCommand = new HashMap<String, Object>();

		Map<String, Object> listEffects = mapPotionEffects(null);
		
		listCommand.put("add", listEffects);
		listCommand.put("remove", listEffects);
		listCommand.put("reset", m_listPlayer);
		listCommand.put("list", null);
		
		return listCommand;
	}
	
	private Map<String, Object> listCommandInventory()
	{
		Map<String, Object> listCommand = new HashMap<String, Object>();
		
		Map<String, Object> listInv = m_listInv;
		Map<String, Object> listInvPoints = m_listInvPoint;
		Map<String, Object> listInvPlayers = m_listInvPlayer;
		
		listCommand.put("chestReplace", listInvPoints);
		listCommand.put("chestDrop", listInvPoints);
		listCommand.put("chestClear", listInv);
		listCommand.put("itemsDrop", listInvPoints);
		listCommand.put("del", listInv);
		listCommand.put("give", listInvPlayers);
		listCommand.put("get", listInv);
		listCommand.put("set", listInv);
		listCommand.put("list", null);
		
		return listCommand;
	}
	
	private Map<String, Object> listCommandItem()
	{
		Map<String, Object> listCommand = new HashMap<String, Object>();

		
		listCommand.put("id", null);
		listCommand.put("hat", null);
		listCommand.put("head", m_listPlayer);
		listCommand.put("give", mapMaterials(null));
		listCommand.put("copy", null);
		listCommand.put("name", null);
		listCommand.put("lore", null);
		listCommand.put("enchant", mapItemEnchant(null));
		
		return listCommand;
	}
	
	private Map<String, Object> listCommandKit()
	{
		Map<String, Object> listCommand = new HashMap<String, Object>();
		
		Map<String, Object> listKits = m_listKit;
		Map<String, Object> listKitsPlayers = m_listKitPlayer;
		
		listCommand.put("del", listKits);
		listCommand.put("get", listKits);
		listCommand.put("give", listKitsPlayers);
		listCommand.put("list", null);
		listCommand.put("set", listKits);
		
		return listCommand;
	}
	
	private Map<String, Object> listCommandList()
	{
		Map<String, Object> listCommand = new HashMap<String, Object>();

		listCommand.put("command", null);
		listCommand.put("enchantment", null);
		listCommand.put("sound", null);
		listCommand.put("effect", null);
		listCommand.put("entity", null);
		listCommand.put("point", null);
		listCommand.put("kit", null);
		listCommand.put("potionEffect", null);
		listCommand.put("potion", null);
		listCommand.put("color", null);
		listCommand.put("zombie", null);
		listCommand.put("skeleton", null);
		listCommand.put("horsevariant", null);
		listCommand.put("horsecolor", null);
		listCommand.put("horsestyle", null);
		listCommand.put("villager", null);
		listCommand.put("item", null);
		listCommand.put("block", null);
		
		return listCommand;
	}
	
	private Map<String, Object> listCommandPoint()
	{
		Map<String, Object> listCommand = new HashMap<String, Object>();

		Map<String, Object> listPoints = m_listPoint;
		Map<String, Object> listPointsPoints = m_listPointPoint;
		Map<String, Object> listPointsPlayers = m_listPointPlayer;
		
		listCommand.put("del", listPoints);
		listCommand.put("set", listPointsPoints);
		listCommand.put("effect", listPoints);
		listCommand.put("explosion", listPoints);
		listCommand.put("move", listPoints);
		listCommand.put("list", null);
		listCommand.put("listEffect", null);
		listCommand.put("setBlock", listPoints);
		listCommand.put("tp", listPointsPlayers);
		listCommand.put("trace", listPoints);
		listCommand.put("write", listPoints);
		
		return listCommand;
	}
	
	private Map<String, Object> listCommandSound()
	{
		Map<String, Object> listCommand = new HashMap<String, Object>();
		
		listCommand.put("play", mapSounds(m_listPlayer));
		listCommand.put("list", null);
		
		return listCommand;
	}
	
	private Map<String, Object> listCommandTask()
	{
		Map<String, Object> listCommand = new HashMap<String, Object>();
		
		Map<String, Object> listTasks = m_listTask;
		
		listCommand.put("set", listTasks);
		listCommand.put("del", listTasks);
		listCommand.put("info", listTasks);
		listCommand.put("list", null);
		listCommand.put("startRepeat", listTasks);
		listCommand.put("stopRepeat", listTasks);
		
		return listCommand;
	}
	
	private Map<String, Object> listCommandTool()
	{
		Map<String, Object> listCommand = new HashMap<String, Object>();
		
		listCommand.put("aspiration", null);
		listCommand.put("repulsion", null);
		
		return listCommand;
	}
	
	private Map<String, Object> listCommandZone()
	{
		Map<String, Object> listCommand = new HashMap<String, Object>();
		
		Map<String, Object> listZones = m_listZone;
		Map<String, Object> listZonesItems = m_listZoneItem;
		Map<String, Object> listZonesItemsItems = m_listZoneItemItem;
		
		listCommand.put("set", listZones);
		listCommand.put("move", listZones);
		listCommand.put("setBlock", listZonesItems);
		listCommand.put("replaceBlock", listZonesItemsItems);
		listCommand.put("moveBlock", listZones);
		listCommand.put("saveBlock", listZones);
		listCommand.put("loadBlock", listZones);
		listCommand.put("list", null);
		listCommand.put("del", listZones);
		listCommand.put("info", listZones);
		listCommand.put("tool", null);
		
		return listCommand;
	}
	
	private void listCommandSpawn(Map<String, Object> listCommand)
	{
		listCommand.clear();
		
		EntityType[] entities = EntityType.values();
		for(EntityType e : entities) {
			listCommand.put("o:" + e.name().toLowerCase(), listCommand);
		}
		
		PotionEffectType[] potionEffect = PotionEffectType.values();
		for(PotionEffectType p : potionEffect) {
			if(p != null) {
				listCommand.put("e:" + p.getName().toLowerCase(), listCommand);
			}
		}
		
		List<String> listKit = m_plugin.getListKit().getListComplete();
		for(String k : listKit) {
			listCommand.put("k:" + k.toLowerCase(), listCommand);
		}
		
		List<String> listInv = m_plugin.getListInv().getListComplete();
		for(String i : listInv) {
			listCommand.put("drop:" + i.toLowerCase(), listCommand);
		}
		
		List<String> listPoints = m_plugin.getListPoint().getListComplete();
		for(String p : listPoints) {
			listCommand.put("p:" + p.toLowerCase(), listCommand);
			listCommand.put("velocity:" + p.toLowerCase(), listCommand);
		}
		
		List<String> listPlayers = this.listPlayer();
		for(String p : listPlayers) {
			listCommand.put(p, listCommand);
		}
		
		
		listCommand.put("l:", listCommand);
		listCommand.put("d:", listCommand);
		listCommand.put("n:", listCommand);
		listCommand.put("t:", listCommand);
		listCommand.put("s:", listCommand);
		listCommand.put("c:", listCommand);
		listCommand.put("a:", listCommand);
		listCommand.put("pow:", listCommand);
		listCommand.put("ref:", listCommand);
		listCommand.put("b:0", listCommand);
		listCommand.put("b:1", listCommand);
		listCommand.put("v:0", listCommand);
		listCommand.put("v:1", listCommand);
		listCommand.put("f:0", listCommand);
		listCommand.put("f:1", listCommand);
		listCommand.put("fire:", listCommand);
		listCommand.put("i:0", listCommand);
		listCommand.put("i:1", listCommand);
		listCommand.put("passive:0", listCommand);
		listCommand.put("passive:1", listCommand);
		listCommand.put("q:", listCommand);
		listCommand.put("r:", listCommand);
		listCommand.put("xp:", listCommand);
		listCommand.put("velocity:", listCommand);
	}
	
	private void reloadRootCommand()
	{
		listCommandSpawn(m_listCommandSpawn);
		
		m_listCommandNoSlash.put("ubutcher", m_listCommandSpawn);
		m_listCommandNoSlash.put("ucompass", listCommandCompass());
		m_listCommandNoSlash.put("ueffect", listCommandEffect());
		m_listCommandNoSlash.put("uexe", m_listCommandNoSlash);
		m_listCommandNoSlash.put("uinv", listCommandInventory());
		m_listCommandNoSlash.put("uitem", listCommandItem());
		m_listCommandNoSlash.put("ulist", listCommandList());
		m_listCommandNoSlash.put("ultratool", null);
		m_listCommandNoSlash.put("upoint", listCommandPoint());
		m_listCommandNoSlash.put("ukit", listCommandKit());
		m_listCommandNoSlash.put("usound", listCommandSound());
		m_listCommandNoSlash.put("uspawn", m_listCommandSpawn);
		m_listCommandNoSlash.put("utask", listCommandTask());
		m_listCommandNoSlash.put("utool", listCommandTool());
		m_listCommandNoSlash.put("uzone", listCommandZone());
		
		Iterator<String> iterator = m_listCommandNoSlash.keySet().iterator();
		
		while(iterator.hasNext()) {
			String cmd = iterator.next();
			m_listCommandSlash.put("/" + cmd, m_listCommandNoSlash.get(cmd));
		}
	}
}
