package thecleaner.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;

import thecleaner.UltraToolMain;
import thecleaner.file.FileCuboid;
import thecleaner.worldedit.Cuboid;

public class ListZone 
{
	UltraToolMain m_plugin;
	Map<String, Cuboid> m_listZone;
	
	public ListZone(UltraToolMain plugin)
	{
		m_plugin = plugin;
		m_listZone = FileCuboid.Load(m_plugin);
	}
	
	public boolean contains(String key)
	{
		if(key.equals("~")) {
			return false;
		}
		
		return m_listZone.containsKey(key.toLowerCase());
	}
	
	public void put(String key, Cuboid cuboid)
	{
		if(key.equals("~")) {
			return;
		}
		
		key = key.toLowerCase();
		
		if(m_listZone.containsKey(key))
		{
			FileCuboid.saveCuboid(m_plugin, key, cuboid);
		}
		else
		{
			FileCuboid.saveNewCuboid(m_plugin, key, cuboid);
		}
		
		m_listZone.put(key, cuboid);
		m_plugin.getListCommand().updateZone();
	}
	
	public Cuboid get(String key, Player player)
	{
		if(key.equals("~") && player != null) {
			Cuboid cube = m_plugin.getListPlayerCuboid().get(player);
			return cube;
		}
		
		key = key.toLowerCase();
		
		if(m_listZone.containsKey(key))
			return m_listZone.get(key);
		
		return null;
	}
	
	public void remove(String key)
	{
		if(key.equals("~")) {
			return;
		}
		
		key = key.toLowerCase();
		
		m_listZone.remove(key);
		FileCuboid.delPoint(m_plugin, m_listZone);
		m_plugin.getListCommand().updateZone();
	}
	
	public List<String> getList()
	{
		List<String> listZoneName = new ArrayList<String>();
		Iterator<String> locationList = m_listZone.keySet().iterator();
		
		listZoneName.add("~");
		
		while(locationList.hasNext())
		{
			listZoneName.add(locationList.next());
		}
		
		return listZoneName;
	}
	
	public void reload()
	{
		m_listZone = FileCuboid.Load(m_plugin);
		m_plugin.getListCommand().updateZone();
	}
}


