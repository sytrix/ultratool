package thecleaner.list;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.entity.Entity;

import thecleaner.UltraToolMain;

public class ListRefEntity
{
	UltraToolMain m_plugin;
	Map<String, Entity> m_map;
	
	public ListRefEntity(UltraToolMain plugin)
	{
		m_plugin = plugin;
		m_map = new Hashtable<String, Entity>();
	}
	
	public void put(String key, Entity entity)
	{
		m_map.put(key, entity);
	}
	
	public boolean contains(String key)
	{
		if(m_map.containsKey(key))
		{
			Entity entity = m_map.get(key);
			
			if(entity.isDead())
			{
				m_map.remove(key);
				return false;
			}
			
			return true;
		}
		
		return false;
	}
	
	public Entity get(String key)
	{
		if(m_map.containsKey(key))
		{
			Entity entity = m_map.get(key);
			
			if(entity.isDead())
			{
				m_map.remove(key);
				return null;
			}
			
			return m_map.get(key);
		}
		
		return null;
	}
	
	public void updateList()
	{
		Iterator<Entry<String, Entity>> iterator = m_map.entrySet().iterator();
		
		while(iterator.hasNext())
		{
			Entry<String, Entity> entry = iterator.next();
			Entity entity = entry.getValue();
			String key = entry.getKey();
			
			if(entity.isDead())
			{
				m_map.remove(key);
			}
		}
	}
	
	public Set<Entry<String, Entity>> entrySet()
	{
		updateList();
		return m_map.entrySet();
	}
	
	public Set<String> keySet()
	{
		updateList();
		return m_map.keySet();
	}
}
