package thecleaner.list;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.LivingEntity;

public class ListEntityXp 
{
	private List<List<LivingEntity>> m_listListEntity;
	private List<Integer> m_listXp;
	
	public ListEntityXp()
	{
		m_listListEntity = new ArrayList<List<LivingEntity>>();
		m_listXp = new ArrayList<Integer>();
	}
	
	public void add(List<LivingEntity> listEntity, Integer xp)
	{
		m_listListEntity.add(listEntity);
		m_listXp.add(xp);
	}
	
	public void remove(LivingEntity entity)
	{
		for(int i = 0; i < m_listListEntity.size(); i++)
		{
			if(m_listListEntity.get(i).equals(entity))
			{
				m_listListEntity.remove(i);
				m_listXp.remove(i);
				return;
			}
		}
	}
	
	public int size()
	{
		return m_listListEntity.size();
	}
	
	public List<LivingEntity> getGroup(int i)
	{
		return m_listListEntity.get(i);
	}
	
	public Integer getValue(int i)
	{
		return m_listXp.get(i);
	}
	
	public Integer getXp(LivingEntity entity)
	{
		for(int i = 0; i < m_listListEntity.size(); i++)
		{
			List<LivingEntity> listEntity = m_listListEntity.get(i);
			boolean entityExist = false;
			
			for(int j = 0; j < listEntity.size(); j++)
			{
				if(listEntity.get(j).equals(entity))
				{
					entityExist = true;
				}
				
				if(listEntity.get(j).isDead())
				{
					listEntity.remove(j);
					j--;
				}
			}
			
			if(listEntity.isEmpty())
			{
				if(entityExist)
				{
					int xp = m_listXp.get(i);
					m_listListEntity.remove(i);
					m_listXp.remove(i);
					return xp;
				}
				else
				{
					m_listListEntity.remove(i);
					m_listXp.remove(i);
				}
			}
		}
		
		return null;
	}
	
}
