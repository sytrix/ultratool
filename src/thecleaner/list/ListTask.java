package thecleaner.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Location;

import thecleaner.UltraToolMain;
import thecleaner.file.FileTask;
import thecleaner.message.ChatMessage;

public class ListTask 
{
	private UltraToolMain m_plugin;
	private List<String> m_listKey;
	private List<String> m_listCommand;
	private List<Integer> m_listTime;
	private List<Location> m_listLocation;
	private List<String> m_listStackTrace;
	private List<String> m_listLastMessage;
	private List<List<String>> m_listListEvent;
	private boolean m_isLoad;
	
	public ListTask(UltraToolMain plugin)
	{
		m_plugin = plugin;
		m_listKey = new ArrayList<String>();
		m_listCommand = new ArrayList<String>();
		m_listTime = new ArrayList<Integer>();
		m_listLocation = new ArrayList<Location>();
		m_listStackTrace = new ArrayList<String>();
		m_listLastMessage = new ArrayList<String>();
		m_listListEvent = new ArrayList<List<String>>();
		m_isLoad = false;
		
		FileTask.load(m_plugin, this);
		
		m_isLoad = true;
	}
	
	public List<String> getList()
	{
		List<String> listTaskName = new ArrayList<String>();
		Iterator<String> iteratorTask = m_listKey.iterator();
		
		while(iteratorTask.hasNext())
		{
			listTaskName.add(iteratorTask.next());
		}
		
		return listTaskName;
	}
	
	public boolean contains(String key)
	{
		key = key.toLowerCase();
		
		return m_listKey.contains(key);
	}
	
	public void put(String key, Location location, String command)
	{
		key = key.toLowerCase();
		
		if(m_listKey.contains(key))
		{
			Integer id = m_listKey.indexOf(key);
			
			m_listCommand.set(id, command);
		}
		else
		{
			m_listKey.add(key);
			
			m_listCommand.add(command);
			m_listTime.add(null);
			m_listLocation.add(location);
			m_listStackTrace.add("");
			m_listLastMessage.add("");
			m_listListEvent.add(new ArrayList<String>());
		}
		
		
		
		if(m_isLoad) {
			FileTask.save(m_plugin);
			m_plugin.getListCommand().updateTask();
		}
	}
	
	public void setRepeat(String key, Integer tick)
	{
		key = key.toLowerCase();
		
		if(m_listKey.contains(key))
		{
			Integer id = m_listKey.indexOf(key);
			m_listTime.set(id, tick);
		}
		
		if(m_isLoad) {
			FileTask.save(m_plugin);
		}
	}
	
	public void addEvent(String key, String eventName)
	{
		key = key.toLowerCase();
		eventName = eventName.toUpperCase();
		
		if(m_listKey.contains(key))
		{
			Integer id = m_listKey.indexOf(key);
			
			if(!m_listListEvent.get(id).contains(eventName))
				m_listListEvent.get(id).add(eventName);
		}
		
		if(m_isLoad) {
			FileTask.save(m_plugin);
		}
	}
	
	public void removeEvent(String key, String eventName)
	{
		key = key.toLowerCase();
		eventName = eventName.toUpperCase();
		
		if(m_listKey.contains(key))
		{
			Integer id = m_listKey.indexOf(key);
			m_listListEvent.get(id).remove(eventName);
		}
		
		if(m_isLoad) {
			FileTask.save(m_plugin);
		}
	}
	
	public Iterator<String> iterator()
	{
		return m_listKey.iterator();
	}
	
	public String getCommand(String key)
	{
		key = key.toLowerCase();
		
		if(m_listKey.contains(key))
			return m_listCommand.get(m_listKey.indexOf(key));
		
		return null;
	}
	
	public Integer getTick(String key)
	{
		key = key.toLowerCase();
		
		if(m_listKey.contains(key))
			return m_listTime.get(m_listKey.indexOf(key));
		
		return null;
	}
	
	public Location getLocation(String key)
	{
		key = key.toLowerCase();
		
		if(m_listKey.contains(key))
			return m_listLocation.get(m_listKey.indexOf(key));
		
		return null;
	}
	
	public String getLastMessage(String key)
	{
		key = key.toLowerCase();
		
		if(m_listKey.contains(key))
			return m_listLastMessage.get(m_listKey.indexOf(key));
		
		return null;
	}
	
	public String getStackTrace(String key)
	{
		key = key.toLowerCase();
		
		if(m_listKey.contains(key))
			return m_listStackTrace.get(m_listKey.indexOf(key));
		
		return null;
	}
	
	public List<String> getEvents(String key)
	{
		key = key.toLowerCase();
		
		if(m_listKey.contains(key))
			return m_listListEvent.get(m_listKey.indexOf(key));
		
		return null;
	}
	
	public void setLastMessage(String key, String message)
	{
		key = key.toLowerCase();
		
		if(m_listKey.contains(key))
		{
			m_listLastMessage.set(m_listKey.indexOf(key), message);
		}
	}
	
	public void setStackTrace(String key, String stackTrace)
	{
		key = key.toLowerCase();
		
		if(m_listKey.contains(key))
		{
			m_listStackTrace.set(m_listKey.indexOf(key), stackTrace);
		}
	}
	
	public void del(String key)
	{
		key = key.toLowerCase();
		
		int id = m_listKey.indexOf(key);
		
		m_listKey.remove(id);
		m_listCommand.remove(id);
		m_listTime.remove(id);
		m_listLocation.remove(id);
		m_listStackTrace.remove(id);
		m_listLastMessage.remove(id);
		m_listListEvent.remove(id);
		
		if(m_isLoad) {
			FileTask.save(m_plugin);
			m_plugin.getListCommand().updateTask();
		}
	}
	
	public String getListFormat()
	{
		String listRepetition = "";
		
		for(int i = 0; i < m_listKey.size(); i++)
		{
			Integer time = m_listTime.get(i);
			String stackTrace = m_listStackTrace.get(i);
			String message = m_listLastMessage.get(i);
			
			if(message.equals("") && stackTrace.equals(""))
				listRepetition += ChatMessage.info("");
			else
				listRepetition += ChatMessage.error("");
			
			listRepetition += m_listKey.get(i);
			
			if(time != null && time > 0)
				listRepetition += ChatMessage.show("(" + time + "t) ; ");
			else
				listRepetition += ChatMessage.show("(#NULL) ; ");
		}
		
		return listRepetition;
	}
	
}
