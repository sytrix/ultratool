package thecleaner.list;

import thecleaner.UltraToolMain;

public class ListEvent 
{
	private UltraToolMain m_plugin;
	private boolean[] m_listEventEnable;
	
	public ListEvent(UltraToolMain plugin)
	{
		int nbEvent = EventEnum.values().length;
		
		m_listEventEnable = new boolean[nbEvent];
		
		for(int i = 0; i < nbEvent; i++)
		{
			m_listEventEnable[i] = false;
		}
		
		m_plugin = plugin;
	}
	
	public void enable(String key)
	{
		if(EventEnum.contains(key))
		{
			int id = EventEnum.indexOf(key);
			
			if(!m_listEventEnable[id])
			{
				m_listEventEnable[id] = true;
				
				m_plugin.getServer().getPluginManager().registerEvents(
						EventEnum.getEvent(m_plugin, key), m_plugin);
			}
		}
	}
}
