package thecleaner.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.CommandBlock;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import thecleaner.UltraToolMain;
import thecleaner.file.FilePoint;
import thecleaner.fonction.ProcessString;
import thecleaner.message.ChatMessage;


public class ListPoint
{
	UltraToolMain m_plugin;
	Map<String, Location> m_point;
	
	public ListPoint(UltraToolMain plugin)
	{
		m_plugin = plugin;
		m_point = FilePoint.Load(m_plugin);
	}
	
	public boolean contains(String key)
	{
		if(key.equals("~")) {
			return false;
		}
		
		return m_point.containsKey(key.toLowerCase());
	}
	
	public Location get(String key, CommandSender sender)
	{
		if(key.equals("~") && sender != null) {
			if(sender instanceof Player) {
				Player player = (Player)sender;
				return player.getLocation();
			} else if(sender instanceof CommandBlock) {
				CommandBlock commandBlock = (CommandBlock)sender;
				return commandBlock.getBlock().getLocation();
			} else {
				return null;
			}
		} 
		else if(key.startsWith("!"))
		{
			key = key.replace("!", "");
			World world = null;
			String[] list = key.split(",");
			Double[] coord = new Double[3];
			
			if(list.length != 4)
				return null;
			
			world = m_plugin.getServer().getWorld(list[0]);
			
			if(world == null)
				return null;
			
			for(int i = 0; i < 3; i++)
			{
				coord[i] = ProcessString.parseDouble(list[i + 1]);
				
				if(coord[i] == null)
					return null;
			}
			
			return new Location(world, coord[0], coord[1], coord[2]);
		}
		
		key = key.toLowerCase();
		
		Entity entity = m_plugin.getListEntityProperty().getEntityByRef(key);
		
		if(entity != null)
			return entity.getLocation();
		
		Location point = m_point.get(key);
		
		if(point != null)
			return point.clone();
		
		return null;
	}
	
	public void set(String key, Location loc)
	{
		if(key.equals("~")) {
			return;
		}
		
		key = key.toLowerCase();
		
		if(m_point.containsKey(key))
			FilePoint.savePoint(m_plugin, key, loc);
		else
			FilePoint.saveNewPoint(m_plugin, key, loc);
		
		m_point.put(key, loc);
		m_plugin.getListCommand().updatePoint();
	}
	
	public void set(String key, String keyLoc)
	{
		if(key.equals("~")) {
			return;
		}
		
		key = key.toLowerCase();
		keyLoc = keyLoc.toLowerCase();
		
		if(m_point.containsKey(keyLoc))
		{
			Location location = m_point.get(keyLoc);
			
			if(location != null)
			{
				if(m_point.containsKey(key))
					FilePoint.savePoint(m_plugin, key, location);
				else
					FilePoint.saveNewPoint(m_plugin, key, location);
				
				m_point.put(key, location);
				m_plugin.getListCommand().updatePoint();
			}
		}
	}
	
	public List<String> getList()
	{
		List<String> listPointName = new ArrayList<String>();
		Iterator<String> locationList = m_point.keySet().iterator();
		
		listPointName.add("~");
		
		while(locationList.hasNext())
		{
			listPointName.add(locationList.next());
		}
		
		return listPointName;
	}
	
	public List<String> getListComplete()
	{
		List<String> listPointName = this.getList();
		Iterator<String> locationList = m_plugin.getListEntityProperty().getIteratorReferences();
		
		listPointName.add("~");
		
		while(locationList.hasNext())
		{
			listPointName.add(locationList.next());
		}
		
		return listPointName;
	}
	
	public String getListFormat()
	{
		String listPoint = "";
		Iterator<String> locationList = m_point.keySet().iterator();
		Iterator<String> locationEntityList = m_plugin.getListEntityProperty().getIteratorReferences();
		
		listPoint += ChatMessage.info("~ ; ");
		
		while(locationList.hasNext())
		{
			listPoint += locationList.next() + " ; ";
		}
		
		listPoint += ChatMessage.show("");
		
		while(locationEntityList.hasNext())
		{
			listPoint += locationEntityList.next() + " ; ";
		}
		
		return listPoint;
	}
	
	public void remove(String key)
	{
		if(key.equals("~")) {
			return;
		}
		
		key = key.toLowerCase();
		
		if(m_point.containsKey(key))
		{
			m_point.remove(key);
			FilePoint.delPoint(m_plugin, m_point);
			m_plugin.getListCommand().updatePoint();
		}
	}
	
	public void reload()
	{
		m_point = FilePoint.Load(m_plugin);
		m_plugin.getListCommand().updatePoint();
	}
}




