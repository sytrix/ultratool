package thecleaner.list;

import java.util.ArrayList;
import java.util.List;

public class ListChar 
{
	private List<String> m_alpha;
	private List<String> m_number;
	private List<String> m_charSpecial;
	private String m_listCharSpecial;
	private int m_width;
	private int m_height;
	
	public ListChar()
	{
		m_alpha = new ArrayList<String>();
		m_number = new ArrayList<String>();
		m_charSpecial = new ArrayList<String>();
		m_listCharSpecial = "";
		m_width = 0;
		m_height = 0;
	}
	
	public void setAlpha(List<String> alpha)
	{
		m_alpha = alpha;
	}
	
	public void setNumber(List<String> number)
	{
		m_number = number;
	}
	
	public void setCharSpecial(List<String> charSpecial, String listCharSpecial)
	{
		m_charSpecial = charSpecial;
		m_listCharSpecial = listCharSpecial;
	}
	
	public void setSize(int width, int height)
	{
		m_width = width;
		m_height = height;
	}
	
	public List<List<Boolean>> getText(String text)
	{
		List<List<Boolean>> construction = new ArrayList<List<Boolean>>();
		
		for(int i = 0; i < m_height; i++)
		{
			construction.add(new ArrayList<Boolean>());
		}
		
		text = text.toLowerCase();
		
		for(int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			
			if(c >= 'a' && c <= 'z')
			{
				int pointeur = (c - 'a') * m_width;
				
				for(int x = 0; x < m_width; x++)
				{
					for(int y = 0; y < m_height; y++)
					{
						construction.get(y).add(
								m_alpha.get(y).charAt(pointeur + x) != ' ');
					}
				}
				
			}
			else if(c >= '0' && c <= '9')
			{
				int pointeur = (c - '0') * m_width;
				
				for(int x = 0; x < m_width; x++)
				{
					for(int y = 0; y < m_height; y++)
					{
						construction.get(y).add(
								m_number.get(y).charAt(pointeur + x) != ' ');
					}
				}
				
			}
			else if(m_listCharSpecial.contains(c + ""))
			{
				int pointeur = m_listCharSpecial.indexOf(c + "") * m_width;
				
				for(int x = 0; x < m_width; x++)
				{
					for(int y = 0; y < m_height; y++)
					{
						construction.get(y).add(
								m_charSpecial.get(y).charAt(pointeur + x) != ' ');
					}
				}
				
			}
			else
			{
				for(int x = 0; x < m_width; x++)
				{
					for(int y = 0; y < m_height; y++)
					{
						construction.get(y).add(false);
					}
				}
			}
			
			for(int y = 0; y < m_height; y++)
			{
				construction.get(y).add(false);
			}
		}
		
		return construction;
	}
}
