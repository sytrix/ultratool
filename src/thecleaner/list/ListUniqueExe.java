package thecleaner.list;

import java.util.ArrayList;
import java.util.List;

public class ListUniqueExe 
{
	private List<String> m_list;
	
	public ListUniqueExe()
	{
		m_list = new ArrayList<String>();
	}
	
	public void put(String name)
	{
		name = name.toLowerCase();
		
		if(!m_list.contains(name))
			m_list.add(name);
	}
	
	public boolean contain(String name)
	{
		name = name.toLowerCase();
		
		return m_list.contains(name);
	}
	
	public void remove(String name)
	{
		name = name.toLowerCase();
		
		m_list.remove(name);
	}
}
