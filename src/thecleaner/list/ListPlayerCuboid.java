package thecleaner.list;

import java.util.Hashtable;
import java.util.Map;

import org.bukkit.entity.Player;

import thecleaner.worldedit.Cuboid;

public class ListPlayerCuboid 
{
	Map<Player, Cuboid> m_listCuboid;
	
	public ListPlayerCuboid()
	{
		m_listCuboid = new Hashtable<Player, Cuboid>();
	}
	
	public void put(Player player, Cuboid cuboid)
	{
		m_listCuboid.put(player, cuboid);
	}
	
	public Cuboid get(Player player)
	{
		if(m_listCuboid.containsKey(player))
			return m_listCuboid.get(player);
		
		return null;
	}
	
	public boolean contains(Player player)
	{
		return m_listCuboid.containsKey(player);
	}
	
	
}


