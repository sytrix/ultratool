package thecleaner.list;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import thecleaner.fonction.CheckMethod;
import thecleaner.fonction.ProcessString;

public class ListDefaultKit 
{
	public static String getListFormat()
	{
		String listKit = "";
		DefaultKitEnum[] listDefaultKit = DefaultKitEnum.values();
		
		listKit += ChatColor.GREEN;
		
		for(int i = 0; i < listDefaultKit.length; i++)
		{
			listKit += "!" + listDefaultKit[i].name().toLowerCase() + " ; ";
		}
		
		listKit += "#RGB ; #RRGGBB ;";
		
		return listKit;
	}
	
	public static ItemStack[] get(String value)
	{
		ItemStack item[] = null;
		
		if(value.startsWith("!") || value.startsWith("#"))
		{
			value = value.replace("!", "").toLowerCase();
			Color finalColor = null;
			
			if(value.startsWith("#"))
			{
				value = value.replace("#", "");
				if(value.length() == 3 || value.length() == 6)
				{
					String r, g, b;
					
					if(value.length() == 3)
					{
						r = value.substring(0, 1);
						g = value.substring(1, 2);
						b = value.substring(2, 3);
						
						r += "0";
						g += "0";
						b += "0";
					}
					else
					{
						r = value.substring(0, 2);
						g = value.substring(2, 4);
						b = value.substring(4, 6);
					}
					
					finalColor = Color.fromRGB(
							ProcessString.parseIntBase16(r), 
							ProcessString.parseIntBase16(g), 
							ProcessString.parseIntBase16(b));
				}
				else
					return null;
			}
			
			if(finalColor == null)
				finalColor = DefaultKitEnum.getColor(value);
			
			
			if(finalColor == null)
			{
				if(value.equals("wood"))
				{
					item = new ItemStack[6];
					
					item[0] = new ItemStack(Material.LEATHER_HELMET);
					item[1] = new ItemStack(Material.LEATHER_CHESTPLATE);
					item[2] = new ItemStack(Material.LEATHER_LEGGINGS);
					item[3] = new ItemStack(Material.LEATHER_BOOTS);
					item[4] = new ItemStack(Material.WOOD_SWORD);
					
					if(CheckMethod.existDoubleHand()) item[5] = new ItemStack(Material.SHIELD);
				}
				else if(value.equals("stone"))
				{
					item = new ItemStack[6];
					
					item[0] = new ItemStack(Material.LEATHER_HELMET);
					item[1] = new ItemStack(Material.LEATHER_CHESTPLATE);
					item[2] = new ItemStack(Material.LEATHER_LEGGINGS);
					item[3] = new ItemStack(Material.LEATHER_BOOTS);
					item[4] = new ItemStack(Material.STONE_SWORD);
					
					if(CheckMethod.existDoubleHand()) item[5] = new ItemStack(Material.SHIELD);
				}
				else if(value.equals("iron"))
				{
					item = new ItemStack[6];
					
					item[0] = new ItemStack(Material.IRON_HELMET);
					item[1] = new ItemStack(Material.IRON_CHESTPLATE);
					item[2] = new ItemStack(Material.IRON_LEGGINGS);
					item[3] = new ItemStack(Material.IRON_BOOTS);
					item[4] = new ItemStack(Material.IRON_SWORD);
					
					if(CheckMethod.existDoubleHand()) item[5] = new ItemStack(Material.SHIELD);
				}
				else if(value.equals("gold"))
				{
					item = new ItemStack[6];
					
					item[0] = new ItemStack(Material.GOLD_HELMET);
					item[1] = new ItemStack(Material.GOLD_CHESTPLATE);
					item[2] = new ItemStack(Material.GOLD_LEGGINGS);
					item[3] = new ItemStack(Material.GOLD_BOOTS);
					item[4] = new ItemStack(Material.GOLD_SWORD);
					
					if(CheckMethod.existDoubleHand()) item[5] = new ItemStack(Material.SHIELD);
				}
				else if(value.equals("diam"))
				{
					item = new ItemStack[6];
					
					item[0] = new ItemStack(Material.DIAMOND_HELMET);
					item[1] = new ItemStack(Material.DIAMOND_CHESTPLATE);
					item[2] = new ItemStack(Material.DIAMOND_LEGGINGS);
					item[3] = new ItemStack(Material.DIAMOND_BOOTS);
					item[4] = new ItemStack(Material.DIAMOND_SWORD);
					
					if(CheckMethod.existDoubleHand()) item[5] = new ItemStack(Material.SHIELD);
				}
				else
				{
					return null;
				}
				
				return item;
			}
			
			if(finalColor != null)
			{
				item = new ItemStack[6];
				
				item[0] = new ItemStack(Material.LEATHER_HELMET);
				item[1] = new ItemStack(Material.LEATHER_CHESTPLATE);
				item[2] = new ItemStack(Material.LEATHER_LEGGINGS);
				item[3] = new ItemStack(Material.LEATHER_BOOTS);
				item[4] = new ItemStack(Material.IRON_SWORD);
				
				if(CheckMethod.existDoubleHand()) item[5] = new ItemStack(Material.SHIELD);
				
				for(int i = 0; i < 4; i++)
				{
					LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) item[i].getItemMeta();
					leatherArmorMeta.setColor(finalColor);
					item[i].setItemMeta(leatherArmorMeta);
				}
			}
		}
		
		return item;
	}
}
