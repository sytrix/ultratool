package thecleaner.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

import thecleaner.UltraToolMain;
import thecleaner.file.FileKit;
import thecleaner.fonction.KitManager;
import thecleaner.message.ChatMessage;
import thecleaner.worldedit.MaterialManager;


public class ListKit
{
	UltraToolMain m_plugin;
	Map<String, ItemStack[]> m_equipment;
	
	public ListKit(UltraToolMain plugin)
	{
		m_plugin = plugin;
		
		m_equipment = FileKit.Load(m_plugin);
	}
	
	public boolean contains(String key)
	{
		return m_equipment.containsKey(key.toLowerCase());
	}
	
	public ItemStack[] get(String key, Player sender)
	{
		ItemStack[] kit = null;
		
		if(key.equals("~") && sender != null) {
			kit = KitManager.getKit(sender);
			return kit;
		}
		
		
		key = key.toLowerCase();
		
		Entity entity = m_plugin.getListEntityProperty().getEntityByRef(key);
		
		if(entity != null)
		{
			if(entity instanceof LivingEntity)
			{
				kit = new ItemStack[6];
				
				EntityEquipment entityEquipment = ((LivingEntity)entity).getEquipment();
				ItemStack[] hands = KitManager.getItemHands((LivingEntity)entity);
				
				kit[0] = entityEquipment.getHelmet();
				kit[1] = entityEquipment.getChestplate();
				kit[2] = entityEquipment.getLeggings();
				kit[3] = entityEquipment.getBoots();
				kit[4] = hands[0];
				kit[5] = hands[1];
				
				return kit;
			}
		}
		
		kit = m_equipment.get(key);
		
		if(kit != null)
			return kit;
		
		if(key.startsWith("!") && key.contains(","))
		{
			MaterialManager materialManager = new MaterialManager();
			ItemStack[] items = null;
			
			key = key.replace("!", "");
			materialManager.processing(key);
			
			if(!materialManager.isValid())
				return null;
			
			items = materialManager.getItemStack();
			
			if(items.length == 5)
				return items;
			
			return null;
		}
		
		return ListDefaultKit.get(key);
	}
	
	public void set(String key, ItemStack[] equipment)
	{
		key = key.toLowerCase();
		
		if(m_equipment.containsKey(key))
			FileKit.saveEquipment(m_plugin, key, equipment);
		else
			FileKit.saveNewEquipment(m_plugin, key, equipment);
		
		m_equipment.put(key, equipment);
		m_plugin.getListCommand().updateKit();
	}
	
	public List<String> getList()
	{
		List<String> listPointName = new ArrayList<String>();
		Iterator<String> locationList = m_equipment.keySet().iterator();
		
		listPointName.add("~");
		
		while(locationList.hasNext())
		{
			listPointName.add(locationList.next());
		}
		
		return listPointName;
	}
	
	public List<String> getListComplete()
	{
		List<String> listPointName = this.getList();
		
		DefaultKitEnum[] defaultKit = DefaultKitEnum.values();
		
		for(DefaultKitEnum k : defaultKit)
		{
			listPointName.add("!" + k.name().toLowerCase());
		}
		
		listPointName.add("#");
		listPointName.add("!wood");
		listPointName.add("!stone");
		listPointName.add("!iron");
		listPointName.add("!gold");
		listPointName.add("!diam");
		
		Iterator<String> locationList = m_plugin.getListEntityProperty().getIteratorReferences();
		
		while(locationList.hasNext())
		{
			listPointName.add(locationList.next());
		}
		
		return listPointName;
	}
	
	public String getListFormat()
	{
		return this.getListFormat(true);
	}
	
	public String getListFormat(boolean interne)
	{
		String listKit = "";
		Iterator<String> kitList = m_equipment.keySet().iterator();
		Iterator<String> kitEntityList = m_plugin.getListEntityProperty().getIteratorReferences();
		
		if(interne)
			listKit += ChatMessage.info("");
		else
			listKit += ChatMessage.show("~ ; ");
		
		while(kitList.hasNext())
		{
			listKit += kitList.next() + " ; ";
		}
		
		listKit += ChatMessage.show("");
		
		while(kitEntityList.hasNext())
		{
			listKit += kitEntityList.next() + " ; ";
		}
		
		listKit += ListDefaultKit.getListFormat();
		
		return listKit;
	}
	
	public void remove(String key)
	{
		key = key.toLowerCase();
		m_equipment.remove(key);
		FileKit.delEquipment(m_plugin, m_equipment);
		m_plugin.getListCommand().updateKit();
	}
}




