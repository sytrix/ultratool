package thecleaner.list;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Entity;

import thecleaner.UltraToolMain;
import thecleaner.fonction.ProcessString;

public class ListEntityProperty {
	
	public class EntityProperty {
		private String m_uuid;
		private String m_reference;
		private String m_invToDrop;
		private Double m_damage;
		private boolean m_passive;
		private boolean m_invincible;
		private boolean m_livingEntity;
		
		public EntityProperty(String[] params) {
			m_uuid = params[0];
			m_reference = params[1];
			m_invToDrop = params[2];
			m_damage = null;
			m_passive = false;
			m_invincible = false;
			m_livingEntity = false;
			
			if(params[3] != null)
				m_damage = ProcessString.parseDouble(params[3]);
			
			m_passive = "true".equals(params[4]);
			m_invincible = "true".equals(params[5]);
			m_livingEntity = "true".equals(params[6]);
		}
		
		public EntityProperty(String uuid) {
			m_uuid = uuid;
			m_reference = null;
			m_invToDrop = null;
			m_damage = null;
			m_passive = false;
			m_invincible = false;
			m_livingEntity = false;
		}
		
		public EntityProperty(String uuid, String reference) {
			m_uuid = uuid;
			m_reference = null;
			m_invToDrop = null;
			m_damage = null;
			m_passive = false;
			m_invincible = false;
			m_livingEntity = false;
		}
		
		public String getInlineData() {
			return m_uuid + ";" + m_reference + ";" + m_invToDrop + ";" + m_damage + ";" + m_passive + ";" + m_invincible + ";" + m_livingEntity;
		}
		
		public boolean isUselessProperty() {
			if(m_reference == null && m_invToDrop == null && m_damage == null && !m_passive && !m_invincible && !m_livingEntity) {
				return true;
			}
			
			return false;
		}

		public String getUUID() {
			return m_uuid;
		}
		
		private void setReference(String reference) {
			m_reference = reference;
		}
		
		public String getReference() {
			return m_reference;
		}

		public String getInvToDrop() {
			return m_invToDrop;
		}

		public void setInvToDrop(String invToDrop) {
			m_invToDrop = invToDrop;
		}

		public Double getDamage() {
			return m_damage;
		}

		public void setDamage(Double damage) {
			m_damage = damage;
		}

		public boolean isPassive() {
			return m_passive;
		}

		public void setPassive(boolean passive) {
			m_passive = passive;
		}

		public boolean isInvincible() {
			return m_invincible;
		}

		public void setInvincible(boolean invincible) {
			m_invincible = invincible;
		}
		
		public boolean isLivingEntity() {
			return m_livingEntity;
		}
		
		public void setLivingEntity(boolean livingEntity) {
			m_livingEntity = livingEntity;
		}
	}
	
	private UltraToolMain m_plugin;
	private Map<String, EntityProperty> m_properties;
	private Map<String, EntityProperty> m_references;
	
	
	public ListEntityProperty(UltraToolMain plugin) {
		m_plugin = plugin;
		m_properties = new HashMap<>();
		m_references = new HashMap<>();
	}
	
	public List<String> getLinesToSave() {
		List<String> list = new ArrayList<>();
		Iterator<String> iterator = this.getIteratorUUID();
		
		while(iterator.hasNext()) {
			EntityProperty property = m_properties.get(iterator.next());
			if(!property.isUselessProperty()) {
				list.add(property.getInlineData());
			}
		}
		
		return list;
	}
	
	public void loadLines(List<String> listData) {
		Iterator<String> iterator = listData.iterator();
		
		while(iterator.hasNext()) {
			String line = iterator.next();
			
			String[] params = line.split(";");
			for(int i = 0; i < params.length; i++) {
				if(params[i].equals("null"))
					params[i] = null;
			}
			
			EntityProperty prop = new EntityProperty(params);
			
			if(params[0] != null) {
				m_properties.put(params[0], prop);
				if(params[1] != null) {
					m_references.put(params[1], prop);
				}
			}
		}
	}
	
	public void addEntity(Entity entity) {
		String uuid = entity.getUniqueId().toString();
		if(!m_properties.containsKey(uuid)) {
			m_properties.put(uuid, new EntityProperty(uuid));
		}
	}
	
	public void addReference(Entity entity, String ref) {
		String uuid = entity.getUniqueId().toString();
		EntityProperty entityRef = null;
		ref = ref.toLowerCase();
		if(m_properties.containsKey(uuid)) {
			entityRef = m_properties.get(uuid);
			entityRef.setReference(ref);
		} else {
			entityRef = new EntityProperty(uuid, ref);
			m_properties.put(uuid, entityRef);
		}
		if(m_references.containsKey(ref)) {
			m_properties.get(ref).setReference(null);
		}
		
		m_references.put(ref, entityRef);
	}
	
	public EntityProperty getProperty(Entity entity) {
		String uuid = entity.getUniqueId().toString();
		return m_properties.get(uuid);
	}
	
	public EntityProperty getPropertyByUUID(String uuid) {
		return m_properties.get(uuid);
	}
	
	public Entity getEntityByRef(String ref) {
		ref = ref.toLowerCase();
		EntityProperty property = m_references.get(ref);
		if(property != null) {
			UUID uuid = UUID.fromString(property.getUUID());
			return m_plugin.getServer().getEntity(uuid);
		}
		
		return null;
	}
	
	public Iterator<String> getIteratorUUID() {
		return m_properties.keySet().iterator();
	}
	
	public Iterator<String> getIteratorReferences() {
		return m_references.keySet().iterator();
	}
	
	public List<String> getListByUUID() {
		Iterator<String> iterator = m_properties.keySet().iterator();
		List<String> listUUID = new ArrayList<>();
		
		while(iterator.hasNext()) {
			listUUID.add(iterator.next());
		}
		
		return listUUID;
	}
	
	public List<String> getListByRef() {
		Iterator<String> iterator = m_references.keySet().iterator();
		List<String> listRef = new ArrayList<>();
		
		while(iterator.hasNext()) {
			listRef.add(iterator.next());
		}
		
		return listRef;
	}
	
	public void remove(Entity entity) {
		String uuid = entity.getUniqueId().toString();
		EntityProperty entityProperty = m_properties.get(uuid);
		
		m_properties.remove(uuid);
		if(entityProperty != null) {
			m_references.remove(entityProperty.getReference());
		}
		
	}
}
