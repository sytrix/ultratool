package thecleaner.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import thecleaner.UltraToolMain;
import thecleaner.file.FileInv;
import thecleaner.message.ChatMessage;


public class ListInv
{
	UltraToolMain m_plugin;
	Map<String, ItemStack[]> m_inv;
	
	public ListInv(UltraToolMain plugin)
	{
		m_plugin = plugin;
		
		m_inv = FileInv.Load(m_plugin);
	}
	
	public int size()
	{
		return m_inv.size();
	}
	
	public boolean contains(String key)
	{
		return m_inv.containsKey(key.toLowerCase());
	}
	
	public ItemStack[] get(String key, Player sender)
	{
		key = key.toLowerCase();
		ItemStack[] items = m_plugin.getListKit().get(key, sender);
		
		if(items == null)
		{
			items = m_inv.get(key);
		}
		
		if(items != null)
		{
			List<ItemStack> listSort = new ArrayList<ItemStack>();
			for(int i = 0; i < items.length; i++)
			{
				if(items[i] != null && !items[i].getType().equals(Material.AIR))
					listSort.add(items[i]);
			}
			
			items = new ItemStack[listSort.size()];
			
			for(int i = 0; i < listSort.size(); i++)
			{
				items[i] = listSort.get(i);
			}
		}
		
		return items;
	}
	
	public void set(String key, ItemStack[] equipment)
	{
		key = key.toLowerCase();
		
		if(m_inv.containsKey(key))
			FileInv.saveEquipment(m_plugin, key, equipment);
		else
			FileInv.saveNewEquipment(m_plugin, key, equipment);
		
		m_inv.put(key, equipment);
		m_plugin.getListCommand().updateInv();
	}
	
	public List<String> getList()
	{
		List<String> listPointName = new ArrayList<String>();
		Iterator<String> locationList = m_inv.keySet().iterator();
		
		while(locationList.hasNext())
		{
			listPointName.add(locationList.next());
		}
		
		return listPointName;
	}
	
	public List<String> getListComplete()
	{
		List<String> listPointName = this.getList();
		Iterator<String> locationList = m_plugin.getListKit().getListComplete().iterator();
		
		while(locationList.hasNext())
		{
			listPointName.add(locationList.next());
		}
		
		return listPointName;
	}
	
	public String getListFormat()
	{
		String listKit = "";
		Iterator<String> invList = m_inv.keySet().iterator();
		
		listKit += ChatMessage.info("");
		
		while(invList.hasNext())
		{
			listKit += invList.next() + " ; ";
		}
		
		listKit += m_plugin.getListKit().getListFormat(false);
		
		return listKit;
	}
	
	public void remove(String key)
	{
		key = key.toLowerCase();
		m_inv.remove(key);
		FileInv.delEquipment(m_plugin, m_inv);
		m_plugin.getListCommand().updateInv();
	}
}




