package thecleaner.list;

import org.bukkit.Color;

public enum DefaultKitEnum 
{
	AQUA, BLACK, BLUE, FUCHSIA, GRAY, GREEN, LIME, MAROON, NAVY, 
	OLIVE, ORANGE, PURPLE, RED, SILVER, TEAL, WHITE, YELLOW, 
	WOOD, STONE, IRON, GOLD, DIAM;
	
	public static DefaultKitEnum getKit(String kitName)
	{
		DefaultKitEnum[] listKit = DefaultKitEnum.values();
		String kitNameLower = kitName.toLowerCase();
		
		for(int i = 0; i < listKit.length; i++)
		{
			if(listKit[i].name().toLowerCase().equals(kitNameLower))
				return listKit[i];
		}
		
		return null;
	}
	
	public static Color getColor(DefaultKitEnum color)
	{
		if(color.equals(DefaultKitEnum.AQUA))
			return Color.AQUA;
		else if(color.equals(DefaultKitEnum.BLACK))
			return Color.BLACK;
		else if(color.equals(DefaultKitEnum.BLUE))
			return Color.BLUE;
		else if(color.equals(DefaultKitEnum.FUCHSIA))
			return Color.FUCHSIA;
		else if(color.equals(DefaultKitEnum.GRAY))
			return Color.GRAY;
		else if(color.equals(DefaultKitEnum.GREEN))
			return Color.GREEN;
		else if(color.equals(DefaultKitEnum.LIME))
			return Color.LIME;
		else if(color.equals(DefaultKitEnum.MAROON))
			return Color.MAROON;
		else if(color.equals(DefaultKitEnum.NAVY))
			return Color.NAVY;
		else if(color.equals(DefaultKitEnum.OLIVE))
			return Color.OLIVE;
		else if(color.equals(DefaultKitEnum.ORANGE))
			return Color.ORANGE;
		else if(color.equals(DefaultKitEnum.PURPLE))
			return Color.PURPLE;
		else if(color.equals(DefaultKitEnum.RED))
			return Color.RED;
		else if(color.equals(DefaultKitEnum.SILVER))
			return Color.SILVER;
		else if(color.equals(DefaultKitEnum.TEAL))
			return Color.TEAL;
		else if(color.equals(DefaultKitEnum.WHITE))
			return Color.WHITE;
		else if(color.equals(DefaultKitEnum.YELLOW))
			return Color.YELLOW;
		
		return null;
	}
	
	public static Color getColor(String kitName)
	{
		DefaultKitEnum kit = getKit(kitName);
		
		if(kit != null)
			return getColor(kit);
		
		return null;
	}
}

