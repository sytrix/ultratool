package thecleaner.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import thecleaner.UltraToolMain;
import thecleaner.message.ChatMessage;
import thecleaner.message.Help;
import thecleaner.message.Language;

public class UltraTool extends UltraCommandExecutor
{
	public UltraTool(UltraToolMain plugin)
	{
		super(plugin);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Language language = m_plugin.getLanguage();
		
		if(arg.length != 0)
		{
			sender.sendMessage(ChatMessage.errorNbArg(command));
			return true;
		}
		
		Help.command(language, sender);
		return true;
	}
	
	
}
