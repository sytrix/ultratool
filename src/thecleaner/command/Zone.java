package thecleaner.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import thecleaner.UltraToolMain;
import thecleaner.file.FileMap;
import thecleaner.fonction.ProcessString;
import thecleaner.list.ListPlayerCuboid;
import thecleaner.list.ListZone;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;
import thecleaner.worldedit.MaterialManager;
import thecleaner.worldedit.Cuboid;

public class Zone extends UltraCommandExecutor
{
	private ListZone m_listZone;
	private ListPlayerCuboid m_listPlayerCuboid;
	
	public Zone(UltraToolMain plugin)
	{
		super(plugin);
		m_listZone = m_plugin.getListZone();
		m_listPlayerCuboid = m_plugin.getListPlayerCuboid();
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Player player = null;
		
		Language language = m_plugin.getLanguage();
		
		if(sender instanceof Player)
		{
			player = (Player)sender;
		}
		
		if(arg.length == 0)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.zone.title")));
			sender.sendMessage(ChatMessage.info("/" + command + " set <zone> - d�finir une zone"));
			sender.sendMessage(ChatMessage.info("/" + command + " move <zone> <x> <y> <z> - d�placer une zone"));
			sender.sendMessage(ChatMessage.info("/" + command + " setBlock <zone> <blockid> - remplacer une zone par des blocks"));
			sender.sendMessage(ChatMessage.info("/" + command + " replaceBlock <zone> <blocks> - remplacer tout sauf l'air"));
			sender.sendMessage(ChatMessage.info("/" + command + " replaceBlock <zone> <blocks> <blocks> - remplacer des blocks par d'autres"));
			sender.sendMessage(ChatMessage.info("/" + command + " moveBlock <zone> <x> <y> <z> - d�placer le contenu d'une zone"));
			sender.sendMessage(ChatMessage.info("/" + command + " saveBlock <zone> <nom> - sauver les blocks contenu dans une zone"));
			sender.sendMessage(ChatMessage.info("/" + command + " loadBlock <zone> <nom> - charger et coller le contenu d'une zone"));
			sender.sendMessage(ChatMessage.info("/" + command + " list - liste des zones"));
			sender.sendMessage(ChatMessage.info("/" + command + " del <zone> - supprime une zone"));
			sender.sendMessage(ChatMessage.info("/" + command + " info <zone> - information concernant une zone"));
			sender.sendMessage(ChatMessage.info("/" + command + " tool - vous procure l'outil permettant de cr�e des zones"));
			
			return true;
		}
		
		if(sender.hasPermission("ultratool.code"))
			arg = m_plugin.getScript().replace(arg);
		
		String key = arg[0].toLowerCase();
		
		if(key.equals("set"))
		{
			if(arg.length != 2)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			if(player == null)
			{
				sender.sendMessage(ChatMessage.errorMustBePlayer());
				return true;
			}
			
			boolean error = false;
			
			if(!ProcessString.isCorrectString(arg[1]))
			{
				sender.sendMessage(ChatMessage.errorNotAlphaNum(arg[1]));
				error = true;
			}
			
			Cuboid zone = m_listPlayerCuboid.get(player);
			
			if(zone == null)
			{
				sender.sendMessage(ChatMessage.info(language.get("command.zone.not_selected")));
				error = true;
			}
			else
			{
				if(!zone.isValid())
				{
					sender.sendMessage(ChatMessage.info(language.get("command.zone.not_valid")));
				}
			}
			
			if(error)
				return true;
			
			m_listZone.put(arg[1], zone.clone());
			sender.sendMessage(ChatMessage.info(language.getElement("command.zone.set", arg[1])));
		}
		else if(key.equals("move") || key.equals("moveblock"))
		{
			if(arg.length != 5)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			boolean error = false;
			
			Cuboid cuboid = m_listZone.get(arg[1], player);
			
			if(cuboid == null)
			{
				sender.sendMessage(ChatMessage.errorZoneUndef(arg[1]));
				error = true;
			}
			
			Integer mx = ProcessString.parseInt(arg[2]);
			Integer my = ProcessString.parseInt(arg[3]);
			Integer mz = ProcessString.parseInt(arg[4]);
			
			if(mx == null)
			{
				sender.sendMessage(ChatMessage.errorNotInteger(arg[2]));
				error = true;
			}
			if(my == null)
			{
				sender.sendMessage(ChatMessage.errorNotInteger(arg[3]));
				error = true;
			}
			if(mz == null)
			{
				sender.sendMessage(ChatMessage.errorNotInteger(arg[4]));
				error = true;
			}
			
			if(error)
				return true;
			
			if(key.equals("move"))
			{
				m_listZone.put(arg[1], cuboid.move(mx, my, mz));
				//sender.sendMessage(ChatMessage.info(language.getElement("command.zone.set", arg[1])));
			}
			else
			{
				World world = cuboid.getWorld();
				Cuboid newCuboid = cuboid.move(mx, my, mz);
				List<Integer> listBlockId = new ArrayList<Integer>();
				List<Byte> listBlockData = new ArrayList<Byte>();
				
				int posX = cuboid.getPosX();
				int posY = cuboid.getPosY();
				int posZ = cuboid.getPosZ();
				int sizeX = cuboid.getSizeX();
				int sizeY = cuboid.getSizeY();
				int sizeZ = cuboid.getSizeZ();
				
				for(int z = posZ; z < posZ + sizeZ; z++)
				{
					for(int y = posY; y < posY + sizeY; y++)
					{
						for(int x = posX; x < posX + sizeX; x++)
						{
							Block block = world.getBlockAt(x, y, z);
							
							listBlockId.add(this.getMaterialId(block.getType()));
							listBlockData.add(this.getDataFromBlock(block));
						}
					}
				}
				
				posX = newCuboid.getPosX();
				posY = newCuboid.getPosY();
				posZ = newCuboid.getPosZ();
				sizeX = newCuboid.getSizeX();
				sizeY = newCuboid.getSizeY();
				sizeZ = newCuboid.getSizeZ();
				
				for(int i = 0; i < listBlockId.size(); i++)
				{
					int idY = i / sizeX;
					int idZ = idY / sizeY;
					
					Block block = world.getBlockAt(posX + i % sizeX, posY + idY % sizeY, posZ + idZ % sizeZ);
					
					Material mat = this.getMaterialFromId(listBlockId.get(i));
					
					if(mat != null)
					{
						if(!block.getType().equals(mat))
							block.setType(mat);
					}
					
					if(this.getDataFromBlock(block) != listBlockData.get(i))
						this.setDataFromBlock(block, listBlockData.get(i));
				}
			}
		}
		else if(key.equals("setblock"))
		{
			if(arg.length != 3)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			Cuboid cuboid = m_listZone.get(arg[1], player);
			MaterialManager blockManager = new MaterialManager();
			boolean error = false;
			
			blockManager.processing(arg[2]);
			
			if(cuboid == null)
			{
				sender.sendMessage(ChatMessage.errorZoneUndef(arg[1]));
				error = true;
			}
			
			if(!blockManager.isValid())
			{
				sender.sendMessage(ChatMessage.errorSyntax(command));
				error = true;
			}
			
			if(error)
				return true;
			
			World world = cuboid.getWorld();
			
			int sizex = cuboid.getPosX() + cuboid.getSizeX();
			int sizey = cuboid.getPosY() + cuboid.getSizeY();
			int sizez = cuboid.getPosZ() + cuboid.getSizeZ();
			
			for(int x = cuboid.getPosX(); x < sizex; x++)
			{
				for(int y = cuboid.getPosY(); y < sizey; y++)
				{
					for(int z = cuboid.getPosZ(); z < sizez; z++)
					{
						Block block = world.getBlockAt(x, y, z);
						block = blockManager.setBlock(block);
					}
				}
			}
		}
		else if(key.equals("replaceblock"))
		{
			if(arg.length != 3 && arg.length != 4)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			Cuboid cuboid = m_listZone.get(arg[1], player);
			MaterialManager oldBlockManager = null;
			MaterialManager newBlockManager = new MaterialManager();
			boolean error = false;
			
			if(cuboid == null)
			{
				sender.sendMessage(ChatMessage.errorZoneUndef(arg[1]));
				error = true;
			}
			
			if(arg.length == 4)
			{
				oldBlockManager = new MaterialManager();
				oldBlockManager.processing(arg[2]);
				newBlockManager.processing(arg[3]);
			}
			else
			{
				newBlockManager.processing(arg[2]);
			}
			
			if(arg.length == 4)
			{
				if(!oldBlockManager.isValid())
				{
					sender.sendMessage(ChatMessage.errorNameUndef(arg[2]));
					error = true;
				}
				if(!newBlockManager.isValid())
				{
					sender.sendMessage(ChatMessage.errorNameUndef(arg[3]));
					error = true;
				}
			}
			else
			{
				if(!newBlockManager.isValid())
				{
					sender.sendMessage(ChatMessage.errorNameUndef(arg[2]));
					error = true;
				}
			}
			
			if(error)
				return true;
			
			World world = cuboid.getWorld();
			
			int sizex = cuboid.getPosX() + cuboid.getSizeX();
			int sizey = cuboid.getPosY() + cuboid.getSizeY();
			int sizez = cuboid.getPosZ() + cuboid.getSizeZ();
			
			for(int x = cuboid.getPosX(); x < sizex; x++)
			{
				for(int y = cuboid.getPosY(); y < sizey; y++)
				{
					for(int z = cuboid.getPosZ(); z < sizez; z++)
					{
						Block block = world.getBlockAt(x, y, z);
						
						if(oldBlockManager == null)
						{
							if(!block.getType().equals(Material.AIR))
								block = newBlockManager.setBlock(block);
						}
						else
						{
							if(oldBlockManager.contain(block))
								block = newBlockManager.setBlock(block);
						}
							
					}
				}
			}
		}
		else if(key.equals("saveblock") || key.equals("loadblock"))
		{
			if(arg.length != 3)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			Cuboid cuboid = m_listZone.get(arg[1], player);
			String contentName = "";
			boolean error = false;
			
			if(cuboid == null)
			{
				sender.sendMessage(ChatMessage.errorZoneUndef(arg[1]));
				error = true;
			}
			
			if(ProcessString.isCorrectString(arg[2]))
				contentName = arg[2];
			else
				error = true;
			
			if(error)
				return true;
			
			if(key.equals("saveblock"))
				FileMap.saveCuboid(m_plugin, contentName, cuboid);
			else
				FileMap.loadCuboid(m_plugin, contentName, cuboid);
		}
		else if(key.equals("list"))
		{
			if(arg.length != 1)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			sender.sendMessage(ChatMessage.title("Zone List"));
			String listZone = ChatMessage.info("");
			List<String> nameList = m_listZone.getList();
			
			for(int i = 0; i < nameList.size(); i++)
			{
				listZone += nameList.get(i) + " ; ";
			}
			
			sender.sendMessage(listZone);
		}
		else if(key.equals("del"))
		{
			if(arg.length != 2)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			if(!m_listZone.contains(arg[1]))
			{
				sender.sendMessage(ChatMessage.errorZoneUndef(arg[1]));
				return true;
			}
			
			m_listZone.remove(arg[1]);
			sender.sendMessage(ChatMessage.info(language.getElement("command.zone.del", arg[1])));
		}
		else if(key.equals("info"))
		{
			if(arg.length != 2)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			Cuboid cuboid = m_listZone.get(arg[1], player);
			
			if(cuboid == null)
			{
				sender.sendMessage(ChatMessage.errorZoneUndef(arg[1]));
				return true;
			}
			
			
			Location pos1 = cuboid.getPosition1();
			Location pos2 = cuboid.getPosition2();
			String infoPos = cuboid.getPosX() + ", " + cuboid.getPosY() + ", " + cuboid.getPosZ();
			String infoSize = cuboid.getSizeX() + "x" + cuboid.getSizeY() + "x" + cuboid.getSizeZ();
			
			sender.sendMessage(ChatMessage.title("Info zone: " + arg[1]));
			sender.sendMessage(ChatMessage.show("Point 1: ") + ChatMessage.info(ProcessString.getStringLocation(pos1)));
			sender.sendMessage(ChatMessage.show("Point 2: ") + ChatMessage.info(ProcessString.getStringLocation(pos2)));
			sender.sendMessage(ChatMessage.show("Position: ") + ChatMessage.info(infoPos));
			sender.sendMessage(ChatMessage.show("Size: ") + ChatMessage.info(infoSize));
			sender.sendMessage(ChatMessage.show("Volume: ") + ChatMessage.info(cuboid.getVolume().toString()));

		}
		else if(key.equals("tool"))
		{
			if(arg.length != 1)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			if(player == null)
			{
				sender.sendMessage(ChatMessage.errorMustBePlayer());
				return true;
			}
			
			
			player.getInventory().addItem(new ItemStack(this.getMaterialFromId(m_plugin.getPluginConfig().getZoneToolId()), 1));
		}
		else
		{
			sender.sendMessage(ChatMessage.errorCommand(command));
		}
		
		return true;
	}
}
