package thecleaner.command;

import java.util.Collection;
import java.util.Iterator;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import thecleaner.UltraToolMain;
import thecleaner.fonction.ProcessString;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;

public class Effect extends UltraCommandExecutor 
{
	public Effect(UltraToolMain plugin)
	{
		super(plugin);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Player player = null;
		Language language = m_plugin.getLanguage();
		
		if(sender instanceof Player)
		{
			player = (Player)sender;
		}
		
		if(arg.length == 0)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.effect.title")));
			sender.sendMessage(ChatMessage.info("/" + command + " add <effect> <duration> <amplifier> [player]"));
			sender.sendMessage(ChatMessage.info("/" + command + " remove <effect> [player]"));
			sender.sendMessage(ChatMessage.info("/" + command + " reset [player]"));
			sender.sendMessage(ChatMessage.info("/" + command + " list"));
			return true;
		}
		
		if(sender.hasPermission("ultratool.code"))
			arg = m_plugin.getScript().replace(arg);
		
		String key = arg[0].toLowerCase();
		
		if(key.equals("add"))
		{
			if(arg.length == 4 || arg.length == 5)
			{
				Player client = null;
				PotionEffectType potionType = null;
				Integer effect = -1;
				Integer duration = 0;
				Integer amplifier = 0;
				boolean error = false;
				
				effect = ProcessString.parseInt(arg[1]);
				duration = ProcessString.parseInt(arg[2]);
				amplifier = ProcessString.parseInt(arg[3]);
				
				if(effect == null)
				{
					potionType = PotionEffectType.getByName(arg[1]);
					
					if(potionType == null)
					{
						sender.sendMessage(ChatMessage.errorNameUndef(arg[1]));
						error = true;
					}
				}
				else
				{
					potionType = this.getPotionEffectFromId(effect);
					
					if(potionType == null)
					{
						sender.sendMessage(ChatMessage.errorOutOfRange(effect));
						error = true;
					}
				}
				
				if(duration == null)
				{
					sender.sendMessage(ChatMessage.errorNotInteger(arg[2]));
					error = true;
				}
				
				if(amplifier == null)
				{
					sender.sendMessage(ChatMessage.errorNotInteger(arg[3]));
					error = true;
				}
				
				if(arg.length == 4)
				{
					if(player != null)
						client = player;
					else
					{
						sender.sendMessage(ChatMessage.errorMustBePlayer());
						error = true;
					}
				}
				
				if(arg.length == 5)
				{
					client = this.getPlayer(arg[4]);
					
					if(client == null)
					{
						sender.sendMessage(ChatMessage.errorPlayerNotFound(arg[4]));
						error = true;
					}
				}
				
				if(error)
					return true;
				
				client.addPotionEffect(potionType.createEffect(duration, amplifier));
			}
			else
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
			}
		}
		else if(key.equals("remove"))
		{
			if(arg.length == 2 || arg.length == 3)
			{
				Player client = null;
				PotionEffectType potionType = null;
				Integer effect = -1;
				boolean error = false;
				
				effect = ProcessString.parseInt(arg[1]);
				
				if(effect == null)
				{
					potionType = PotionEffectType.getByName(arg[1]);
					
					if(potionType == null)
					{
						sender.sendMessage(ChatMessage.errorNameUndef(arg[1]));
						error = true;
					}
				}
				else
				{
					potionType = this.getPotionEffectFromId(effect);
					
					if(potionType == null)
					{
						sender.sendMessage(ChatMessage.errorOutOfRange(effect));
						error = true;
					}
				}
				
				if(arg.length == 2)
				{
					if(player != null)
						client = player;
					else
					{
						sender.sendMessage(ChatMessage.errorMustBePlayer());
						error = true;
					}
				}
				
				if(arg.length == 3)
				{
					client = this.getPlayer(arg[2]);
					
					if(client == null)
					{
						sender.sendMessage(ChatMessage.errorPlayerNotFound(arg[2]));
						error = true;
					}
				}
				
				if(error)
					return true;
				
				client.removePotionEffect(potionType);
			}
			else
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
			}
		}
		else if(key.equals("reset"))
		{
			if(arg.length == 1 || arg.length == 2)
			{
				Player client = null;
				boolean error = false;
				
				if(arg.length == 1)
				{
					if(player != null)
						client = player;
					else
					{
						sender.sendMessage(ChatMessage.errorMustBePlayer());
						error = true;
					}
				}
				
				if(arg.length == 2)
				{
					client = this.getPlayer(arg[1]);
					
					if(client == null)
					{
						sender.sendMessage(ChatMessage.errorPlayerNotFound(arg[2]));
						error = true;
					}
				}
				
				if(error)
					return true;
				
				Collection<PotionEffect> listEffect = client.getActivePotionEffects();
				Iterator<PotionEffect> iteratorEffect = listEffect.iterator();
				
				while(iteratorEffect.hasNext())
				{
					client.removePotionEffect(iteratorEffect.next().getType());
				}
			}
			else
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
			}
		}
		else if(key.equals("list"))
		{
			if(arg.length == 1)
			{
				PotionEffectType[] nameList = PotionEffectType.values();
				String listFinal = "";
				
				sender.sendMessage(ChatMessage.title(language.get("command.effect.list_title")));
				
				for(int i = 1; i < nameList.length; i++)
				{
					listFinal += ChatColor.GOLD;
					listFinal += nameList[i].getName();
					listFinal += ChatColor.DARK_PURPLE;
					listFinal += "(" + this.getPotionEffectId(nameList[i]) + ") ";
				}
				
				sender.sendMessage(listFinal);
			}
			else
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
			}
		}
		else
		{
			sender.sendMessage(ChatMessage.errorCommand(command));
		}
		
		return true;
	}
}
