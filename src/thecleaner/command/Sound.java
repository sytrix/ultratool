package thecleaner.command;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import thecleaner.UltraToolMain;
import thecleaner.fonction.ProcessString;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;

public class Sound extends UltraCommandExecutor
{
	public Sound(UltraToolMain plugin)
	{
		super(plugin);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Player player = null;
		BlockCommandSender commandBlock = null;
		Language language = m_plugin.getLanguage();
		
		if(sender instanceof Player)
		{
			player = (Player)sender;
		}
		else if(sender instanceof BlockCommandSender)
		{
			commandBlock = (BlockCommandSender) sender;
		}
		
		if(arg.length == 0)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.sound.title")));
			sender.sendMessage(ChatMessage.info("/" + command + " play <sound> [player]"));
			sender.sendMessage(ChatMessage.info("/" + command + " list"));
			return true;
		}
		
		if(sender.hasPermission("ultratool.code"))
			arg = m_plugin.getScript().replace(arg);
		
		String key = arg[0].toLowerCase();
		
		if(key.equals("play"))
		{
			java.util.List<Player> players = new ArrayList<>();
			org.bukkit.Sound[] listSound = org.bukkit.Sound.values();
			org.bukkit.Sound sound = null;
			Location soundLocation = null;
			Integer idSound = null;
			boolean error = false;
			
			if(arg.length == 3)
			{
				Player p = this.getPlayer(arg[2]);
				
				if(p == null)
				{
					sender.sendMessage(ChatMessage.errorPlayerNotFound(arg[2]));
					error = true;
				} else {
					soundLocation = p.getLocation();
					players.add(p);
				}
			}
			else if(arg.length == 2)
			{
				
				
				if(player == null)
				{
					if(commandBlock == null) {
						sender.sendMessage(ChatMessage.errorMustBePlayer());
						error = true;
					} else {
						soundLocation = commandBlock.getBlock().getLocation();
						players = soundLocation.getWorld().getPlayers();
					}
				} else {
					soundLocation = player.getLocation();
					players.add(player);
				}
			}
			else
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			idSound = ProcessString.parseInt(arg[1]);
			
			if(idSound == null)
			{
				
				String soundName = arg[1].toUpperCase();
				
				for(int i = 0; i < listSound.length; i++)
				{
					if(listSound[i].name().equals(soundName))
					{
						sound = listSound[i];
						break;
					}
				}
				
				if(sound == null)
				{
					sender.sendMessage(ChatMessage.errorNameUndef(arg[1]));
					error = true;
				}
			}
			else
			{
				if(idSound >= 0 && idSound < listSound.length)
				{
					sound = listSound[idSound];
				}
				else
				{
					sender.sendMessage(ChatMessage.errorOutOfRange(idSound));
					error = true;
				}
			}
			
			if(error)
				return true;
			
			if(soundLocation == null) {
				
			}
			
			for(Player p : players) {
				p.playSound(soundLocation, sound, 10, 10);
			}
		}
		else if(key.equals("list"))
		{
			if(arg.length != 1)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			org.bukkit.Sound[] nameList = org.bukkit.Sound.values();
			String listFinal = "";
			
			sender.sendMessage(ChatMessage.title("Sound List"));
			
			for(int i = 0; i < nameList.length; i++)
			{
				listFinal += ChatColor.GOLD + nameList[i].name() + 
					ChatColor.DARK_PURPLE + "(" + i + ") ";
			}
			
			sender.sendMessage(listFinal);
		}
		else
		{
			sender.sendMessage(ChatMessage.errorCommand(command));
		}
		
		return true;
	}
}
