package thecleaner.command;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import thecleaner.UltraToolMain;
import thecleaner.fonction.ProcessString;
import thecleaner.list.ListInv;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;
import thecleaner.worldedit.MaterialManager;

public class Inv extends UltraCommandExecutor
{
	private ListInv m_listInv;
	
	public Inv(UltraToolMain plugin)
	{
		super(plugin);
		m_listInv = m_plugin.getListInv();
	}

	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Player player = null;
		BlockCommandSender commandBlock = null;
		Language language = m_plugin.getLanguage();
		
		if(sender instanceof Player)
		{
			player = (Player)sender;
		}
		else if(sender instanceof BlockCommandSender)
		{
			commandBlock = (BlockCommandSender) sender;
		}
		
		if(arg.length == 0)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.inv.title")));
			sender.sendMessage(ChatMessage.info("/" + command + " chestReplace <inventory> [point]"));
			sender.sendMessage(ChatMessage.info("/" + command + " chestDrop <inventory> [point]"));
			sender.sendMessage(ChatMessage.info("/" + command + " chestClear [point]"));
			sender.sendMessage(ChatMessage.info("/" + command + " itemsDrop <inventory/items> [point]"));
			sender.sendMessage(ChatMessage.info("/" + command + " del <inventory>"));
			sender.sendMessage(ChatMessage.info("/" + command + " give <inventory> <pseudo>"));
			sender.sendMessage(ChatMessage.info("/" + command + " get <inventory>"));
			sender.sendMessage(ChatMessage.info("/" + command + " set <inventory>"));
			sender.sendMessage(ChatMessage.info("/" + command + " list"));
			
			return true;
		}
		
		String key = arg[0].toLowerCase();
		
		if(sender.hasPermission("ultratool.code"))
			arg = m_plugin.getScript().replace(arg);
		
		sender.sendMessage(key);
		sender.sendMessage("(" + arg.length + ") " + arg);
		
		if(key.equals("chestreplace") || key.equals("chestdrop") || key.equals("chestclear"))
		{
			if((key.equals("chestclear") && arg.length != 1 && arg.length != 2) || 
					((key.equals("chestreplace") || key.equals("chestdrop")) 
					&& arg.length != 2 && arg.length != 3))
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			Location chestLocation = null;
			
			if(arg.length == 1)
			{
				if(commandBlock != null)
				{
					chestLocation = commandBlock.getBlock().getLocation();
				}
				else if(player != null)
				{
					chestLocation = player.getLocation();
				}
			}
			else if(arg.length == 2)
			{
				if(key.equals("chestclear"))
				{
					chestLocation = m_plugin.getListPoint().get(arg[1], sender);
					
					if(chestLocation == null)
					{
						sender.sendMessage(ChatMessage.errorPointUndef(arg[1]));
						return true;
					}
				}
				else
				{
					if(commandBlock != null)
					{
						chestLocation = commandBlock.getBlock().getLocation();
					}
					else if(player != null)
					{
						chestLocation = player.getLocation();
					}
					else
					{
						sender.sendMessage(ChatMessage.errorNoConsole());
						return true;
					}
				}
			}
			else if(arg.length == 3)
			{
				chestLocation = m_plugin.getListPoint().get(arg[2], sender);
				
				if(chestLocation == null)
				{
					sender.sendMessage(ChatMessage.errorPointUndef(arg[2]));
					return true;
				}
			}
			
			Location researchChest = chestLocation.clone();
			int maxHeight = researchChest.getWorld().getMaxHeight();
			
			while(researchChest.getBlockY() < maxHeight)
			{
				Block block = researchChest.add(0, 1, 0).getBlock();
				
				if(block != null)
				{
					BlockState blockState = block.getState();
					
					if(blockState instanceof InventoryHolder)
					{
						InventoryHolder inventoryHolder = (InventoryHolder) blockState;
						Inventory inventory = inventoryHolder.getInventory();
						
						if(inventory == null)
							return true;
						
						if(key.equals("chestclear") || key.equals("chestreplace"))
							inventory.clear();
						
						if(key.equals("chestreplace") || key.equals("chestdrop"))
						{
							ItemStack[] item = m_listInv.get(arg[1], player);
							
							if(item != null)
							{
								for(int i = 0; i < item.length; i++)
								{
									inventory.addItem(item[i]);
								}
							}
							else
							{
								sender.sendMessage(ChatMessage.errorInventoryUndef(arg[1]));
								return true;
							}
						}
						return true;
					}
				}
			}
			
		}
		else if(key.equals("itemsdrop"))
		{
			if(arg.length != 2 && arg.length != 3)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			Location spawnPoint = null;
			ItemStack[] items = m_listInv.get(arg[1], player);
			boolean error = false;
			
			if(items == null)
			{
				MaterialManager itemManager = new MaterialManager();
				itemManager.processing(arg[1]);
				if(itemManager.isValid())
					items = itemManager.getItemStack();
				
				if(items == null)
				{
					sender.sendMessage(ChatMessage.errorInventoryUndef(arg[1]));
					error = true;
				}
			}
			
			if(arg.length == 2)
			{
				Location commandLocation = null;
				
				if(player != null)
					commandLocation = player.getLocation();
				else if(commandBlock != null)
					commandLocation = commandBlock.getBlock().getLocation().add(0.5, 0, 0.5);
				else
				{
					sender.sendMessage(ChatMessage.errorNoConsole());
					error = true;
				}
				
				if(commandLocation != null)
				{
					int maxHeight = commandLocation.getWorld().getMaxHeight();
					spawnPoint = commandLocation.clone();
					
					while(spawnPoint.getBlock().getType().isSolid()
							&& spawnPoint.getBlockY() < maxHeight)
					{
						spawnPoint = spawnPoint.add(0, 1, 0);
					}
				}
			}
			else // arg.lenght == 3
			{
				spawnPoint = m_plugin.getListPoint().get(arg[2], sender);
				
				if(spawnPoint == null)
				{
					sender.sendMessage(ChatMessage.errorPointUndef(arg[2]));
					error = true;
				}
			}
			
			if(error)
				return true;
			
			World world = spawnPoint.getWorld();
			
			for(int i = 0; i < items.length; i++)
				world.dropItemNaturally(spawnPoint, items[i]);
			
		}
		else if(key.equals("set"))
		{
			if(arg.length == 2)
			{
				if(ProcessString.isCorrectString(arg[1]))
				{
					if(player != null)
					{
						PlayerInventory playerInventory = player.getInventory();
						ItemStack[] contents = playerInventory.getContents().clone();
						
						for(int i = 0; i < contents.length; i++)
						{
							if(contents[i] == null)
							{
								contents[i] = new ItemStack(Material.AIR);
							}
						}
						
						m_listInv.set(arg[1], contents);
						sender.sendMessage(ChatMessage.info(language.getElement("command.inv.set", arg[1])));
					}
					else
					{
						sender.sendMessage(ChatMessage.errorNoConsole());
						return true;
					}
				}
				else
				{
					sender.sendMessage(ChatMessage.errorNotAlphaNum(arg[1]));
					return true;
				}
			}
			else
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
		}
		else if(key.equals("list"))
		{
			sender.sendMessage(ChatMessage.title(language.get("command.inv.list")));
			sender.sendMessage(m_listInv.getListFormat());
		}
		else if(key.equals("del"))
		{
			if(arg.length != 2)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			if(!m_listInv.contains(arg[1]))
			{
				sender.sendMessage(ChatMessage.errorInventoryUndef(arg[1]));
				return true;
			}
			
			m_listInv.remove(arg[1]);
			sender.sendMessage(ChatMessage.info(language.getElement("command.inv.del", arg[1])));
		}
		else if(key.equals("get"))
		{
			if(arg.length == 2)
			{
				if(player != null)
				{
					ItemStack[] inv = m_listInv.get(arg[1], player);
					
					if(inv != null)
					{
						PlayerInventory playerInventory = player.getInventory();
						playerInventory.clear();
						
						for(int i = 0; i < inv.length; i++)
						{
							playerInventory.addItem(inv[i]);
						}
					}
					else
					{
						sender.sendMessage(ChatMessage.errorInventoryUndef(arg[1]));
					}
				}
				else
				{
					sender.sendMessage(ChatMessage.errorMustBePlayer());
				}
			}
			else
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
			}
		}
		else if(key.equals("give"))
		{
			if(arg.length == 3)
			{
				Player client = this.getPlayer(arg[2]);
				
				if(client != null)
				{
					ItemStack[] inv = m_listInv.get(arg[1], player);
					
					if(inv != null)
					{
						PlayerInventory playerInventory = client.getInventory();
						playerInventory.clear();
						
						for(int i = 0; i < inv.length; i++)
							playerInventory.addItem(inv[i]);
					}
					else
					{
						sender.sendMessage(ChatMessage.errorInventoryUndef(arg[1]));
					}
				}
				else
				{
					sender.sendMessage(ChatMessage.errorPlayerNotFound(arg[2]));
				}
			}
			else
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
			}
		}
		else
		{
			sender.sendMessage(ChatMessage.errorCommand(command));
		}
		
		return true;
	}
}
