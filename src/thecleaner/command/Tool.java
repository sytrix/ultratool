package thecleaner.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import thecleaner.UltraToolMain;
import thecleaner.fonction.KitManager;
import thecleaner.fonction.ProcessString;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;

public class Tool extends UltraCommandExecutor
{
	public Tool(UltraToolMain plugin)
	{
		super(plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Player player = null;
		Language language = m_plugin.getLanguage();
		
		if(sender instanceof Player)
		{
			player = (Player)sender;
		}
		
		if(arg.length == 0)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.tool.title")));
			sender.sendMessage(ChatMessage.info("/" + command + " aspiration [radius] [pow]"));
			sender.sendMessage(ChatMessage.info("/" + command + " repulsion [radius] [pow]"));
			
			return true;
		}
		
		String key = arg[0].toLowerCase();
		if(sender.hasPermission("ultratool.code"))
			arg = m_plugin.getScript().replace(arg);
		
		if((key.equals("aspiration") || key.equals("repulsion")))
		{
			ItemStack item = new ItemStack(Material.STICK);
			ItemMeta itemMeta = item.getItemMeta();
			List<String> description = new ArrayList<String>();
			Integer radius = 10;
			Integer pow = 10;
			boolean error = false;
			
			if(player == null)
			{
				sender.sendMessage(ChatMessage.errorMustBePlayer());
				error = true;
			}
			
			if(arg.length >= 2)
				radius = ProcessString.parseInt(arg[1]);
			if(arg.length >= 3)
				pow = ProcessString.parseInt(arg[2]);
			
			if(radius == null)
			{
				sender.sendMessage(ChatMessage.errorNotInteger(arg[1]));
				error = true;
			}
			else if(radius > m_plugin.getPluginConfig().getToolEffectRadiusMax())
			{
				sender.sendMessage(ChatMessage.error("Vous ne pouvez pas d�finir un rayon de plus de " + m_plugin.getPluginConfig().getToolEffectRadiusMax() + " cubes"));
				error = true;
			}
			
			if(pow == null)
			{
				sender.sendMessage(ChatMessage.errorNotInteger(arg[2]));
				error = true;
			}
			
			if(error)
				return true;
			
			if(key.equals("aspiration"))
			{
				description.add(ChatColor.DARK_PURPLE + "Faite un clic droit pour");
				description.add(ChatColor.DARK_PURPLE + "aspirer ce qui vous entours");
				description.add(ChatColor.DARK_PURPLE + "dans un rayon de " + radius + " blocs");
				description.add(ChatColor.DARK_PURPLE + "avec une puissance de " + pow);
				itemMeta.setLore(description);
			}
			else 
			{
				description.add(ChatColor.DARK_PURPLE + "Faite un clic droit pour");
				description.add(ChatColor.DARK_PURPLE + "repulser ce qui vous entours");
				description.add(ChatColor.DARK_PURPLE + "dans un rayon de " + radius + " blocs");
				description.add(ChatColor.DARK_PURPLE + "avec une puissance de " + pow);
				itemMeta.setLore(description);
			}
			
			if(key.equals("aspiration"))
				itemMeta.setDisplayName(ChatColor.GOLD + "aspiration " + radius + "," + pow);
			else 
				itemMeta.setDisplayName(ChatColor.GOLD + "repulsion " + radius + "," + pow);
			
			item.setItemMeta(itemMeta);
			
			item.addUnsafeEnchantment(Enchantment.SILK_TOUCH, radius);
			item.addUnsafeEnchantment(Enchantment.KNOCKBACK, pow);
			
			KitManager.setItemHand(player, item);
		}
		else
		{
			sender.sendMessage(ChatMessage.errorCommand(command));
		}
		
		return true;
	}
}
