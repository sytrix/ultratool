package thecleaner.command;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import thecleaner.UltraToolMain;
import thecleaner.message.ChatMessage;
import thecleaner.message.Help;
import thecleaner.message.Language;

public class List extends UltraCommandExecutor
{
	public List(UltraToolMain plugin)
	{
		super(plugin);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Language language = m_plugin.getLanguage();
		
		if(arg.length == 0)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.list.title")));
			sender.sendMessage(ChatMessage.info("/" + command + " command - help"));
			sender.sendMessage(ChatMessage.info("/" + command + " enchantment - list of enchantment"));
			sender.sendMessage(ChatMessage.info("/" + command + " sound - list of sound"));
			sender.sendMessage(ChatMessage.info("/" + command + " effect - list of effect"));
			sender.sendMessage(ChatMessage.info("/" + command + " entity - o:<id>"));
			sender.sendMessage(ChatMessage.info("/" + command + " point - p:<nom>"));
			sender.sendMessage(ChatMessage.info("/" + command + " kit - k:<nom>"));
			sender.sendMessage(ChatMessage.info("/" + command + " potionEffect - e:<id>:<lvl>"));
			sender.sendMessage(ChatMessage.info("/" + command + " potion - t:<id>"));
			sender.sendMessage(ChatMessage.info("/" + command + " color - c:<id>"));
			sender.sendMessage(ChatMessage.info("/" + command + " zombie - t:<id>"));
			sender.sendMessage(ChatMessage.info("/" + command + " skeleton - t:<id>"));
			sender.sendMessage(ChatMessage.info("/" + command + " horsevariant - t:<id>"));
			sender.sendMessage(ChatMessage.info("/" + command + " horsecolor - c:<id>"));
			sender.sendMessage(ChatMessage.info("/" + command + " horsestyle - s:<id>"));
			sender.sendMessage(ChatMessage.info("/" + command + " villager - t:<id>"));
			sender.sendMessage(ChatMessage.info("/" + command + " item - t:<id>"));
			sender.sendMessage(ChatMessage.info("/" + command + " block - t:<id>"));
			return true;
		}
		
		if(arg.length != 1)
		{
			sender.sendMessage(ChatMessage.errorNbArg(command));
			return true;
		}
		
		String key = arg[0].toLowerCase();
		
		if(key.equals("command") || key.equals("help"))
		{
			Help.command(language, sender);
		}
		else if(key.equals("enchantment"))
		{
			sender.sendMessage(ChatMessage.title(language.get("command.list.enchantment")));
			Enchantment[] listEnchantment = Enchantment.values();
			String messageListEntity = "";
			
			for(int i = 0; i < listEnchantment.length; i++)
			{
				messageListEntity += ChatMessage.info(listEnchantment[i].getName());
				messageListEntity += ChatMessage.show("(" + this.getEnchantId(listEnchantment[i]) + ") ");
				
			}
			
			sender.sendMessage(messageListEntity);
		}
		else if(key.equals("sound"))
		{
			org.bukkit.Sound[] nameList = org.bukkit.Sound.values();
			String listFinal = "";
			
			sender.sendMessage(ChatMessage.title("Sound List"));
			
			for(int i = 0; i < nameList.length; i++)
			{
				listFinal += ChatMessage.info(nameList[i].name());
				listFinal += ChatMessage.show("(" + i + ") ");
			}
			
			sender.sendMessage(listFinal);
		}
		else if(key.equals("entity"))
		{
			sender.sendMessage(ChatMessage.title("Entity List"));
			EntityType[] listEntity = EntityType.values();
			String messageListEntity = "";
			
			for(int i = 0; i < listEntity.length; i++)
			{
				messageListEntity += ChatMessage.info(listEntity[i].name()); 
				messageListEntity += ChatMessage.show("(" + i + ") ");
				
			}
			
			sender.sendMessage(messageListEntity);
		}
		else if(key.equals("point"))
		{
			sender.sendMessage(ChatMessage.title("Point List"));
			String listPoint = m_plugin.getListPoint().getListFormat();
			
			sender.sendMessage(listPoint);
		}
		else if(key.equals("kit"))
		{
			sender.sendMessage(ChatMessage.title("Kit List"));
			sender.sendMessage(ChatMessage.info(m_plugin.getListKit().getListFormat()));
		}
		else if(key.equals("potioneffect"))
		{
			PotionEffectType[] nameList = PotionEffectType.values();
			String listFinal = "";
			
			sender.sendMessage(ChatMessage.title("Effect List"));
			
			for(int i = 1; i < nameList.length; i++)
			{
				listFinal += ChatMessage.info(nameList[i].getName());
				listFinal += ChatMessage.show("(" + i + ") ");
			}
			
			sender.sendMessage(listFinal);
		}
		else if(key.equals("potion"))
		{
			PotionType[] listPotion = PotionType.values();
			String listFinal = "";
			
			sender.sendMessage(ChatMessage.title("Potion List"));
			
			for(int i = 1; i < listPotion.length; i++)
			{
				listFinal += ChatMessage.info(listPotion[i].name());
				listFinal += ChatMessage.show("(" + i + ") ");
			}
			
			sender.sendMessage(listFinal);
		}
		else if(key.equals("color"))
		{
			DyeColor[] listColor = DyeColor.values();
			String listFinal = "";
			
			sender.sendMessage(ChatMessage.title("Color List"));
			
			for(int i = 0; i < listColor.length; i++)
			{
				listFinal += ChatMessage.info(listColor[i].name());
				listFinal += ChatMessage.show("(" + i + ") ");
			}
			
			sender.sendMessage(listFinal);
		}
		else if(key.equals("zombie"))
		{
			String listFinal = "";
			sender.sendMessage(ChatMessage.title("Zombie List"));
			listFinal += ChatMessage.info("NORMAL") + ChatMessage.show("(0) ");
			listFinal += ChatMessage.info("VILLAGER") + ChatMessage.show("(1) ");
			
			sender.sendMessage(listFinal);
		}
		else if(key.equals("skeleton"))
		{
			this.sendListZombie(sender);
		}
		else if(key.equals("horsevariant"))
		{
			this.sendListHorseVariant(sender);
		}
		else if(key.equals("horsecolor"))
		{
			Horse.Color[] listColor = Horse.Color.values();
			String listFinal = "";
			
			sender.sendMessage(ChatMessage.title("Color Horse List"));
			
			for(int i = 0; i < listColor.length; i++)
			{
				listFinal += ChatMessage.info(listColor[i].name());
				listFinal += ChatMessage.show("(" + i + ") ");
			}
			
			sender.sendMessage(listFinal);
		}
		else if(key.equals("horsestyle"))
		{
			Horse.Style[] listStyle = Horse.Style.values();
			String listFinal = "";
			
			sender.sendMessage(ChatMessage.title("Style Horse List"));
			
			for(int i = 0; i < listStyle.length; i++)
			{
				listFinal += ChatMessage.info(listStyle[i].name());
				listFinal += ChatMessage.show("(" + i + ") ");
			}
			
			sender.sendMessage(listFinal);
		}
		else if(key.equals("villager"))
		{
			Profession[] listProfession = Profession.values();
			String listFinal = "";
			
			sender.sendMessage(ChatMessage.title("Villager Type List"));
			
			for(int i = 0; i < listProfession.length; i++)
			{
				listFinal += ChatMessage.info(listProfession[i].name());
				listFinal += ChatMessage.show("(" + i + ") ");
			}
			
			sender.sendMessage(listFinal);
		}
		else if(key.equals("item"))
		{
			Material[] listMaterial = Material.values();
			String listFinal = "";
			
			sender.sendMessage(ChatMessage.title("Item List"));
			
			for(int i = 0; i < listMaterial.length; i++)
			{
				if(!listMaterial[i].isBlock())
				{
					listFinal += ChatMessage.info(listMaterial[i].name());
					listFinal += ChatMessage.show("(" + this.getMaterialId(listMaterial[i]) + ") ");
				}
			}
			
			sender.sendMessage(listFinal);
		}
		else if(key.equals("block"))
		{
			Material[] listMaterial = Material.values();
			String listFinal = "";
			
			sender.sendMessage(ChatMessage.title("Block List"));
			
			for(int i = 0; i < listMaterial.length; i++)
			{
				if(listMaterial[i].isBlock())
				{
					listFinal += ChatMessage.info(listMaterial[i].name());
					listFinal += ChatMessage.show("(" + this.getMaterialId(listMaterial[i]) + ") ");
				}
			}
			
			sender.sendMessage(listFinal);
		}
		else
		{
			sender.sendMessage(ChatMessage.errorCommand(command));
		}
		
		return true;
	}
	
	private void sendListZombie(CommandSender sender) 
	{
		@SuppressWarnings("deprecation")
		org.bukkit.entity.Skeleton.SkeletonType[] listSkeletonType = org.bukkit.entity.Skeleton.SkeletonType.values();
		String listFinal = "";
		
		sender.sendMessage(ChatMessage.title("Skeleton List"));
		
		for(int i = 0; i < listSkeletonType.length; i++)
		{
			listFinal += ChatMessage.info(listSkeletonType[i].name());
			listFinal += ChatMessage.show("(" + i + ") ");
		}
		
		sender.sendMessage(listFinal);
	}
	
	private void sendListHorseVariant(CommandSender sender)
	{
		@SuppressWarnings("deprecation")
		Horse.Variant[] listVariant = Horse.Variant.values();
		String listFinal = "";
		
		sender.sendMessage(ChatMessage.title("Variant Horse List"));
		
		for(int i = 0; i < listVariant.length; i++)
		{
			listFinal += ChatMessage.info(listVariant[i].name());
			listFinal += ChatMessage.show("(" + i + ") ");
		}
		
		sender.sendMessage(listFinal);
	}
}
