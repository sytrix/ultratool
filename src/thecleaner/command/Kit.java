package thecleaner.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import thecleaner.UltraToolMain;
import thecleaner.fonction.KitManager;
import thecleaner.fonction.ProcessString;
import thecleaner.list.ListKit;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;

public class Kit extends UltraCommandExecutor
{
	private ListKit m_listKit;
	
	public Kit(UltraToolMain plugin)
	{
		super(plugin);
		m_listKit = m_plugin.getListKit();
	}

	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Player player = null;
		Language language = m_plugin.getLanguage();
		
		if(sender instanceof Player)
		{
			player = (Player)sender;
		}
		
		if(arg.length == 0)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.kit.title")));
			sender.sendMessage(ChatMessage.info("/" + command + " give <kit> <player>"));
			sender.sendMessage(ChatMessage.info("/" + command + " del <kit>"));
			sender.sendMessage(ChatMessage.info("/" + command + " get <kit>"));
			sender.sendMessage(ChatMessage.info("/" + command + " set <kit>"));
			sender.sendMessage(ChatMessage.info("/" + command + " list"));
			
			return true;
		}
		
		if(sender.hasPermission("ultratool.code"))
			arg = m_plugin.getScript().replace(arg);
		
		if(arg[0].toLowerCase().equals("set"))
		{
			boolean error = false;
			
			if(player == null)
			{
				sender.sendMessage(ChatMessage.errorMustBePlayer());
				error = true;
			}
			
			if(arg.length != 2)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				error = true;
			}
			
			if(arg.length >= 2 && !ProcessString.isCorrectString(arg[1]))
			{
				sender.sendMessage(ChatMessage.errorNotAlphaNum(arg[1]));
				error = true;
			}
			
			if(error)
				return true;
			
			ItemStack[] equipment = KitManager.getKit(player);
			
			if(equipment == null)
			{
				sender.sendMessage(ChatMessage.errorDefault());
				return true;
			}
			
			m_listKit.set(arg[1], equipment);
			sender.sendMessage(ChatMessage.info(language.getElement("command.kit.set", arg[1])));
		}
		else if(arg[0].toLowerCase().equals("list"))
		{
			if(arg.length != 1)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			sender.sendMessage(ChatMessage.title(language.get("command.kit.list")));
			sender.sendMessage(ChatMessage.info(m_listKit.getListFormat()));
		}
		else if(arg[0].toLowerCase().equals("del"))
		{
			if(arg.length != 2)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			if(!m_listKit.contains(arg[1]))
			{
				sender.sendMessage(ChatMessage.errorKitUndef(arg[1]));
				return true;
			}
			
			m_listKit.remove(arg[1]);
			sender.sendMessage(ChatMessage.info(language.getElement("command.kit.del", arg[1])));
		}
		else if(arg[0].toLowerCase().equals("get"))
		{
			ItemStack[] equipment = null;
			boolean error = false;
			
			if(player == null)
			{
				sender.sendMessage(ChatMessage.errorMustBePlayer());
				error = true;
			}
			
			if(arg.length != 2)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				error = true;
			}
			
			if(arg.length >= 2)
			{
				equipment = m_listKit.get(arg[1], player);
				
				if(equipment == null)
				{
					sender.sendMessage(ChatMessage.errorKitUndef(arg[1]));
					error = true;
				}
			}
			
			if(error)
				return true;
			
			if(!KitManager.setKit(player, equipment))
			{
				sender.sendMessage(ChatMessage.errorDefault());
				return true;
			}
			
			sender.sendMessage(ChatMessage.info(language.getElement("command.kit.get", arg[1])));

		}
		else if(arg[0].toLowerCase().equals("give"))
		{
			ItemStack[] equipment = null;
			Player client = null;
			boolean error = false;
			
			if(arg.length != 3)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				error = true;
			}
			
			if(arg.length >= 2)
			{
				equipment = m_listKit.get(arg[1], player);
				if(equipment == null)
				{
					sender.sendMessage(ChatMessage.errorKitUndef(arg[1]));
					error = true;
				}
			}
			
			if(arg.length >= 3)
			{
				client = this.getPlayer(arg[2]);
				
				if(client == null)
				{
					sender.sendMessage(ChatMessage.errorPlayerNotFound(arg[2]));
					error = true;
				}
			}
			
			if(error)
				return true;
			
			if(!KitManager.setKit(client, equipment))
			{
				sender.sendMessage(ChatMessage.errorDefault());
				return true;
			}
		}
		else
		{
			sender.sendMessage(ChatMessage.errorCommand(command));
		}
		
		return true;
	}
}
