package thecleaner.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import thecleaner.UltraToolMain;
import thecleaner.fonction.ProcessString;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;
import thecleaner.sender.PluginSender;

public class Exe extends UltraCommandExecutor
{
	public Exe(UltraToolMain plugin)
	{
		super(plugin);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Language language = m_plugin.getLanguage();
		int nbArg = arg.length;
		
		
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			
			if(!player.isOp())
			{
				sender.sendMessage(ChatMessage.error(ChatMessage.errorMustBeOp()));
				return true;
			}
		}
		
		if(nbArg == 0)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.exe.title")));
			sender.sendMessage(ChatMessage.info("/" + command + " <...>"));
			sender.sendMessage(ChatMessage.info("/" + command + " help"));
			return true;
		}
		
		String key = arg[0].toLowerCase();
		
		
		if(!key.equals("settask"))
		{
			if(sender.hasPermission("ultratool.code"))
				arg = m_plugin.getScript().replace(arg);
		}
		
		nbArg = arg.length;
		
		if(key.equals("help"))
		{
			sender.sendMessage(ChatMessage.title(language.get("command.exe.help_title")));
			sender.sendMessage(ChatMessage.info(language.get("command.exe.help_content")));
		}
		else
		{
			if(sender instanceof Player || sender instanceof BlockCommandSender)
			{
				PluginSender pluginSender = new PluginSender();
				pluginSender.setSender(sender);
				
				try
				{
					processCommand(m_plugin, pluginSender, arg, 0, new ArrayList<String>());
				}
				catch(Exception e)
				{
					processCommand(m_plugin, 
							m_plugin.getServer().getConsoleSender(), 
							arg, 0, new ArrayList<String>());
				}
			}
			else
			{
				sender.sendMessage(ChatMessage.errorNoConsole());
			}
		}
		
		return true;
	}
	
	public void processCommand(final UltraToolMain plugin, final CommandSender sender, 
			final String[] arg, int index, final List<String> listUnique)
	{
		List<String> listArg = new ArrayList<String>();
		
		for(int i = index; i < arg.length; i++)
		{
			if(arg[i].equals(">"))
			{
				sendCommand(listArg, sender);
				listArg.clear();
			}
			else if(arg[i].startsWith(">") && arg[i].endsWith(">"))
			{
				sendCommand(listArg, sender);
				listArg.clear();
				
				String res = arg[i].toLowerCase().replace(">", "");
				String[] splitRes = res.split(":");
				
				if(splitRes.length == 1)
				{
					Integer delay = ProcessString.toTick(res);
					final Integer finalIndex = i + 1; 
					
					if(delay != null && delay > 0)
					{
						plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() 
					    {
							public void run() 
					    	{
								processCommand(plugin, sender, arg, finalIndex, listUnique);
					    	}
						}, delay);
						
						return;
					}
				}
				else if(splitRes.length == 2)
				{
					String key = splitRes[0].toLowerCase();
					String value = splitRes[1];
					
					if(key.equals("unique"))
					{
						if(!plugin.getListUniqueExe().contain(value))
						{
							plugin.getListUniqueExe().put(value);
							listUnique.add(value);
							processCommand(plugin, sender, arg, i + 1, listUnique);
						}
						
						return;
					}
				}
			}
			else
			{
				listArg.add(arg[i]);
			}
		}
		
		sendCommand(listArg, sender);
		
		for(int i = 0; i < listUnique.size(); i++)
			m_plugin.getListUniqueExe().remove(listUnique.get(i));
	}
	
	private void sendCommand(List<String> listArg, CommandSender sender)
	{
		String compile = "";
		int nbArg = listArg.size();
		
		if(nbArg > 0)
		{
			for(int i = 0; i < nbArg; i++)
			{
				compile += listArg.get(i);
				
				if(i != nbArg - 1)
				{
					compile += " ";
				}
			}
			
			m_plugin.getServer().dispatchCommand(sender, compile);
		}
	}
}




