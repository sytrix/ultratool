package thecleaner.command;

import java.util.List;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import thecleaner.UltraToolMain;
import thecleaner.fonction.ProcessString;
import thecleaner.list.ListPoint;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;
import thecleaner.worldedit.MaterialManager;

public class Point extends UltraCommandExecutor
{
	private ListPoint m_listPoint;
	
	public Point(UltraToolMain plugin)
	{
		super(plugin);
		m_listPoint = m_plugin.getListPoint();
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Player player = null;
		BlockCommandSender commandBlock = null;
		Language language = m_plugin.getLanguage();
		
		if(sender instanceof Player)
		{
			player = (Player)sender;
		}
		else if(sender instanceof BlockCommandSender)
		{
			commandBlock = (BlockCommandSender) sender;
		}
		
		if(arg.length == 0)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.point.title")));
			sender.sendMessage(ChatMessage.info("/" + command + " set <point> <x> <y> <z> [yaw] [pitch]"));
			sender.sendMessage(ChatMessage.info("/" + command + " set <to_point> <from_point>"));
			sender.sendMessage(ChatMessage.info("/" + command + " set <point>"));
			sender.sendMessage(ChatMessage.info("/" + command + " move <point> <x> <y> <z>"));
			sender.sendMessage(ChatMessage.info("/" + command + " list"));
			sender.sendMessage(ChatMessage.info("/" + command + " del <point>"));
			sender.sendMessage(ChatMessage.info("/" + command + " tp <point> [joueur]"));
			sender.sendMessage(ChatMessage.info("/" + command + " explosion <point> [pow]"));
			sender.sendMessage(ChatMessage.info("/" + command + " effect <point> <effectId> [quantity] [subid] [radius]"));
			sender.sendMessage(ChatMessage.info("/" + command + " listEffect"));
			sender.sendMessage(ChatMessage.info("/" + command + " setBlock <point> <blocks>"));
			sender.sendMessage(ChatMessage.info("/" + command + " trace <point> <tracer>"));
			sender.sendMessage(ChatMessage.info("/" + command + " write <point> <direction> <block> <text>"));
			return true;
		}
		
		if(sender.hasPermission("ultratool.code"))
			arg = m_plugin.getScript().replace(arg);
		
		String key = arg[0].toLowerCase();
		
		if(key.equals("set"))
		{
			if(arg.length >= 5 && arg.length <= 7)
			{
				Location definePoint = null;
				World world = null;
				
				if(player != null)
				{
					world = player.getWorld();
				}
				else if(commandBlock != null)
				{
					world = commandBlock.getBlock().getWorld();			
				}
				else 
				{
					sender.sendMessage(ChatMessage.errorNoConsole());
					return true;
				}
				
				Integer x, y, z;
				Integer yaw = 0, pitch = 0;
				boolean error = false;
				
				if(!ProcessString.isCorrectString(arg[1]))
				{
					sender.sendMessage(ChatMessage.errorNotAlphaNum(arg[1]));
					error = true;
				}
				
				x = ProcessString.parseInt(arg[2]);
				y = ProcessString.parseInt(arg[3]);
				z = ProcessString.parseInt(arg[4]);
				
				if(x == null)
				{
					sender.sendMessage(ChatMessage.errorNotInteger(arg[2]));
					error = true;
				}
				if(y == null)
				{
					sender.sendMessage(ChatMessage.errorNotInteger(arg[3]));
					error = true;
				}
				if(z == null)
				{
					sender.sendMessage(ChatMessage.errorNotInteger(arg[4]));
					error = true;
				}
				
				
				if(arg.length == 5 && world != null)
				{
					if(error)
						return true;
					
					definePoint = new Location(world, x, y, z);
				}
				if(arg.length >= 6 && world != null)
				{
					yaw = ProcessString.parseInt(arg[5]);
					
					if(arg.length >= 7)
						pitch = ProcessString.parseInt(arg[6]);
					
					if(yaw == null)
					{
						sender.sendMessage(ChatMessage.errorNotInteger(arg[5]));
						error = true;
					}
					
					if(pitch == null)
					{
						sender.sendMessage(ChatMessage.errorNotInteger(arg[6]));
						error = true;
					}
					
					if(error)
						return true;
					
					definePoint = new Location(world, x, y, z, yaw, pitch);
				}
				
				if(error)
					return true;
				
				m_listPoint.set(arg[1], definePoint.add(-0.5, 0, -0.5));
				sender.sendMessage(ChatMessage.info(language.getElement("command.point.set", arg[1])));
			}
			else if(arg.length == 2)
			{
				if(!ProcessString.isCorrectString(arg[1]))
				{
					sender.sendMessage(ChatMessage.errorNotAlphaNum(arg[1]));
					return true;
				}
				
				if(player != null)
				{
					Location pointLocation = player.getLocation();
					m_listPoint.set(arg[1], pointLocation);
				}
				else if(commandBlock != null)
				{
					Location locBlock = commandBlock.getBlock().getLocation().add(0.5, 0, 0.5);
					
					while(locBlock.getBlock().getType().isSolid())
						locBlock = locBlock.add(0, 1, 0);
					
					m_listPoint.set(arg[1], locBlock);
				}
				else
				{
					sender.sendMessage(ChatMessage.errorNoConsole());
					return true;
				}
				
				sender.sendMessage(ChatMessage.info(language.getElement("command.point.set", arg[1])));
			}
			else if(arg.length == 3)
			{
				if(!ProcessString.isCorrectString(arg[1]))
				{
					sender.sendMessage(ChatMessage.errorNotAlphaNum(arg[1]));
					return true;
				}
				
				Location pointLocation = m_listPoint.get(arg[2], sender);
				
				if(pointLocation != null)
				{
					m_listPoint.set(arg[1], pointLocation);
					sender.sendMessage(ChatMessage.info(language.getElement("command.point.set", arg[1])));
				}
				else
				{
					sender.sendMessage(ChatMessage.errorPointUndef(arg[2]));
					return true;
				}
			}
			else
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
		}
		else if(key.equals("move"))
		{
			boolean error = false;
			
			if(arg.length != 5)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			Location definePoint = m_listPoint.get(arg[1], sender);
			Float x = ProcessString.parseFloat(arg[2]);
			Float y = ProcessString.parseFloat(arg[3]);
			Float z = ProcessString.parseFloat(arg[4]);
			
			if(definePoint == null)
			{
				sender.sendMessage(ChatMessage.errorPointUndef(arg[1]));
				error = true;
			}
			
			if(x == null)
			{
				sender.sendMessage(ChatMessage.errorNotDouble(arg[2]));
				error = true;
			}
			
			if(y == null)
			{
				sender.sendMessage(ChatMessage.errorNotDouble(arg[3]));
				error = true;
			}
			
			if(z == null)
			{
				sender.sendMessage(ChatMessage.errorNotDouble(arg[4]));
				error = true;
			}
			
			if(error)
				return true;
			
			definePoint.add(x, y, z);
			m_listPoint.set(arg[1], definePoint);
			
			sender.sendMessage(ChatMessage.info(language.getElement("command.point.move", arg[1])));
		}
		else if(key.equals("list"))
		{
			if(arg.length != 1)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			sender.sendMessage(ChatMessage.title(language.get("command.point.list")));
			String listPoint = m_plugin.getListPoint().getListFormat();
			
			sender.sendMessage(listPoint);
		}
		else if(key.equals("del"))
		{
			if(arg.length != 2)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			if(!m_listPoint.contains(arg[1]))
			{
				sender.sendMessage(ChatMessage.errorPointUndef(arg[1]));
				return true;
			}
			
			m_listPoint.remove(arg[1]);
			sender.sendMessage(ChatMessage.info(language.getElement("command.point.del", arg[1])));

		}
		else if(key.equals("tp"))
		{
			Player client = null;
			boolean error = false;
			
			if(arg.length == 2)
			{
				if(player != null)
				{
					client = player;
				}
				else
				{
					sender.sendMessage(ChatMessage.errorNoConsole());
					error = true;
				}
			}
			else if(arg.length == 3)
			{
				client = this.getPlayer(arg[2]);
				
				if(client == null)
				{
					sender.sendMessage(ChatMessage.errorPlayerNotFound(arg[2]));
					error = true;
				}
			}
			else
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				error = true;
			}
			
			Location pointLocation = m_listPoint.get(arg[1], sender);
			
			if(pointLocation == null)
			{
				sender.sendMessage(ChatMessage.errorPointUndef(arg[1]));
				error = true;
			}
			
			if(error)
				return true;
			
			client.teleport(pointLocation);
		}
		else if(key.equals("explosion"))
		{
			boolean error = false;
			Location explosionPoint;
			Float pow = 1.f;
			
			if(arg.length == 3)
			{
				pow = ProcessString.parseFloat(arg[2]);
				
				if(pow == null)
				{
					sender.sendMessage(ChatMessage.errorNotDouble(arg[2]));
					error = true;
				}
			}
			else if(arg.length != 2)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			explosionPoint = m_listPoint.get(arg[1], sender);
			
			if(explosionPoint == null)
			{
				sender.sendMessage(ChatMessage.errorPointUndef(arg[1]));
				error = true;
			}
			
			if(error)
				return true;
			
			World world = explosionPoint.getWorld();
			world.createExplosion(explosionPoint, pow);
		}
		else if(key.equals("effect"))
		{
			if(arg.length < 3 || arg.length > 5)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			Location effectPoint = m_listPoint.get(arg[1], sender);
			Effect effect = null;
			World world = null;
			Integer id = ProcessString.parseInt(arg[2]);
			Integer quantity = 1;
			Integer subId = 0;
			Integer rayon = 32;
			boolean error = false;
			
			if(effectPoint == null)
			{
				sender.sendMessage(ChatMessage.errorPointUndef(arg[1]));
				error = true;
			}
			
			if(id != null)
			{
				effect = this.getEffectFromId(id);
				
				if(effect == null)
				{
					sender.sendMessage(ChatMessage.errorOutOfRange(id));
					error = true;
				}
			}
			else
			{
				Effect[] listEffect = Effect.values();
				String effectName = arg[2].toLowerCase();
				
				for(int i = 0; i < listEffect.length; i++)
				{
					if(listEffect[i].name().toLowerCase().equals(effectName))
					{
						effect = listEffect[i];
						break;
					}
				}
				
				
				if(effect == null)
				{
					sender.sendMessage(ChatMessage.errorNameUndef(arg[2]));
					error = true;
				}
			}
			
			if(arg.length >= 4)
			{
				quantity = ProcessString.parseInt(arg[3]);
				
				if(quantity == null)
				{
					sender.sendMessage(ChatMessage.errorNotInteger(arg[3]));
					error = true;
				}
			}
			
			if(arg.length >= 5)
			{
				subId = ProcessString.parseInt(arg[4]);
				
				if(subId == null)
				{
					sender.sendMessage(ChatMessage.errorNotInteger(arg[4]));
					error = true;
				}
			}
			
			if(arg.length >= 6)
			{
				rayon = ProcessString.parseInt(arg[5]);
				
				if(rayon == null)
				{
					sender.sendMessage(ChatMessage.errorNotInteger(arg[5]));
					error = true;
				}
			}
			
			if(error)
				return true;
			
			world = effectPoint.getWorld();
			
			for(int i = 0; i < quantity; i++)
				world.playEffect(effectPoint, effect, subId.intValue(), rayon.intValue());
		}
		else if(key.equals("listeffect"))
		{
			if(arg.length != 1)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			sender.sendMessage(ChatMessage.title("Effect List"));
			Effect[] listEffect = Effect.values();
			String messageListEntity = "";
			
			for(int i = 0; i < listEffect.length; i++)
			{
				messageListEntity += ChatMessage.info(listEffect[i].name()); 
				messageListEntity += ChatMessage.show("(" + i + ") ");
				
			}
			
			sender.sendMessage(messageListEntity);
		}
		else if(key.equals("setblock"))
		{
			if(arg.length != 3)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			Location definePoint = m_listPoint.get(arg[1], sender);
			MaterialManager blockManager = new MaterialManager();
			boolean error = false;
			
			blockManager.processing(arg[2]);
			
			if(definePoint == null)
			{
				sender.sendMessage(ChatMessage.errorPointUndef(arg[1]));
				error = true;
			}
			
			if(!blockManager.isValid())
			{
				sender.sendMessage(ChatMessage.errorSyntax(command));
				error = true;
			}
			
			if(error)
				return true;
			
			Block block = definePoint.getBlock();
			block = blockManager.setBlock(block);
		}
		else if(key.equals("trace"))
		{
			if(arg.length < 2)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			Location begin = m_listPoint.get(arg[1], sender);
			String path = "";
			boolean res = true;
			
			if(begin == null)
			{
				sender.sendMessage(ChatMessage.errorPointUndef(arg[1]));
				return true;
			}
			
			for(int i = 2; i < arg.length; i++)
			{
				path += arg[i];
			}
			
			path = path.toUpperCase();
			res = trace(path, begin, Material.STONE);
			
			if(!res)
			{
				sender.sendMessage(ChatMessage.errorDefault());
				return true;
			}
		}
		else if(key.equals("write"))
		{
			if(arg.length < 5)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
					
			String direction = arg[2].toUpperCase();
			MaterialManager blockManager = new MaterialManager();
			int dx = 0;
			int dz = 0;
			boolean error = false;
			
			blockManager.processing(arg[3]);
			
			if(direction.length() == 1 && 
					(direction.equals("N") || direction.equals("S") 
							|| direction.equals("E") || direction.equals("W")))
			{
				char c = direction.charAt(0);
				
				switch(c)
				{
					case 'N':
						dz = 1;
						break;
					case 'S':
						dz = -1;
						break;
					case 'E':
						dx = 1;
						break;
					case 'W':
						dx = -1;
						break;
					default:
						error = true;
						break;
				}
			}
			else
			{
				sender.sendMessage(ChatMessage.error("Direction invalide (N/S/E/W)"));
			}
			
			if(!blockManager.isValid())
			{
				sender.sendMessage(ChatMessage.errorSyntax(command));
				error = true;
			}
			
			Location center = m_listPoint.get(arg[1], sender);
			
			if(center == null)
			{
				sender.sendMessage(ChatMessage.errorPointUndef(arg[1]));
				error = true;
			}
			
			if(error)
				return true;
			
			Location curs = center.clone();
			String text = "";
			List<List<Boolean>> bText = null;
			int width = 0;
			int height = 0;
			
			for(int i = 4; i < arg.length - 1; i++)
			{
				text += arg[i] + " ";
			}
			
			text += arg[arg.length - 1];
			
			bText = m_plugin.getListChar().getText(text);
			
			height = bText.size();
			if(height > 0)
				width = bText.get(0).size();
			
			curs.add(dx * -width / 2, height / 2, dz * -width / 2);
			
			for(int x = 0; x < width; x++)
			{
				curs.add(dx, 0, dz);
				for(int y = 0; y < height; y++)
				{
					if(bText.get(y).get(x))
					{
						if(!blockManager.contain(curs.getBlock()))
							blockManager.setBlock(curs.getBlock());
					}
					else
					{
						if(!curs.getBlock().getType().equals(Material.AIR))
							curs.getBlock().setType(Material.AIR);
					}
					
					curs.add(0, -1, 0);
				}
				
				curs.add(0, height, 0);
			}
		}
		else
		{
			sender.sendMessage(ChatMessage.errorCommand(command));
		}
		
		return true;
	}
	
	@SuppressWarnings("deprecation")
	protected static boolean trace(String path, Location location, Material material)
	{
		int number = 0;
		boolean acceptAll = false;
		
		for(int i = 0; i < path.length(); i++)
		{
			char c = path.charAt(i);
			
			if(c >= '0' && c <= '9')
			{
				number *= 10;
				number += path.charAt(i) - '0';
			}
			else if(c == '*')
			{
				acceptAll = true;
			}
			else if(c == '(' || c == ')')
			{
				if(c == '(')
				{
					int counter = 1;
					int j = 0;
					
					for(j = 0; j < number; j++)
						if(!trace(path.substring(i + 1), location, material))
							return false;
					
					j = i + 1;
					
					while(j < path.length() && counter != 0)
					{
						char d = path.charAt(j);
						
						if(d == '(')
							counter++;
						else if(d == ')')
							counter--;
						
						j++;
					}
					
					i = j - 1;
					
					number = 0;
				}
				else
				{
					return true;
				}
			}
			else if(c == 'E' || c == 'W' 
					|| c == 'N' || c == 'S' 
					|| c == 'U' || c == 'D')
			{
				int x = 0, y = 0, z = 0;
				
				switch(c)
				{
					case 'N':
						z = -1;
						break;
					case 'S':
						z = 1;
						break;
					case 'E':
						x = 1;
						break;
					case 'W':
						x = -1;
						break;
					case 'U':
						y = 1;
						break;
					case 'D':
						y = -1;
						break;
				}
				
				for(int j = 0; j < number; j++)
				{
					location.add(x, y, z);
					
					if(material != null)
					{
						location.getBlock().setType(material);
					}
					
					
				}
				number = 0;
			}
			else if(c == 'B')
			{
				material = Material.getMaterial(number);
				
				if(acceptAll)
				{
					material = null;
					acceptAll = false;
				}
				else if(material == null)
				{
					return false;
				}
				
				number = 0;
			}
			else
			{
				return false;
			}
		}
		
		return true;
	}
}
