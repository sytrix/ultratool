package thecleaner.command;

import java.util.Iterator;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandExecutor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.potion.PotionEffectType;

import thecleaner.UltraToolMain;

abstract public class UltraCommandExecutor implements CommandExecutor  {
	
	protected UltraToolMain m_plugin;
	
	public UltraCommandExecutor(UltraToolMain plugin) {
		m_plugin = plugin;
	}
	
	public Player getPlayer(String name) {
		
		Iterator<? extends Player> iterator = m_plugin.getServer().getOnlinePlayers().iterator();
		while(iterator.hasNext()) {
			Player player = iterator.next();
			if(player.getName().equals(name)) {
				return player;
			}
		}
		
		return null;
	}
	
	@SuppressWarnings("deprecation")
	public Integer getPotionEffectId(PotionEffectType potionEffectType) {
		return potionEffectType.getId();
	}
	
	@SuppressWarnings("deprecation")
	public PotionEffectType getPotionEffectFromId(Integer id) {
		return PotionEffectType.getById(id);
	}
	
	@SuppressWarnings("deprecation")
	public Integer getMaterialId(Material material) {
		return material.getId();
	}
	
	@SuppressWarnings("deprecation")
	public Material getMaterialFromId(Integer id) {
		return Material.getMaterial(id);
	}
	@SuppressWarnings("deprecation")
	public Byte getDataFromMaterialData(MaterialData materialData) {
		return materialData.getData();
	}
	
	@SuppressWarnings("deprecation")
	public Byte getDataFromBlock(Block block) {
		return block.getData();
	}
	
	@SuppressWarnings("deprecation")
	public void setDataFromBlock(Block block, byte data) {
		block.setData(data);
	}
	
	@SuppressWarnings("deprecation")
	public void setOwnerOfSkullMeta(SkullMeta skullMeta, String owner) {
		skullMeta.setOwner(owner);
	}
	
	@SuppressWarnings("deprecation")
	public Integer getEnchantId(Enchantment enchant) {
		return enchant.getId();
	}
	
	@SuppressWarnings("deprecation")
	public Enchantment getEnchantFromId(Integer id) {
		return Enchantment.getById(id);
	}
	
	@SuppressWarnings("deprecation")
	public Effect getEffectFromId(Integer id) {
		return Effect.getById(id);
	}
}
