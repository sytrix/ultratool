package thecleaner.command;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import thecleaner.UltraToolMain;
import thecleaner.fonction.ReplaceString;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;
import thecleaner.mob.GroupMobData;
import thecleaner.mob.MobData;
import thecleaner.mob.SpawnMob;

public class Spawn extends UltraCommandExecutor
{
	public Spawn(UltraToolMain plugin)
	{
		super(plugin);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Player player = null;
		BlockCommandSender commandBlock = null;
		Language language = m_plugin.getLanguage();
		
		if(sender instanceof Player)
		{
			player = (Player)sender;
		}
		else if(sender instanceof BlockCommandSender)
		{
			commandBlock = (BlockCommandSender) sender;
		}
		
		if(arg.length == 0)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.spawn.title")));
			sender.sendMessage(ChatMessage.info("/" + command + " <param...>"));
			sender.sendMessage(ChatMessage.info("/" + command + " help"));
			return true;
		}
	
		if(sender.hasPermission("ultratool.code"))
			arg = m_plugin.getScript().replace(arg);
		
		Location spawnLocation = null;
		
		if(player != null)
		{
			spawnLocation = player.getLocation();
		}
		else if(commandBlock != null)
		{
			spawnLocation = commandBlock.getBlock().getLocation().add(0.5, 0, 0.5);
			
			while(spawnLocation.getBlock().getType().isSolid())
				spawnLocation = spawnLocation.add(0, 1, 0);
		}
		
		java.util.List<String[]> listArg = ReplaceString.replace(m_plugin, spawnLocation, arg, sender);
		
		String key = arg[0].toLowerCase();
		
		if(key.equals("help"))
		{
			boolean firstPage = false;
			
			if(arg.length == 2)
			{
				if(arg[1].equals("2"))
				{
					sender.sendMessage(ChatMessage.title("Argument Group"));
					sender.sendMessage(ChatMessage.info("p:<point> - donner un point de spawn"));
					sender.sendMessage(ChatMessage.info("q:<quantiter> - nombre de r�p�tition"));
					sender.sendMessage(ChatMessage.info("r:<rayon> - applique rayon de spawn al�atoire"));
					sender.sendMessage(ChatMessage.info("xp:<quantiter> - nombre d'xp lacher par les mobs"));
					sender.sendMessage(ChatMessage.info("velocity:<x>,<y>,<z> - attribue de l'inertie au spawn"));
				}
				else
					firstPage = true;
			}
			else
				firstPage = true;
			
			if(firstPage)
			{
				sender.sendMessage(ChatMessage.title("Argument Mob"));
				sender.sendMessage(ChatMessage.info("o:<id> - invoquer une entiter a partir de son id"));
				sender.sendMessage(ChatMessage.info("drop:<kit> - donner un inventaire a un mob"));
				sender.sendMessage(ChatMessage.info("k:<kit> - donner un kit a un mob"));
				sender.sendMessage(ChatMessage.info("l:<life> - attribuer un nb de point de vie"));
				sender.sendMessage(ChatMessage.info("d:<damage> - attribuer un nb de degats"));
				sender.sendMessage(ChatMessage.info("n:<name> - nom attribuer au mob"));
				sender.sendMessage(ChatMessage.info("e:<effect>,[lvl],[time] - effet du mob (Effet de potion)"));
				sender.sendMessage(ChatMessage.info("t:<type> - type de entiter/mob"));
				sender.sendMessage(ChatMessage.info("s:<style> - style de entiter/mob"));
				sender.sendMessage(ChatMessage.info("c:<color> - couleur entiter/mob"));
				sender.sendMessage(ChatMessage.info("a:<place> - attacher � la laisse a l'entiter selon sa place"));
				sender.sendMessage(ChatMessage.info("pow:<intensiter> - puissance de l'entiter/mob"));
				sender.sendMessage(ChatMessage.info("ref:<name> - cr�er une r�f�rence"));
				sender.sendMessage(ChatMessage.info("fire:<ticks> - nombre de ticks restant avant extinction?"));
				sender.sendMessage(ChatMessage.info("b:<0/1> - b�b� mob (zombie / cochon /...)"));
				sender.sendMessage(ChatMessage.info("v:<0/1> - v�hicule de la pr�c�dente entiter/mob"));
				sender.sendMessage(ChatMessage.info("f:<0/1> - friend/ami avec l'animal?"));
				sender.sendMessage(ChatMessage.info("i:<0/1> - invincible?"));
				sender.sendMessage(ChatMessage.info("passive:<0/1> - mob pacifique?"));
				sender.sendMessage(ChatMessage.info("Page suivante " + ChatColor.DARK_PURPLE + "/uspawn help 2"));
			}
		}
		else
		{
			for(int i = 0; i < listArg.size(); i++)
				spawn(sender, listArg.get(i), spawnLocation);
		}
		
		return true;
	}
	
	private void spawn(CommandSender sender, String[] arg, Location spawnLocation)
	{
		GroupMobData groupMobData = new GroupMobData();
		
		
		groupMobData.setSpawnLocation(spawnLocation);
		java.util.List<String> listError = groupMobData.processing(m_plugin, sender, arg);
		
		if(!groupMobData.moreThanOneMob())
		{
			listError.add("Vous devez préciser la créature que vous voulez invoquer!");
			printLogError(sender, listError);
			
			return;
		}
		
		if(groupMobData.getSpawnLocation() == null)
		{
			java.util.List<MobData> listMobData = groupMobData.getListMobData();
			boolean mobExist = false;
			
			for(int i = 0; i < listMobData.size(); i++)
			{
				if(listMobData.get(i).getEntityType() != null)
					mobExist = true;
			}
			
			if(mobExist)
			{
				listError.add("Vous devez spécifier un point!");
				printLogError(sender, listError);
				
				return;
			}
		}
		
		groupMobData.compile();
		
		SpawnMob.spawn(m_plugin, groupMobData, listError);
		
		printLogError(sender, listError);
	}
	
	private void printLogError(CommandSender sender, java.util.List<String> listError)
	{
		if(!listError.isEmpty())
		{
			sender.sendMessage(ChatMessage.title("Log error"));
			
			for(int i = 0; i < listError.size(); i++)
			{
				sender.sendMessage(ChatMessage.info(listError.get(i)));
			}
		}
	}
}
