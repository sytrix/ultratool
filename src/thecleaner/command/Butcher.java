package thecleaner.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Hanging;
import org.bukkit.entity.NPC;
import org.bukkit.entity.Player;

import thecleaner.UltraToolMain;
import thecleaner.fonction.CheckClass;
import thecleaner.fonction.KillEntity;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;
import thecleaner.mob.FindMob;
import thecleaner.mob.GroupMobData;
import thecleaner.mob.MobData;

public class Butcher extends UltraCommandExecutor
{
	public Butcher(UltraToolMain plugin) {
		super(plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Player player = null;
		BlockCommandSender commandBlock = null;
		Language language = m_plugin.getLanguage();
		
		if(sender instanceof Player)
		{
			player = (Player)sender;
		}
		else if(sender instanceof BlockCommandSender)
		{
			commandBlock = (BlockCommandSender) sender;
		}
		
		if(arg.length == 0)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.butcher.help_title")));
			sender.sendMessage(ChatMessage.info("/" + command + " <param...>"));
			return true;
		}
		
		if(sender.hasPermission("ultratool.code"))
			arg = m_plugin.getScript().replace(arg);
		
		GroupMobData groupMobData = new GroupMobData();
		List<String> listError = new ArrayList<String>();
		Location location = null;
		
		if(player != null)
			location = player.getLocation();
		if(commandBlock != null)
			location = commandBlock.getBlock().getLocation().add(0.5, 0.5, 0.5);
		
		groupMobData.setSpawnLocation(location);
		listError = groupMobData.processing(m_plugin, sender, arg);
		
		if(groupMobData.getRadius() == null)
			groupMobData.setRadius(50);
		
		if(groupMobData.getSpawnLocation() != null)
		{
			List<Entity> listEntity = FindMob.find(m_plugin, groupMobData, listError);
			
			for(int i = 0; i < listEntity.size(); i++)
			{
				Entity entity = listEntity.get(i);
				
				if(!entity.getType().equals(EntityType.PLAYER))
				{
					if(entity instanceof Hanging || entity instanceof NPC || 
							entity instanceof EnderCrystal || 
							(CheckClass.existArmorStand() && entity instanceof ArmorStand))
					{
						List<MobData> listMobData = groupMobData.getListMobData();
						boolean mustDelete = false;
						
						for(int j = 0; j < listMobData.size(); j++)
						{
							EntityType entityType = listMobData.get(j).getEntityType();
							
							if(entityType == null && listMobData.get(j).getEntity() != null)
								entityType = listMobData.get(j).getEntity().getType();
							
							if(entityType != null && entityType.equals(entity.getType()))
								mustDelete = true;
						}
						
						if(mustDelete) {
							KillEntity.process(m_plugin, entity);
							entity.remove();
						}
					}
					else {
						KillEntity.process(m_plugin, entity);
						entity.remove();
					}
				}
			}
		}
		
		printLogError(sender, listError);
		
		return true;
	}
	
	private void printLogError(CommandSender sender, java.util.List<String> listError)
	{
		if(!listError.isEmpty())
		{
			sender.sendMessage(ChatMessage.title("Log error"));
			
			for(int i = 0; i < listError.size(); i++)
			{
				sender.sendMessage(ChatMessage.info(listError.get(i)));
			}
		}
	}
	
}
