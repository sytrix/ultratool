package thecleaner.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import thecleaner.UltraToolMain;
import thecleaner.fonction.KitManager;
import thecleaner.fonction.ProcessString;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;

public class Item extends UltraCommandExecutor
{
	public Item(UltraToolMain plugin)
	{
		super(plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Player player = null;
		Language language = m_plugin.getLanguage();
		
		if(sender instanceof Player)
		{
			player = (Player)sender;
		}
		
		if(arg.length == 0)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.item.title")));
			sender.sendMessage(ChatMessage.info("/" + command + " id"));
			sender.sendMessage(ChatMessage.info("/" + command + " hat"));
			sender.sendMessage(ChatMessage.info("/" + command + " head <player>"));
			sender.sendMessage(ChatMessage.info("/" + command + " give <id/name> [amount]"));
			sender.sendMessage(ChatMessage.info("/" + command + " copy"));
			sender.sendMessage(ChatMessage.info("/" + command + " name <name>"));
			sender.sendMessage(ChatMessage.info("/" + command + " lore <description>"));
			sender.sendMessage(ChatMessage.info("/" + command + " enchant <enchantment> [level]"));
			
			return true;
		}
		
		if(player == null)
		{
			sender.sendMessage(ChatMessage.errorMustBePlayer());
			return true;
		}
		
		String key = arg[0].toLowerCase();
		if(sender.hasPermission("ultratool.code"))
			arg = m_plugin.getScript().replace(arg);
		
		if(key.equals("id"))
		{
			if(arg.length != 1)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			ItemStack hand = KitManager.getItemHand(player);
			
			if(hand == null || hand.getType().equals(Material.AIR))
			{
				sender.sendMessage(ChatMessage.error(language.get("command.item.no_item_hand")));
				return true;
			}
			
			Material handMaterial = hand.getType();
			int handIntData = this.getMaterialId(handMaterial);
			byte handDataMat = this.getDataFromMaterialData(hand.getData());
			
			
			player.sendMessage(ChatMessage.info(language.getElement("command.item.id_name", 
					ProcessString.niceString(handMaterial.name()) + ":" + 
					ProcessString.niceString(hand.getData().toString()))));
			player.sendMessage(ChatMessage.info(language.getElement("command.item.id_number", 
					handIntData + ":" + handDataMat)));
		}
		else if(key.equals("hat"))
		{
			if(arg.length != 1)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			ItemStack helmet = KitManager.getHelmet(player);
			ItemStack hand = KitManager.getItemHand(player);
			
			if(helmet != null && hand != null
					&& !helmet.getType().equals(Material.AIR) 
					&& !hand.getType().equals(Material.AIR))
			{
				if(hand != null)
				{
					KitManager.setHelmet(player, hand);
					
					if(helmet != null)
						KitManager.setItemHand(player, helmet);
				}
			}
			else if(hand != null && !hand.getType().equals(Material.AIR))
			{
				KitManager.setHelmet(player, hand);
				KitManager.setItemHand(player, null);
			}
			else
			{
				sender.sendMessage(ChatMessage.error(language.get("command.item.no_item_hand")));
			}
		}
		else if(key.equals("head"))
		{
			if(arg.length != 2)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short)SkullType.PLAYER.ordinal());
			
			SkullMeta skullMeta = (SkullMeta)head.getItemMeta();
			this.setOwnerOfSkullMeta(skullMeta, arg[1]);
			skullMeta.setDisplayName(arg[1]);
			head.setItemMeta(skullMeta);
			
			
			player.getEquipment().setHelmet(head);
		
		}
		else if(key.equals("give"))
		{
			if(arg.length != 2 && arg.length != 3)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			Material mat = null;
			Integer id = null;
			
			id = ProcessString.parseInt(arg[1]);
			
			if(id != null)
			{
				mat = this.getMaterialFromId(id);

				if(mat == null)
				{
					sender.sendMessage(ChatMessage.errorOutOfRange(id));
					return true;
				}
			}
			else
			{
				Material[] materials = Material.values();
				
				for(int i = 0; i < materials.length; i++) {
					if(materials[i].name().toLowerCase().equals(arg[1].toLowerCase())) {
						mat = materials[i];
					}
				}
				
				if(mat == null)
				{
					player.sendMessage(ChatMessage.errorNameUndef(arg[1]));
					return true;
				}
			}
			
			ItemStack itemStack = new ItemStack(mat);
			if(arg.length == 3) {
				Integer amount = ProcessString.parseInt(arg[2]);
				if(amount != null) {
					itemStack.setAmount(amount);
				} else {
					sender.sendMessage(ChatMessage.errorNotInteger(arg[2]));
					return true;
				}
			}
			
			player.getInventory().addItem(itemStack);
		
		}
		else if(key.equals("copy"))
		{
			if(arg.length != 1)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			ItemStack hand = KitManager.getItemHand(player);
			
			if(hand == null || hand.getType().equals(Material.AIR))
			{
				sender.sendMessage(ChatMessage.error(language.get("command.item.no_copy_air")));
				return true;
			}
				
			player.getInventory().addItem(hand);
		}
		else if((key.equals("name") || key.equals("lore")))
		{
			if(arg.length < 2)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			ItemStack hand = KitManager.getItemHand(player);
			
			if(hand == null || hand.getType().equals(Material.AIR))
			{
				sender.sendMessage(ChatMessage.error(language.get("command.item.no_item_hand")));
				return true;
			}
			
			ItemMeta itemMeta = hand.getItemMeta();
			String strScan = "";
			
			for(int i = 1; i < arg.length; i++)
			{
				strScan += arg[i].replace('&', '�');
				
				if(i < arg.length - 1)
					strScan += " ";
			}
			
			if(key.equals("lore"))
			{
				List<String> listString = new ArrayList<String>();
				String[] splitString = strScan.split("�r");
				
				for(int i = 0; i < splitString.length; i++)
				{
					listString.add(splitString[i]);
				}
				
				itemMeta.setLore(listString);
			}
			else if(key.equals("name"))
			{
				itemMeta.setDisplayName(strScan);
			}
			
			hand.setItemMeta(itemMeta);
		}
		else if(key.equals("enchant"))
		{
			if(arg.length != 2 && arg.length != 3)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			ItemStack hand = KitManager.getItemHand(player);
			
			if(hand == null || hand.getType().equals(Material.AIR))
			{
				sender.sendMessage(ChatMessage.error(language.get("command.item.no_item_hand")));
				return true;
			}
			
			Enchantment enchantment = null;
			Integer id = null;
			Integer level = null;
			
			id = ProcessString.parseInt(arg[1]);
			
			if(id != null)
			{
				enchantment = this.getEnchantFromId(id);
				
				if(enchantment == null)
				{
					sender.sendMessage(ChatMessage.errorOutOfRange(id));
					return true;
				}
			}
			else
			{
				enchantment = Enchantment.getByName(arg[1].toUpperCase());
				
				if(enchantment == null)
				{
					sender.sendMessage(ChatMessage.errorNameUndef(arg[1]));
					return true;
				}
			}
			
			if(arg.length == 3)
			{
				level = ProcessString.parseInt(arg[2]);
			
				if(level == null)
				{
					sender.sendMessage(ChatMessage.errorNotInteger(arg[2]));
					return true;
				}
			}
			else
				level = 1;
			
			String enchantmentName = ProcessString.niceString(enchantment.getName());
			
			if(enchantment.canEnchantItem(hand))
			{
				if(level > 0 && level <= enchantment.getMaxLevel())
				{
					hand.addEnchantment(enchantment, level);
					sender.sendMessage(ChatMessage.info(
							language.getElement("command.item.enchantment_add", 
									enchantmentName)));
				}
				else
				{
					hand.addUnsafeEnchantment(enchantment, level);
					sender.sendMessage(ChatMessage.info(
							language.getElement("command.item.enchantment_over_max_level", 
									enchantmentName)));
				}
				
			}
			else
			{
				hand.addUnsafeEnchantment(enchantment, level);
				sender.sendMessage(ChatMessage.info(
						language.getElement("command.item.enchantment_useless", 
								enchantmentName)));
			}

		}
		else
			sender.sendMessage(ChatMessage.errorCommand(command));
		
		return true;
	}
}
