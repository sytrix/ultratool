package thecleaner.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import thecleaner.UltraToolMain;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;

public class Send extends UltraCommandExecutor
{
	public Send(UltraToolMain plugin)
	{
		super(plugin);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Language language = m_plugin.getLanguage();
		
		if(arg.length == 0)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.send.title")));
			sender.sendMessage(ChatMessage.info("/" + command + " <player> <message>"));
			return true;
		}
		
		String argCompile = "";
		for(int i = 1; i < arg.length; i++)
		{
			argCompile += arg[i] + " ";
		}
		
		if(sender.hasPermission("ultratool.code"))
			argCompile = m_plugin.getScript().replace(argCompile);
		
		Player client = this.getPlayer(arg[0]);
		String message = "";
		
		if(client != null)
		{
			message = argCompile.replace('&', '§');
			
			client.sendMessage(message);
		}
		else
		{
			sender.sendMessage(ChatMessage.errorPlayerNotFound(arg[0]));
		}
		
		return true;
	}

}
