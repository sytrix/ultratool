package thecleaner.command;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import thecleaner.UltraToolMain;
import thecleaner.list.ListPoint;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;

public class Compass extends UltraCommandExecutor
{
	private ListPoint m_listPoint;
	
	public Compass(UltraToolMain plugin)
	{
		super(plugin);
		m_listPoint = m_plugin.getListPoint();
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Player player = null;
		Language language = m_plugin.getLanguage();
		
		if(sender instanceof Player)
		{
			player = (Player)sender;
		}
		
		if(arg.length == 0)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.compass.title")));
			sender.sendMessage(ChatMessage.info("/" + command + " set <point> [player]"));
			return true;
		}
	
		if(sender.hasPermission("ultratool.code"))
			arg = m_plugin.getScript().replace(arg);
		
		
		String key = arg[0].toLowerCase();
		
		if(key.equals("set"))
		{
			if(arg.length == 2 || arg.length == 3)
			{
				Player client = null;
				Location compassLocation = m_listPoint.get(arg[1], sender);
				
				if(compassLocation == null)
				{
					sender.sendMessage(ChatMessage.errorPointUndef(arg[1]));
				}
				
				if(arg.length == 2)
				{
					if(player != null)
						client = player;
					else
					{
						sender.sendMessage(ChatMessage.errorMustBePlayer());
						return true;
					}
				}
				else if(arg.length == 3)
				{
					client = this.getPlayer(arg[2]);
					
					if(client == null)
					{
						sender.sendMessage(ChatMessage.errorPlayerNotFound(arg[2]));
						return true;
					}
				}
				
				if(compassLocation != null)
				{
					if(client != null)
					{
						client.setCompassTarget(compassLocation);
					}
				}
			}
			else
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
			}
		}
		else
		{
			sender.sendMessage(ChatMessage.errorCommand(command));
		}
		
		return true;
	}
}
