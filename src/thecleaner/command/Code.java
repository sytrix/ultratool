package thecleaner.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import sytrix.parser.PathFindingException;
import sytrix.parser.Token;
import thecleaner.UltraToolMain;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;
import thecleaner.script.Script;

public class Code extends UltraCommandExecutor
{
	private Script m_script;
	
	public Code(UltraToolMain plugin)
	{
		super(plugin);
		m_script = m_plugin.getScript();
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] args) 
	{
		Language language = m_plugin.getLanguage();
		
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			
			if(!player.isOp())
			{
				sender.sendMessage(ChatMessage.error(ChatMessage.errorMustBeOp()));
				return true;
			}
		}
		
		if(args.length == 0)
		{
			
			sender.sendMessage(ChatMessage.title(language.get("command.code.title")));
			sender.sendMessage(ChatMessage.info("/" + command + " <code...>"));
			return true;
		}
		
		StringBuilder code = new StringBuilder();
		for(String arg : args) {
			code.append(arg);
			code.append(' ');
		}
		
		
		try {
			Object object = m_script.execute(code.toString());
			if(object == null) {
				sender.sendMessage(ChatMessage.info("NULL"));
			} else {
				sender.sendMessage(ChatMessage.info(object.getClass().getSimpleName() + ":" + object.toString()));
			}
		} catch (PathFindingException e) {
			Token badToken = e.getBadToken();
			StringBuilder cursor = new StringBuilder();
			for(int i = 0; i < badToken.getIdxBeg() + 3; i++) {
				cursor.append(' ');
			}
			for(int i = 0; i < badToken.getLength(); i++) {
				cursor.append('^');
			}
			//sender.sendMessage("§3" + badToken.toString());
			System.err.println("Compilation error with this code : " + code);
			sender.sendMessage(ChatMessage.error(code.toString()));
			sender.sendMessage(ChatMessage.error(cursor.toString()));
			sender.sendMessage(ChatMessage.error("Unexpected token type '" + badToken.getType() + "' and value '" + badToken.getValue() + "'"));
		} catch (Exception e) {
			System.err.println("Compilation error with this code : " + code);
			e.printStackTrace();
			sender.sendMessage(ChatMessage.error("Error with code : " + code));
			/*
			Integer charIndex = e.getCharIndex();
			if(charIndex != null) {
				int nbSpace = e.getCharIndex();
				StringBuilder s = new StringBuilder();
				for(int i = 0; i < nbSpace; i++) {
					s.append(' ');
				}
				s.append('^');
				sender.sendMessage("§5" + s.toString());
			}
			sender.sendMessage("§5Error : " + e.getMessage());
			*/
		}
		
		
		/*
		try {
			sender.sendMessage(m_script.execute(code.toString()).toString());
		} catch (EvalSyntaxException e) {
			sender.sendMessage("�5" + code);
			Integer charIndex = e.getCharIndex();
			if(charIndex != null) {
				int nbSpace = e.getCharIndex();
				StringBuilder s = new StringBuilder();
				for(int i = 0; i < nbSpace; i++) {
					s.append(' ');
				}
				s.append('^');
				sender.sendMessage("�5" + s.toString());
			}
			sender.sendMessage("�5Error : " + e.getMessage());
		}
		
		
		/*
		String key = arg[0].toLowerCase();
		
		if(key.equals("help") && arg.length == 1)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.code.help_title")));
			sender.sendMessage(ChatMessage.info(language.get("command.code.help_content")));
		}
		else if(key.equals("list") && arg.length == 1)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.code.list_title")));
			Iterator<Entry<String, Object>> iterator = m_plugin.getJSCode().getListVar().iterator();
			String list = "";
			
			while(iterator.hasNext())
			{
				Entry<String, Object> element = iterator.next();
				Object value = element.getValue();
				
				if(value instanceof Double
						|| value instanceof String
						|| value instanceof Integer
						|| value instanceof Boolean
						|| value instanceof ScriptObject
						|| value instanceof JSArray
						|| value == null)
				{
					String className = "";
					String resultValue = ""; 
					String keyName = element.getKey();
					
					if(value != null)
					{
						className = element.getValue().getClass().getSimpleName();
						resultValue = element.getValue().toString();
					}
					else
					{
						className = "[Undef]";
						resultValue = "null";
					}
					
					if(value instanceof String)
					{
						resultValue = "\"" + resultValue + "\"";
					}
					
					list += ChatMessage.show(className + " " + keyName + " = ");
					list += ChatMessage.info(resultValue) + "\n";
				}
			}
			
			sender.sendMessage(list);
		}
		else if(key.equals("clear") && arg.length == 1)
		{
			m_plugin.getJSCode().clear();
		}
		else
		{
			String line = "";
			for(int i = 0; i < arg.length; i++)
			{
				line += arg[i] + " ";
			}
			
			Object res = m_jsCode.execute(line, sender);
			
			if(res instanceof Boolean)
			{
				Boolean boolRes = (Boolean)res;
				
				if(commandBlock != null)
					ConditionBlock.active(m_plugin, commandBlock.getBlock().getLocation(), boolRes);
			}
			
			if(res != null)
			{
				String className = res.getClass().getSimpleName();
				String resultValue = res.toString();
				
				if(res instanceof String)
				{
					resultValue = "\"" + resultValue + "\"";
				}
				else if(res instanceof JSArray)
				{
					resultValue = ((JSArray)res).toString(true);
				}
				
				if(commandBlock == null)
					sender.sendMessage(
							ChatMessage.show(className + ": ") + 
							ChatMessage.info(resultValue));
			}
			else
			{
				if(commandBlock == null)
					sender.sendMessage(ChatMessage.info(language.get("command.code.result_is_null")));
			}
		}*/
		
		return true;
	}
}
