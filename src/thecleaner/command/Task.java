package thecleaner.command;

import org.bukkit.Location;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import thecleaner.UltraToolMain;
import thecleaner.fonction.ProcessString;
import thecleaner.list.ListTask;
import thecleaner.message.ChatMessage;
import thecleaner.message.Language;

public class Task extends UltraCommandExecutor
{
	private ListTask m_listTask;
	
	public Task(UltraToolMain plugin)
	{
		super(plugin);
		m_listTask = m_plugin.getListTask();
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String command, String[] arg) 
	{
		Language language = m_plugin.getLanguage();
		int nbArg = arg.length;
		Player player = null;
		BlockCommandSender commandBlock = null;
		
		
		if(sender instanceof Player)
		{
			player = (Player) sender;
			
			if(!player.isOp())
			{
				sender.sendMessage(ChatMessage.error(ChatMessage.errorMustBeOp()));
				return true;
			}
		}
		else if(sender instanceof BlockCommandSender)
		{
			commandBlock = (BlockCommandSender) sender;
		}
		
		if(nbArg == 0)
		{
			sender.sendMessage(ChatMessage.title(language.get("command.task.title")));
			sender.sendMessage(ChatMessage.info("/" + command + " set <task> <command...>"));
			sender.sendMessage(ChatMessage.info("/" + command + " del <task>"));
			sender.sendMessage(ChatMessage.info("/" + command + " info <task>"));
			sender.sendMessage(ChatMessage.info("/" + command + " list"));
			sender.sendMessage(ChatMessage.info("/" + command + " startRepeat <task> <interval>"));
			sender.sendMessage(ChatMessage.info("/" + command + " stopRepeat <task>"));
			//sender.sendMessage(ChatMessage.info("/" + command + " addEvent <task> <event>"));
			//sender.sendMessage(ChatMessage.info("/" + command + " delEvent <task> <event>"));
			//sender.sendMessage(ChatMessage.info("/" + command + " listEvent"));
			return true;
		}
		
		String key = arg[0].toLowerCase();
		
		
		if(!key.equals("set"))
		{
			if(sender.hasPermission("ultratool.code"))
				arg = m_plugin.getScript().replace(arg);
		}
		
		nbArg = arg.length;
		
		if(key.equals("set"))
		{
			if(arg.length < 3)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			Location location = null;
			String commandCompile = "";
			boolean error = false;
			
			if(player != null)
			{
				location = player.getLocation();
			}
			else if(commandBlock != null)
			{
				location = commandBlock.getBlock().getLocation();
			}
			else
			{
				sender.sendMessage(ChatMessage.errorNoConsole());
				error = true;
			}
			
			if(!ProcessString.isCorrectString(arg[1]))
			{
				sender.sendMessage(ChatMessage.errorNotAlphaNum(arg[1]));
				error = true;
			}
			
			if(error)
				return true;
			
			for(int i = 2; i < arg.length; i++)
			{
				commandCompile += arg[i] + " ";
			}
			
			sender.sendMessage(ChatMessage.info(language.getElement("command.task.set", arg[1])));
			m_listTask.put(arg[1], location, commandCompile);
		}
		else if(key.equals("del"))
		{
			if(arg.length != 2)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			if(!m_listTask.contains(arg[1]))
			{
				sender.sendMessage(ChatMessage.errorTaskUndef(arg[1]));
				return true;
			}
			
			sender.sendMessage(ChatMessage.info(language.getElement("command.task.del", arg[1])));
			m_listTask.del(arg[1]);
		}
		else if(key.equals("info"))
		{
			if(arg.length != 2)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			if(!m_listTask.contains(arg[1]))
			{
				sender.sendMessage(ChatMessage.errorTaskUndef(arg[1]));
				return true;
			}
			
			java.util.List<String> listEventName = m_listTask.getEvents(arg[1]);
			Integer tick = m_listTask.getTick(arg[1]);
			
			sender.sendMessage(ChatMessage.title("Info task: " + arg[1]));
			sender.sendMessage(ChatMessage.show("Command: ") + ChatMessage.info(m_listTask.getCommand(arg[1])));
			if(tick != null && tick > 0)
				sender.sendMessage(ChatMessage.show("Delay: ") + ChatMessage.info(tick + "t"));
			sender.sendMessage(ChatMessage.show("Events name: ") + ChatMessage.info(listEventName.toString()));
			sender.sendMessage(ChatMessage.show("Last message returned: ") + ChatMessage.info(m_listTask.getLastMessage(arg[1])));
			sender.sendMessage(ChatMessage.show("Stack Trace Error: ") + ChatMessage.info(m_listTask.getStackTrace(arg[1])));
			sender.sendMessage(ChatMessage.show("Execution point: ") + 
					ChatMessage.info(ProcessString.getStringLocation(m_listTask.getLocation(arg[1]))));
		}
		else if(key.equals("list"))
		{
			if(arg.length != 1)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			sender.sendMessage(ChatMessage.title(language.get("command.task.list_title")));
			sender.sendMessage(m_listTask.getListFormat());
		}
		else if(key.equals("startrepeat"))
		{
			if(arg.length != 3)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			Integer tick = ProcessString.toTick(arg[2]);
			boolean error = false;
			
			if(!ProcessString.isCorrectString(arg[1]))
			{
				sender.sendMessage(ChatMessage.errorNotAlphaNum(arg[1]));
				error = true;
			}
			
			if(tick == null)
			{
				sender.sendMessage(ChatMessage.errorNameUndef(arg[2]));
				error = true;
			}
			else
			{
				if(tick <= 0)
				{
					sender.sendMessage(ChatMessage.error(language.get("command.task.tick_neg_or_null")));
					error = true;
				}
			}
			
			if(!m_listTask.contains(arg[1]))
			{
				sender.sendMessage(ChatMessage.errorTaskUndef(arg[1]));
				error = true;
			}
			
			if(error)
				return true;
			
			sender.sendMessage(ChatMessage.info(language.getElement("command.task.set", arg[1])));
			m_listTask.setRepeat(arg[1], tick);
		}
		else if(key.equals("stoprepeat"))
		{
			if(arg.length != 2)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			boolean error = false;
			
			if(!m_listTask.contains(arg[1]))
			{
				sender.sendMessage(ChatMessage.errorTaskUndef(arg[1]));
				error = true;
			}
			
			if(error)
				return true;
			
			sender.sendMessage(ChatMessage.info(language.getElement("command.task.set", arg[1])));
			m_listTask.setRepeat(arg[1], null);
		}
		/*
		else if(key.equals("addevent"))
		{
			if(arg.length != 3)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			String eventName = arg[2];
			boolean error = false;
			
			if(!EventEnum.contains(eventName))
			{
				sender.sendMessage(ChatMessage.errorNameUndef(arg[2]));
				error = true;
			}
			
			if(!m_listTask.contains(arg[1]))
			{
				sender.sendMessage(ChatMessage.errorTaskUndef(arg[1]));
				error = true;
			}
			
			if(error)
				return true;
			
			sender.sendMessage(ChatMessage.info(language.getElement("command.task.set", arg[1])));
			m_listTask.addEvent(arg[1], eventName);
			m_plugin.getListEvent().enable(eventName);
		}
		else if(key.equals("delevent"))
		{
			if(arg.length != 3)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			String eventName = arg[2];
			boolean error = false;
			
			if(!EventEnum.contains(eventName))
			{
				sender.sendMessage(ChatMessage.errorNameUndef(arg[2]));
				error = true;
			}
			
			if(!m_listTask.contains(arg[1]))
			{
				sender.sendMessage(ChatMessage.errorTaskUndef(arg[1]));
				error = true;
			}
			
			if(error)
				return true;
			
			sender.sendMessage(ChatMessage.info(language.getElement("command.task.set", arg[1])));
			m_listTask.removeEvent(arg[1], eventName);
		}
		else if(key.equals("listevent"))
		{
			if(arg.length != 1)
			{
				sender.sendMessage(ChatMessage.errorNbArg(command));
				return true;
			}
			
			EventEnum[] listEvent = EventEnum.values();
			String listEventName = "";
			
			for(int i = 0; i < listEvent.length; i++)
			{
				listEventName += listEvent[i].toString() + ", ";
			}
			
			sender.sendMessage(ChatMessage.title(language.get("command.task.list_event_title")));
			sender.sendMessage(ChatMessage.info(listEventName));
		}*/
		else
		{
			sender.sendMessage(ChatMessage.errorCommand(command));
			return true;
		}
		
		return true;
	}
}




