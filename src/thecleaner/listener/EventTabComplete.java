package thecleaner.listener;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.TabCompleteEvent;

import thecleaner.UltraToolMain;

public class EventTabComplete implements Listener {
	
	private UltraToolMain m_plugin;
	
	public EventTabComplete(UltraToolMain plugin)
	{
		m_plugin = plugin;
	}
	
	@EventHandler
	public void onTabComplete(TabCompleteEvent tabCompleteEvent)
	{
		CommandSender sender = tabCompleteEvent.getSender();
		String buffer = tabCompleteEvent.getBuffer();
		List<String> completions = tabCompleteEvent.getCompletions();
		
		if(sender.isOp() || sender.isPermissionSet("ultratool.tab"))
		{
			if(buffer.length() > 0 && (buffer.charAt(0) == '/' || sender instanceof Player))
			{
				m_plugin.getListCommand().reloadCommand();
				Map<String, Object> listCommandSlash = m_plugin.getListCommand().getSlashCommand();
				Map<String, Object> listCommandNoSlash = m_plugin.getListCommand().getNoSlashCommand();
				Map<String, Object> listCommand = listCommandSlash;
				
				String[] splitBuffer = buffer.split(" ", -1);
				boolean isPluginCommand = false;
				boolean isSpawnSyntax = false;
				boolean isExecSyntax = false;
				
				for(int i = 0; i < splitBuffer.length; i++)
				{
					Object nextListCommand = null;
					boolean exeBeginAnotherCommand = false;
					
					if(i == 0) {
						nextListCommand = listCommand.get(splitBuffer[i]);
						
						if(nextListCommand == null) {
							nextListCommand = listCommandNoSlash.get(splitBuffer[i]);
						}
						
						if(nextListCommand == null) {
							return;
						} else {
							isPluginCommand = true;
						}
						
						if(splitBuffer[i].equals("/ubutcher") || splitBuffer[i].equals("/uspawn") ||
							splitBuffer[i].equals("ubutcher") || splitBuffer[i].equals("uspawn")) {
							isSpawnSyntax = true;
						} else if(splitBuffer[i].equals("/uexe") || 
								splitBuffer[i].equals("uexe")) {
							isExecSyntax = true;
							
						}
					} else {
						
						if(isExecSyntax && splitBuffer[i].startsWith(">")) {
							nextListCommand = listCommandNoSlash;
							isSpawnSyntax = false;
							isPluginCommand = true;
						} else {
							
							if(!isSpawnSyntax) {
								if(listCommand != null) {
									nextListCommand = listCommand.get(splitBuffer[i]);
									isPluginCommand = true;
								}
								
								if(splitBuffer[i].equals("ubutcher") || splitBuffer[i].equals("uspawn")) {
									isSpawnSyntax = true;
								} else if(splitBuffer[i].equals("uexe")) {
									isExecSyntax = true;
									nextListCommand = listCommandNoSlash;
								}
							}
						}
					}
					
					if(i == splitBuffer.length - 1) {
						if(isPluginCommand || exeBeginAnotherCommand) {
							completions.clear();
							
							if(listCommand != null) {
								Iterator<String> keys = listCommand.keySet().iterator();
								
								while(keys.hasNext())
								{
									String key = keys.next();
									if(key.startsWith(splitBuffer[i]))
									{
										completions.add(key);
									}
								}
							}
							
							if(isExecSyntax && splitBuffer[i].startsWith(">")) {
								completions.add(">");
								completions.add(">unique:");
							}
						}
					} else if(nextListCommand != null && nextListCommand instanceof Map) {
						listCommand = this.castObj(nextListCommand);
					} else  {
						if(!isSpawnSyntax) {
							listCommand = null;
						}
					}
					
				}
				
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, Object> castObj(Object obj) 
	{
		return (Map<String, Object>) obj;
	}
}
