package thecleaner.listener;

import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import thecleaner.UltraToolMain;
import thecleaner.list.ListEntityProperty.EntityProperty;


public class EventDamage implements Listener
{
	UltraToolMain m_plugin;
	
	public EventDamage(UltraToolMain plugin)
	{
		m_plugin = plugin;
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent entityDamage)
	{
		EntityProperty entityProperty = m_plugin.getListEntityProperty().getProperty(entityDamage.getEntity());
		if(entityProperty != null && entityProperty.isInvincible())
		{
			entityDamage.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onDamageOtherEntity(EntityDamageByEntityEvent entityDamageByEntity)
	{
		Entity damager = entityDamageByEntity.getDamager();
		EntityProperty entityProperty = m_plugin.getListEntityProperty().getProperty(damager);
		
		/*
		if(entityDamager instanceof Projectile)
		{
			Projectile projectile = (Projectile)entityDamager;
			if(!listEntityDamage.contains(entityDamager))
			{
				entityDamager = projectile._INVALID_getShooter();
			}
			
			if(listEntityDamage.contains(entityDamager))
			{
				if(listEntityDamage.getValue(entityDamager) == 0)
					entityDamager.setFireTicks(0);
			}
		}*/
		
		if(entityProperty != null && entityProperty.getDamage() != null)
		{
			entityDamageByEntity.setDamage(entityProperty.getDamage());
		}
	}
	
	@EventHandler
	public void onProjectileDamage(EntityChangeBlockEvent entityChangeBlock)
	{
		Entity entity = entityChangeBlock.getEntity();
		EntityProperty entityProperty = m_plugin.getListEntityProperty().getProperty(entity);
		
		/*
		if(entity instanceof Projectile)
		{
			Projectile projectile = (Projectile)entity;
			
			if(!m_plugin.getListEntityPassive().contains(entity) && !m_plugin.getListEntityValueDamage().contains(entity))
			{
				entity = projectile._INVALID_getShooter();
			}
		}
		*/
		
		if(entityProperty != null) {
			if(entityProperty.isPassive()) {
				entityChangeBlock.setCancelled(true);
			}
			
			if(entityProperty.getDamage() != null && entityProperty.getDamage() == 0.0) {
				entityChangeBlock.setCancelled(true);
			}
		}
	}
}
