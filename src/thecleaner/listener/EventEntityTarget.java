package thecleaner.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;

import thecleaner.UltraToolMain;
import thecleaner.list.ListEntityProperty.EntityProperty;


public class EventEntityTarget implements Listener
{
	UltraToolMain m_plugin;
	
	public EventEntityTarget(UltraToolMain plugin)
	{
		m_plugin = plugin;
	}
	
	@EventHandler
	public void onProjectileLaunch(ProjectileLaunchEvent projectileLaunch)
	{
		/*
		ProjectileSource projectileSource = projectileLaunch.getEntity().getShooter();
		
		if(projectileSource instanceof LivingEntity)
		{
			if(m_plugin.getListEntityPassive().contains(projectileSource))
			{
				projectileLaunch.setCancelled(true);
			}
		}*/
	}
	
	@EventHandler
	public void onTarget(EntityTargetEvent entityTarget)
	{
		EntityProperty entityProperty = m_plugin.getListEntityProperty().getProperty(entityTarget.getEntity());
		if(entityProperty != null && entityProperty.isPassive())
		{
			entityTarget.setCancelled(true);
		}
	}
	
}
