package thecleaner.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldLoadEvent;

import thecleaner.UltraToolMain;


public class EventWorldLoad implements Listener
{
	UltraToolMain m_plugin;
	
	public EventWorldLoad(UltraToolMain plugin)
	{
		m_plugin = plugin;
	}
	
	
	@EventHandler
	public void onLoad(WorldLoadEvent worldLoad)
	{
		m_plugin.getListPoint().reload();
		m_plugin.getListZone().reload();
	}
}