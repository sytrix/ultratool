package thecleaner.listener;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import thecleaner.UltraToolMain;
import thecleaner.fonction.KitManager;

public class EventTool implements Listener
{
	
	public EventTool(UltraToolMain plugin)
	{
	}
	
	@EventHandler
	public static void onClic(PlayerInteractEvent playerInteract)
	{
		Action action = playerInteract.getAction();
		Player player = playerInteract.getPlayer();
		
		if(!player.hasPermission("ultratool.tool"))
			return;
		
		ItemStack itemHand = KitManager.getItemHand(player);
		
		if(itemHand == null)
			return;
		
		ItemMeta metaHand = itemHand.getItemMeta();
		
		if(metaHand == null)
			return;
		
		String itemName = metaHand.getDisplayName();
		
		if(itemName == null)
			return;
		
		Material material = itemHand.getType();
		
		if(material == null)
			return;
		
		Integer radius = null;
		
		if(itemHand.containsEnchantment(Enchantment.SILK_TOUCH))
			radius = itemHand.getEnchantmentLevel(Enchantment.SILK_TOUCH);
		else return;
		
		Integer pow = null;
		if(itemHand.containsEnchantment(Enchantment.KNOCKBACK))
			pow = itemHand.getEnchantmentLevel(Enchantment.KNOCKBACK);
		else return;
		
		if(material.equals(Material.STICK))
		{
			if(action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK))
			{
				/*
				List<World> worlds = m_plugin.getServer().getWorlds();
				for(World w : worlds) {
					if(player.getWorld() == w) {
						List<Entity> entities = w.getEntities();
						for(Entity e : entities) {
							double distance = e.getLocation().distance(player.getLocation());
							if(distance < 15) {
								player.sendMessage(ChatColor.LIGHT_PURPLE + "[" + e.getType().toString() + "] " + e.getUniqueId().toString() + " (" + distance + ")");
							}
						}
					}
				}*/
				/*
				int size = m_plugin.getListEntityInvincible().size();
				player.sendMessage(ChatColor.DARK_BLUE + "Nb: " + size);
				for(int i = 0 ; i < size; i++) {
					player.sendMessage(ChatColor.DARK_BLUE + "Invincible : " + m_plugin.getListEntityInvincible().get(i));
					player.sendMessage(ChatColor.DARK_BLUE + "UUID : " + m_plugin.getServer().getEntity(UUID.fromString(m_plugin.getListEntityInvincible().getUUID(i))));
				}*/
				
				if(itemName.startsWith(ChatColor.GOLD + "aspiration"))
					aspiration(player, radius, pow);
				else if(itemName.startsWith(ChatColor.GOLD + "repulsion"))
					repulse(player, radius, pow);
				
				playerInteract.setCancelled(true);
			}
		}
	}
	
	public static void repulse(Player player, int radius, double lvl)
	{
		List<Entity> listEntity = player.getWorld().getEntities();
		Location playerLocation = player.getLocation();
		Vector vec = new Vector(0, 0, 0);
		
		
		for(int i = 0; i < listEntity.size(); i++)
		{
			if(playerLocation.distance(listEntity.get(i).getLocation()) < radius)
			{
				double intensiter = ((playerLocation.distance(listEntity.get(i).getLocation()) - radius) * (-1)) / radius;
				double force = intensiter * lvl * 0.1;
				double dstx = playerLocation.getX() - listEntity.get(i).getLocation().getX();
				double dsty = playerLocation.getY() - listEntity.get(i).getLocation().getY();
				double dstz = playerLocation.getZ() - listEntity.get(i).getLocation().getZ();
				
				double angle = Math.atan2(dstz, dstx);
				
				double vecx = force * Math.cos(angle + Math.PI);
				double vecz = force * Math.sin(angle + Math.PI);
				
				double dstOri = Math.sqrt((dstx * dstx) + (dstz * dstz));
				double angleOri = Math.atan2(dstOri, dsty);
				double vecy = force * Math.cos(angleOri + Math.PI) + 0.5 * intensiter;
				
				Vector velocity = listEntity.get(i).getVelocity();
				
				vec.setX(vecx);
				vec.setY(vecy);
				vec.setZ(vecz);
				
				velocity.add(vec);
				
				changeVelocity(player, listEntity.get(i), velocity);
			}
		}
	}

	public static void aspiration(Player player, int radius, double lvl)
	{
		List<Entity> listEntity = player.getWorld().getEntities();
		Location playerLocation = player.getLocation();
		Vector vec = new Vector(0, 0, 0);
		
		for(int i = 0; i < listEntity.size(); i++)
		{
			if(playerLocation.distance(listEntity.get(i).getLocation()) < radius)
			{
				double intensiter = playerLocation.distance(listEntity.get(i).getLocation()) / radius;
				double force = intensiter * lvl * 0.1;
				double dstx = playerLocation.getX() - listEntity.get(i).getLocation().getX();
				double dsty = playerLocation.getY() - listEntity.get(i).getLocation().getY();
				double dstz = playerLocation.getZ() - listEntity.get(i).getLocation().getZ();
				
				double angle = Math.atan2(dstz, dstx);
				
				double vecx = force * Math.cos(angle);
				double vecz = force * Math.sin(angle);
				
				double dstOri = Math.sqrt((dstx * dstx) + (dstz * dstz));
				double angleOri = Math.atan2(dstOri, dsty);
				double vecy = force * Math.cos(angleOri) + 0.50 * intensiter;
				
				Vector velocity = listEntity.get(i).getVelocity();
				
				vec.setX(vecx);
				vec.setY(vecy);
				vec.setZ(vecz);
				
				velocity.add(vec);
				
				changeVelocity(player, listEntity.get(i), velocity);
			}
		}
	}
	
	public static void changeVelocity(Player player, Entity entity, Vector velocity)
	{
		if(entity instanceof Player)
		{
			Player entityPlayer = (Player) entity;
			
			if(entityPlayer != player && entityPlayer.getGameMode() != GameMode.CREATIVE)
			{
				entity.setVelocity(velocity);
			}
		}
		else
		{
			if(entity != player.getVehicle() && !entity.isInsideVehicle())
			{
				if(entity instanceof Fireball)
				{
					Fireball fireball = (Fireball)entity;
					fireball.setDirection(velocity);
				}
				else
					entity.setVelocity(velocity);
			}
		}
	}
}
