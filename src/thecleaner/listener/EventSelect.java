package thecleaner.listener;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import thecleaner.UltraToolMain;
import thecleaner.list.ListPlayerCuboid;
import thecleaner.message.ChatMessage;
import thecleaner.worldedit.Cuboid;

public class EventSelect implements Listener
{
	UltraToolMain m_plugin;
	
	public EventSelect(UltraToolMain plugin)
	{
		m_plugin = plugin;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onClic(PlayerInteractEvent playerInteract)
	{
		Player player = playerInteract.getPlayer();
		
		if(player.isOp() || player.hasPermission("ultratool.zone"))
		{
			ItemStack item = player.getItemInHand();
			
			if(item == null)
				return;
			
			if(item.getType().equals(Material.getMaterial(m_plugin.getPluginConfig().getZoneToolId())))
			{
				ListPlayerCuboid listPlayerCuboid = m_plugin.getListPlayerCuboid();
				Cuboid selection = null;
				
				if(listPlayerCuboid.contains(player))
				{
					selection = listPlayerCuboid.get(player);
				}
				else
				{
					selection = new Cuboid();
				}
				
				if(playerInteract.getClickedBlock() == null)
					return;
				
				Block block = playerInteract.getClickedBlock();
				Location pos = block.getLocation();
				String infoPos = pos.getBlockX() + ", " + pos.getBlockY() + ", " + pos.getBlockZ();
				String blockInfo = block.getType().name() + "(" + block.getType().getId() + ")";
				
				if(playerInteract.getAction().equals(Action.LEFT_CLICK_BLOCK))
				{
					playerInteract.setCancelled(true);
					Location posPre = selection.getPosition1();
					if(!pos.equals(posPre))
					{
						selection.setPosition1(pos);
						player.sendMessage(ChatMessage.info("[" + m_plugin.getName() + "] Select pos 1: " + infoPos + "      b:" + blockInfo));
					}
				}
				else if(playerInteract.getAction().equals(Action.RIGHT_CLICK_BLOCK))
				{
					playerInteract.setCancelled(true);
					Location posPre = selection.getPosition2();
					if(!pos.equals(posPre))
					{
						selection.setPosition2(pos);
						player.sendMessage(ChatMessage.info("[" + m_plugin.getName() + "] Select pos 2: " + infoPos + "      b:" + blockInfo));
					}
				}
					
				
				listPlayerCuboid.put(player, selection);
			}
		}
	}
}

