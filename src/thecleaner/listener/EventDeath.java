package thecleaner.listener;

import java.util.List;

import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.FireworkExplodeEvent;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.inventory.ItemStack;

import thecleaner.UltraToolMain;
import thecleaner.fonction.KillEntity;
import thecleaner.fonction.PluginConfig;
import thecleaner.list.ListEntityProperty.EntityProperty;
import thecleaner.list.ListEntityXp;


public class EventDeath implements Listener
{
	UltraToolMain m_plugin;
	
	public EventDeath(UltraToolMain plugin)
	{
		m_plugin = plugin;
	}
	
	/*
	@EventHandler
	public void onItemCombustByBlock(EntityCombustByBlockEvent event)
	{
		Entity entity = event.getEntity();
		KillEntity.process(m_plugin, entity);
	}
	*/
	
	@EventHandler
	public void onItemLava(EntityDamageEvent event)
	{
		Entity entity = event.getEntity();
		if(entity instanceof Item) {
			KillEntity.process(m_plugin, entity);
		}
	}
	
	@EventHandler
	public void onItemCombust(EntityCombustEvent event)
	{
		Entity entity = event.getEntity();
		if(entity instanceof Item) {
			KillEntity.process(m_plugin, entity);
		}
	}
	
	@EventHandler
	public void onItemDepop(ItemDespawnEvent event)
	{
		Entity entity = event.getEntity();
		KillEntity.process(m_plugin, entity);
	}
	
	@EventHandler
	public void onVehicleDestroy(VehicleDestroyEvent event)
	{
		Entity entity = event.getVehicle();
		KillEntity.process(m_plugin, entity);
	}
	
	@EventHandler
	public void onFireworkExplode(FireworkExplodeEvent event)
	{
		Entity entity = event.getEntity();
		KillEntity.process(m_plugin, entity);
	}
	
	@EventHandler
	public void onEntityChangeBlock(EntityChangeBlockEvent event)
	{
		Entity entity = event.getEntity();
	    if (entity instanceof FallingBlock)
	    {
	    	KillEntity.process(m_plugin, entity);
	    }
	}
	
	@EventHandler
	public void onProjectileHit(ProjectileHitEvent event) {
		KillEntity.process(m_plugin, event.getEntity()); 
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPickupItem(org.bukkit.event.player.PlayerPickupItemEvent playerPickupItem)
	{
		Entity entity = playerPickupItem.getItem();
		EntityProperty entityProperty = m_plugin.getListEntityProperty().getProperty(entity);
		
		if(entityProperty != null) {
			if(entityProperty.isInvincible())
				playerPickupItem.setCancelled(true);
			else
				KillEntity.process(m_plugin, entity);
		}
	}
	
	@EventHandler
	public void onEntityExplode(EntityExplodeEvent entityExplode) 
	{
		KillEntity.process(m_plugin, entityExplode.getEntity()); 
	}
	
	@EventHandler
	public void onDeath(EntityDeathEvent entityDeath)
	{
		Entity entity = entityDeath.getEntity();
		EntityProperty entityProperty = m_plugin.getListEntityProperty().getProperty(entity);
		ListEntityXp listEntityXp = m_plugin.getListEntityXp();
		List<ItemStack> drops = entityDeath.getDrops();
		PluginConfig pluginConfig = m_plugin.getPluginConfig();
		
		if(!pluginConfig.getLootDefault())
		{
			if(entityProperty != null)
				drops.clear();
		}
		
		if(entityProperty != null)
		{
			String name = entityProperty.getInvToDrop();
			if(name != null) {
				ItemStack[] items = m_plugin.getListInv().get(name, null);
				if(items != null) {
					for(int i = 0; i < items.length; i++)
					{
						drops.add(items[i]);
					}
				}
			}
		}
		
		if(!pluginConfig.getLootXp())
		{
			if(entityProperty != null)
				entityDeath.setDroppedExp(0);
			
		}
		
		if(entity instanceof LivingEntity)
		{
			Integer lootXp = listEntityXp.getXp((LivingEntity) entity);
			if(lootXp != null)
			{
				entityDeath.setDroppedExp(lootXp);
			}
		}
		
		
		
		KillEntity.process(m_plugin, entity);
	}
	
	
}