package thecleaner.sender;

import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.block.Block;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationAbandonedEvent;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class PluginSender implements CommandSender, ConsoleCommandSender, BlockCommandSender
{
	private static JavaPlugin m_plugin;
	private static ConsoleCommandSender m_console;
	private CommandSender m_sender;
	private Location m_location;
	private String m_messageStack;
	
	public PluginSender()
	{
		super();
		
		m_sender = null;
		m_location = null;
		m_messageStack = "";
	}
	
	public static void setPlugin(JavaPlugin plugin)
	{
		m_plugin = plugin;
		m_console = m_plugin.getServer().getConsoleSender();
	}
	
	public void setSender(CommandSender sender)
	{
		m_sender = sender;
	}
	
	public PermissionAttachment addAttachment(Plugin arg0) 
	{
		return m_console.addAttachment(arg0);
	}

	public PermissionAttachment addAttachment(Plugin arg0, int arg1) 
	{
		return m_console.addAttachment(arg0, arg1);
	}

	public PermissionAttachment addAttachment(Plugin arg0, String arg1, boolean arg2) 
	{
		return m_console.addAttachment(arg0, arg1, arg2);
	}

	public PermissionAttachment addAttachment(Plugin arg0, String arg1,
			boolean arg2, int arg3) 
	{
		return m_console.addAttachment(arg0, arg1, arg2, arg3);
	}

	public Set<PermissionAttachmentInfo> getEffectivePermissions() 
	{
		return m_console.getEffectivePermissions();
	}

	public boolean hasPermission(String arg0) 
	{
		return m_console.hasPermission(arg0);
	}

	public boolean hasPermission(Permission arg0) 
	{
		return m_console.hasPermission(arg0);
	}

	public boolean isPermissionSet(String arg0) 
	{
		return m_console.isPermissionSet(arg0);
	}

	public boolean isPermissionSet(Permission arg0) 
	{
		return m_console.isPermissionSet(arg0);
	}

	public void recalculatePermissions() 
	{
		m_console.recalculatePermissions();
	}

	public void removeAttachment(PermissionAttachment arg0) 
	{
		m_console.removeAttachment(arg0);
	}

	public boolean isOp() 
	{
		return m_console.isOp();
	}

	public void setOp(boolean isOp) 
	{
		m_console.setOp(isOp);
	}

	public String getName() 
	{
		return "[" + m_plugin.getName() + "]";
	}

	public Server getServer() 
	{
		return m_console.getServer();
	}

	public void sendMessage(String message) 
	{
		if(m_sender instanceof Player)
		{
			((Player)m_sender).sendMessage(message);
			return;
		}
		
		if(!m_messageStack.equals(""))
			m_messageStack += "\n";
		
		m_messageStack += message;
	}

	public void sendMessage(String[] word) 
	{
		String message = "";
		
		for(int i = 0; i < word.length - 1; i++)
		{
			message += word[i] + " ";
		}
		
		message += word[word.length - 1];
		
		this.sendMessage(message);
	}
	
	public String getMessage()
	{
		return m_messageStack;
	}
	
	public void clearMessage()
	{
		m_messageStack = "";
	}

	public void abandonConversation(Conversation arg0) 
	{
		m_console.abandonConversation(arg0);
	}

	public void abandonConversation(Conversation arg0, ConversationAbandonedEvent arg1) 
	{
		m_console.abandonConversation(arg0, arg1);
	}

	public void acceptConversationInput(String arg0) 
	{
		m_console.acceptConversationInput(arg0);
	}

	public boolean beginConversation(Conversation arg0) 
	{
		return m_console.beginConversation(arg0);
	}

	public boolean isConversing() 
	{
		return m_console.isConversing();
	}

	public void sendRawMessage(String arg0) 
	{
		m_console.sendRawMessage(arg0);
	}
	
	public void setLocation(Location location)
	{
		m_location = location;
	}

	public Block getBlock() 
	{
		if(m_location != null)
		{
			return m_location.getBlock();
		}
		
		if(m_sender != null)
		{
			if(m_sender instanceof BlockCommandSender)
			{
				return ((BlockCommandSender) m_sender).getBlock();
			}
			else if(m_sender instanceof Player)
			{
				return ((Player) m_sender).getLocation().getBlock();
			}
		}
		
		return null;
	}

	@Override
	public Spigot spigot() {
		return null;
	}

}
