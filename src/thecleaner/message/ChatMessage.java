package thecleaner.message;

import org.bukkit.ChatColor;

public class ChatMessage 
{
	private static Language m_language;
	
	public static void setLanguage(Language language)
	{
		m_language = language;
	}
	
	public static String info(String information)
	{
		return ChatColor.GOLD + information;
	}
	
	public static String show(String text)
	{
		return ChatColor.BLUE + text;
	}
	
	public static String error(String error)
	{
		return ChatColor.RED + error;
	}
	
	public static String helpCommand(String command, String text)
	{
		return show(command) + ChatColor.GRAY + " � " + info(text);
	}
	
	public static String title(String title)
	{
		return ChatColor.DARK_PURPLE + "--- " + title + " ---";
	}
	
	public static String errorDefault()
	{
		return error(m_language.get("command.error.default"));
	}
	
	public static String errorMustBePlayer()
	{
		return error(m_language.get("command.error.must_be_player"));
	}
	
	public static String errorMustBeOp()
	{
		return error(m_language.get("command.error.must_be_op"));
	}
	
	public static String errorNoConsole()
	{
		return error(m_language.get("command.error.no_console"));
	}
	
	public static String errorCheckCommand(String command)
	{
		return error(m_language.getCmd("command.error.check_your_command", command));
	}
	
	public static String errorCommand(String command)
	{
		return error(m_language.getCmd("command.error.no_sub_command", command));
	}
	
	public static String errorSyntax(String command)
	{
		return error(m_language.getCmd("command.error.syntax", command));
	}
	
	public static String errorNbArg(String command)
	{
		return error(m_language.getCmd("command.error.nb_arg_invalid", command));
	}
	
	public static String errorOutOfRange(Integer id)
	{
		return error(m_language.getElement("command.error.int_out_of_range", id));
	}
	
	public static String errorNameUndef(String name)
	{
		return error(m_language.getElement("command.error.unknow_string_value", name));
	}
	
	public static String errorPointUndef(String pointName)
	{
		return error(m_language.getElement("command.error.unknow_point_value", pointName));
	}
	
	public static String errorKitUndef(String kitName)
	{
		return error(m_language.getElement("command.error.unknow_kit_value", kitName));
	}
	
	public static String errorInventoryUndef(String inventoryName)
	{
		return error(m_language.getElement("command.error.unknow_inventory_value", inventoryName));
	}
	
	public static String errorTaskUndef(String taskName)
	{
		return error(m_language.getElement("command.error.unknow_task_value", taskName));
	}
	
	public static String errorZoneUndef(String zoneName)
	{
		return error(m_language.getElement("command.error.unknow_zone_value", zoneName));
	}
	
	public static String errorPlayerNotFound(String playername)
	{
		return error(m_language.getElement("command.error.player_not_found", playername));
	}
	
	public static String errorNotAlphaNum(String value)
	{
		return error(m_language.getElement("command.error.not_alpha_num", value));
	}
	
	public static String errorNotInteger(String value)
	{
		return error(m_language.getElement("command.error.not_integer", value));
	}
	
	public static String errorNotDouble(String value)
	{
		return error(m_language.getElement("command.error.not_double", value));
	}
}
