package thecleaner.message;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.bukkit.configuration.file.YamlConfiguration;

import thecleaner.UltraToolMain;

public class Language 
{
	UltraToolMain m_plugin;
	YamlConfiguration m_messageFile;
	
	public Language(UltraToolMain plugin)
	{
		m_plugin = plugin;
		m_messageFile = null;
		ChatMessage.setLanguage(this);
		
		String language = m_plugin.getPluginConfig().getLanguage();
		String filename = null;
		File fichier_config;
		
		InputStream stream = null;
		
		if(language.length() == 2)
		{
			language = language.toUpperCase();
		}
		
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 2; j++)
			{
				switch(i)
				{
					case 0:
						filename = "lang/" + language + ".yml";
						break;
					case 1:
						filename = "lang/" 
							+ System.getProperty("user.language").substring(0, 2).toUpperCase() 
							+ ".yml";
						break;
					case 2:
						filename = "lang/EN.yml";
						break;
				}
				
				switch(j)
				{
					case 0:
						fichier_config = new File(plugin.getDataFolder() + "/" + filename);
						if(fichier_config.exists())
						{
							m_messageFile = YamlConfiguration.loadConfiguration(fichier_config);
							return;
						}
						break;
					case 1:
						stream = m_plugin.getResource(filename);
						if(stream != null)
						{
							m_plugin.saveResource(filename, false);
							m_messageFile = YamlConfiguration.loadConfiguration(
									new InputStreamReader(m_plugin.getResource(filename)));
							return;
						}
						break;
				}
			}
		}
	}
	
	public String get(String key)
	{
		if(m_messageFile != null)
		{
			String message = m_messageFile.getString(key);
			
			if(message == null)
				return "#NoMessage";
				
			return message;
		}
		
		return "#FileNotFound";
	}
	
	public String getCmd(String key, String cmd)
	{
		if(m_messageFile != null)
		{
			String message = m_messageFile.getString(key);
			
			if(message == null)
				return "#NoMessage";
				
			return message.replace("<command>", cmd);
		}
		
		return "#FileNotFound";
	}
	
	public String getElement(String key, Integer element)
	{
		return this.getElement(key, element.toString());
	}
	
	public String getElement(String key, String element)
	{
		if(m_messageFile != null)
		{
			String message = m_messageFile.getString(key);
			
			if(message == null)
				return "#NoMessage";
				
			return message.replace("<element>", element);
		}
		
		return "#FileNotFound";
	}
}
