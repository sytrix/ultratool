package thecleaner.message;

import org.bukkit.command.CommandSender;


public class Help 
{
	static public void command(Language language, CommandSender sender)
	{
		sender.sendMessage(ChatMessage.title(language.get("command.ultratool.title")));
		sender.sendMessage(ChatMessage.helpCommand("/ubutcher", language.get("command.ultratool.butcher")));
		sender.sendMessage(ChatMessage.helpCommand("/ucode", language.get("command.ultratool.code")));
		sender.sendMessage(ChatMessage.helpCommand("/ucompass", language.get("command.ultratool.compass")));
		sender.sendMessage(ChatMessage.helpCommand("/ueffect", language.get("command.ultratool.effect")));
		sender.sendMessage(ChatMessage.helpCommand("/uexe", language.get("command.ultratool.exe")));
		sender.sendMessage(ChatMessage.helpCommand("/uinv", language.get("command.ultratool.inv")));
		sender.sendMessage(ChatMessage.helpCommand("/uitem", language.get("command.ultratool.item")));
		sender.sendMessage(ChatMessage.helpCommand("/ukit", language.get("command.ultratool.kit")));
		sender.sendMessage(ChatMessage.helpCommand("/ulist", language.get("command.ultratool.list")));
		sender.sendMessage(ChatMessage.helpCommand("/upoint", language.get("command.ultratool.point")));
		sender.sendMessage(ChatMessage.helpCommand("/usend", language.get("command.ultratool.send")));
		sender.sendMessage(ChatMessage.helpCommand("/usound", language.get("command.ultratool.sound")));
		sender.sendMessage(ChatMessage.helpCommand("/uspawn", language.get("command.ultratool.spawn")));
		sender.sendMessage(ChatMessage.helpCommand("/utask", language.get("command.ultratool.task")));
		sender.sendMessage(ChatMessage.helpCommand("/utool", language.get("command.ultratool.tool")));
		sender.sendMessage(ChatMessage.helpCommand("/ultratool", language.get("command.ultratool.ultratool")));
		sender.sendMessage(ChatMessage.helpCommand("/uzone", language.get("command.ultratool.zone")));
		
	}
}
