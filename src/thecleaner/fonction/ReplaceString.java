package thecleaner.fonction;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;

import thecleaner.UltraToolMain;
import thecleaner.mob.FindMob;
import thecleaner.mob.GroupMobData;

public class ReplaceString 
{
	public static List<String[]> replace(UltraToolMain plugin, Location location, String line, CommandSender sender)
	{
		List<String[]> listResSplit = new ArrayList<String[]>();
		List<String> listRes = new ArrayList<String>();
		List<List<Entity>> listListEntity = new ArrayList<List<Entity>>();
		boolean find = false;
		StringBuilder code = new StringBuilder();
		String res = "";
		
		for(int i = 0; i < line.length(); i++)
		{
			if(line.charAt(i) == '(' && i > 0 && line.charAt(i - 1) == '@')
			{
				find = true;
			}
			else if(line.charAt(i) == ')')
			{
				if(find) {
					res = "{" + listListEntity.size() + "}";
					
					GroupMobData groupMobData = new GroupMobData();
					
					groupMobData.setSpawnLocation(location);
					Iterator<String> errorMessage = groupMobData.processing(plugin, sender, code.toString().split(" ")).iterator();
					
					while(errorMessage.hasNext()) {
						String message = errorMessage.next();
						sender.sendMessage(ChatColor.RED + "Eval Error : " + message);
					}
					
					List<String> listError = new ArrayList<String>();
					List<Entity> listEntity = FindMob.find(plugin, groupMobData, listError);
					listListEntity.add(listEntity);
					
					errorMessage = listError.iterator();
					while(errorMessage.hasNext()) {
						String message = errorMessage.next();
						sender.sendMessage(ChatColor.RED + "Spawn Error : " + message);
					}
					
					find = false;
					code = new StringBuilder();
				}
			}
			else if(find)
			{
				code.appendCodePoint(line.codePointAt(i));
			}
			else
			{
				if(line.charAt(i) != '@')
					res += line.charAt(i);
			}
		}
		
		int produit = 1;
		for(int i = 0; i < listListEntity.size(); i++)
			produit *= listListEntity.get(i).size();
		
		for(int i = 0; i < produit; i++) {
			listRes.add(res);
		}
		sender.sendMessage(listRes.toString());
		sender.sendMessage(listListEntity.toString());
		
		extractSubString(listRes, listListEntity, 0);
		
		for(int i = 0; i < listRes.size(); i++)
		{
			//plugin.getServer().broadcastMessage(listRes.get(i));
			listResSplit.add(listRes.get(i).split(" "));
		}
		
		return listResSplit;
	}
	
	public static void extractSubString(
			List<String> listRes, List<List<Entity>> listListEntity, int indexList)
	{
		if(indexList < listListEntity.size())
		{
			for(int i = 0; i < listListEntity.get(indexList).size(); i++)
			{
				String res = listRes.get(i);
				res = res.replace("{" + indexList + "}", listListEntity.get(indexList).get(i).getUniqueId().toString());
				listRes.set(i, res);
				
				if(indexList + 1 < listListEntity.size())
					extractSubString(listRes, listListEntity, indexList + 1);
					
			}
		}
	}
	
	public static List<String[]> replace(UltraToolMain plugin, Location location, String arg[], CommandSender sender)
	{
		List<String[]> res = null;
		String line = "";
		
		for(int i = 0; i < arg.length; i++)
			line += arg[i] + " ";
			
		res = ReplaceString.replace(plugin, location, line, sender);
		
		return res;
	}
}
