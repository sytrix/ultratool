package thecleaner.fonction;

public class StringGen 
{
	private static String getBegin()
	{
		int id = (int) (Math.random() * 23);
		
		switch(id)
		{
		case 0:
			return "tr-";
		case 1:
			return "gr-";
		case 2:
			return "kr-";
		case 3:
			return "fr-";
		case 4:
			return "ch-";
		case 5:
			return "scr-";
		case 6:
			return "z-";
		case 7:
			return "sr-";
		case 8:
			return "y-";
		case 9:
			return "st-";
		case 10:
			return "n-";
		case 11:
			return "sh-";
		case 12:
			return "j-";
		case 13:
			return "c-";
		case 14:
			return "v-";
		case 15:
			return "s-";
		case 16:
			return "b-";
		case 17:
			return "k-";
		case 18:
			return "l-";
		case 19:
			return "dr-";
		case 20:
			return "h-";
		case 21:
			return "ph-";
		case 22:
			return "p-";
		}
		
		return "";
	}
	
	private static String getMiddle()
	{
		int id = (int) (Math.random() * 29);
		
		switch(id)
		{
		case 0:
			return "n-";
		case 1:
			return "br-";
		case 2:
			return "c-";
		case 3:
			return "tr-";
		case 4:
			return "f-";
		case 5:
			return "y-";
		case 6:
			return "ph-";
		case 7:
			return "x-";
		case 8:
			return "r-";
		case 9:
			return "cn-";
		case 10:
			return "y-";
		case 11:
			return "r-";
		case 12:
			return "l-";
		case 13:
			return "j-";
		case 14:
			return "z-";
		case 15:
			return "p-";
		case 16:
			return "t-";
		case 17:
			return "gn-";
		case 18:
			return "ct-";
		case 19:
			return "ch-";
		case 20:
			return "k-";
		case 21:
			return "m-";
		case 22:
			return "d-";
		case 23:
			return "v-";
		case 24:
			return "dr-";
		case 25:
			return "pt-";
		case 26:
			return "ss-";
		case 27:
			return "rr-";
		case 28:
			return "ts-";
		}
		
		return "";
	}
	
	private static String getEnd()
	{
		int id = (int) (Math.random() * 19);
		
		switch(id)
		{
		case 0:
			return "ck";
		case 1:
			return "z";
		case 2:
			return "r";
		case 3:
			return "t";
		case 4:
			return "y";
		case 5:
			return "p";
		case 6:
			return "s";
		case 7:
			return "d";
		case 8:
			return "f";
		case 9:
			return "g";
		case 10:
			return "h";
		case 11:
			return "k";
		case 12:
			return "l";
		case 13:
			return "m";
		case 14:
			return "n";
		case 15:
			return "x";
		case 16:
			return "c";
		case 17:
			return "v";
		case 18:
			return "b";
		}
		
		return "";
	}
	
	private static String getAleatVowel()
	{
		int nbAleat = (int) (Math.random() * 100);
		
		if(nbAleat < 92)
		{
			nbAleat %= 5;
			
			switch(nbAleat)
			{
			case 0:
				return "a";
			case 1:
				return "e";
			case 2:
				return "i";
			case 3:
				return "o";
			case 4:
				return "u";
			}
		}
		else
		{
			if(nbAleat < 94)
				return "ou";
			else if(nbAleat < 96)
				return "oi";
			else if(nbAleat == 96)
				return "on";
			else if(nbAleat == 97)
				return "y";
			else if(nbAleat == 98)
				return "io";
			else if(nbAleat == 99)
				return "ui";
			
		}
			
		return null;
	}
	
	public static String name()
	{
		String name = "";
		
		
		if(Math.random() * 2 < 1) 
		{
			name += getAleatVowel();
		}
		
		name += getBegin();
		
		if(Math.random() * 4 < 2) 
		{
			name += getMiddle();
			if(Math.random() * 10 < 1)
			{
				name += getMiddle();
			}
		}
		
		name += getEnd();
		
		if(name.length() < 4 || (Math.random() * 2 < 1))
		{
			name += getAleatVowel();
		}
		
		
		while(name.contains("-"))
		{
			name = name.replaceFirst("-", getAleatVowel());
		}
		
		
		return name;
	}
}
