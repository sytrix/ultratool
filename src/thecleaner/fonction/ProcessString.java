package thecleaner.fonction;

import org.bukkit.Location;

public class ProcessString 
{
	public static String niceString(String string)
	{
		return string.replace('_', ' ').toLowerCase();
	}
	
	public static Integer toTick(String time)
	{
		Integer number = 0;
		Integer delay = 0;
		
		
		for(int j = 0; j < time.length(); j++)
		{
			char c = time.charAt(j);
			
			if(c >= '0' && c <= '9')
			{
				number *= 10;
				number += c - '0';
			}
			else
			{
				switch(c)
				{
					case 't':
						delay += number;
						break;
					case 's':
						delay += number * 20;
						break;
					case 'm':
						delay += number * 20 * 60;
						break;
					case 'h':
						delay += number * 20 * 3600;
						break;
					case 'd':
						delay += number * 20 * 3600 * 24;
						break;
					default:
						return null;
				}
				
				number = 0;
			}
		}
		
		return delay;
	}
	
	public static String getStringLocation(Location location)
	{
		String info = "";
		info += "x:" + location.getBlockX();
		info += ", y:" + location.getBlockY();
		info += ", z:" + location.getBlockZ();
		
		return info;
	}
	
	public static Integer parseInt(String nb)
	{
		try {return Integer.parseInt(nb, 10);}
		catch (NumberFormatException nfe) {return null;}
	}
	
	public static Byte parseByte(String nb)
	{
		try {return Byte.parseByte(nb);}
		catch (NumberFormatException nfe) {return null;}
	}
	
	public static Integer parseIntBase16(String nb)
	{
		try {return Integer.parseInt(nb, 16);}
		catch (NumberFormatException nfe) {return null;}
	}
	
	public static Double parseDouble(String nb)
	{
		try {return Double.parseDouble(nb);}
		catch (NumberFormatException nfe) {return null;}
	}
	
	public static Float parseFloat(String arg)
	{
		try {return Float.parseFloat(arg);}
		catch (NumberFormatException nfe) {return null;}
	}
	
	public static boolean isCorrectString(String text)
	{
		for(int i = 0; i < text.length(); i++)
		{
			if ((text.charAt(i) < '0' || text.charAt(i) > '9') && 
				(text.charAt(i) < 'A' || text.charAt(i) > 'Z') &&
				(text.charAt(i) < 'a' || text.charAt(i) > 'z') && 
				text.charAt(i) != '_')
					return false;
		}
		
	    return true;
	}
}
