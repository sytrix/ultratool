package thecleaner.fonction;

public class CheckMethod {
	
	public static boolean exist(String className, String methodName)
	{
		try
		{
            try 
            {
            	// TO Test
				
				try {
					Class.forName(className).getMethod(methodName, (Class<?>[]) null);
				} catch (NoSuchMethodException | SecurityException e) {
				  	return false;
				}
			} 
            catch (ClassNotFoundException e) 
			{
				return false;
			}
	    }
	    catch (NoClassDefFoundError ex) 
	    {
	        return false ;
	    }
		
		return true;
	}
	
	public static boolean existDoubleHand()
	{
		return exist("org.bukkit.inventory.EntityEquipment", "getItemInMainHand");
	}
	
	
	public static boolean existMultiplePassenger()
	{
		return exist("org.bukkit.entity.Entity;", "addPassenger");
	}
}
