package thecleaner.fonction;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

import thecleaner.UltraToolMain;
import thecleaner.list.ListEntityProperty;
import thecleaner.list.ListEntityXp;

public class KillEntity 
{
	public static void process(UltraToolMain plugin, Entity entity)
	{	
		ListEntityProperty listEntityProperty = plugin.getListEntityProperty();
		ListEntityXp listEntityXp = plugin.getListEntityXp();
		
		listEntityProperty.remove(entity);
		
		if(entity instanceof LivingEntity)
		{
			listEntityXp.remove((LivingEntity) entity);
		}
	}
}
