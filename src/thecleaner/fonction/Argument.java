package thecleaner.fonction;

public class Argument 
{
	public static String getKey(String argument)
	{
		if(argument.contains(":"))
			return argument.substring(0, argument.indexOf(":"));
		
		return null;
	}
	
	public static String getValue(String argument)
	{
		if(argument.contains(":"))
			return argument.substring(argument.indexOf(":") + 1);

		return null;
	}
	
	public static Integer getValueInt(String argument)
	{
		if(argument.contains(":"))
		{
			try {return Integer.parseInt(argument.substring(argument.indexOf(":") + 1), 10);}
			catch (NumberFormatException nfe) {return null;}
		}
		
		return null;
	}
	
	public static Byte getValueByte(String argument)
	{
		if(argument.contains(":"))
		{ 
			try {return Byte.parseByte(argument.substring(argument.indexOf(":") + 1));}
			catch (NumberFormatException nfe) {return null;}
		}
		
		return null;
	}
	
	public static Float getValueFloat(String argument)
	{
		if(argument.contains(":"))
		{
			try {return Float.parseFloat(argument.substring(argument.indexOf(":") + 1));}
			catch (NumberFormatException nfe) {return null;}
		}
		
		return null;
	}
	
	public static Double getValueDouble(String argument)
	{
		if(argument.contains(":"))
		{
			try {return Double.parseDouble(argument.substring(argument.indexOf(":") + 1));}
			catch (NumberFormatException nfe) {return null;}
		}
		
		return null;
	}
}
