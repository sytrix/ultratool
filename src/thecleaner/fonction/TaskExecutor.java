package thecleaner.fonction;

import java.util.Iterator;

import org.bukkit.scheduler.BukkitRunnable;

import thecleaner.UltraToolMain;
import thecleaner.list.ListTask;
import thecleaner.sender.PluginSender;

public class TaskExecutor extends BukkitRunnable
{
	private UltraToolMain m_plugin;
	private ListTask m_listTask;
	private Integer m_countTick;
	
	public TaskExecutor(UltraToolMain plugin)
	{
		m_plugin = plugin;
		m_listTask = m_plugin.getListTask();
		m_countTick = 0;
		this.runTaskTimer(m_plugin, 0L, 1L);
	}
	
	public void run() 
	{
		Iterator<String> iterator = m_listTask.iterator();
		
		while(iterator.hasNext())
		{
			String key = iterator.next();
			Integer tick = m_listTask.getTick(key);
			
			if(tick != null && tick > 0 && m_countTick % tick == 0)
			{
				PluginSender pluginSender = new PluginSender();
				pluginSender.setLocation(m_listTask.getLocation(key));
				boolean error = false;
				
				try
				{
					m_plugin.getServer().dispatchCommand(
							pluginSender, m_listTask.getCommand(key));
				}
				catch(Exception e)
				{
					m_listTask.setStackTrace(key, e.getMessage());
					error = true;
				}
				
				if(error)
				{
					try
					{
						m_plugin.getServer().dispatchCommand(
								m_plugin.getServer().getConsoleSender(), 
								m_listTask.getCommand(key));
					}
					catch(Exception e)
					{
						m_listTask.setStackTrace(key, e.getMessage());
					}
				}
				
				m_listTask.setLastMessage(key, pluginSender.getMessage());
			}
		}
		
		m_countTick++;
    }
	
}
