package thecleaner.fonction;

import java.util.Map.Entry;

public class SimpleEntry<T1, T2> implements Entry<String, Object> 
{
	private String m_key;
	private Object m_value;
	
	public SimpleEntry(String key)
	{
		m_key = key;
	}
	
	@Override
	public String getKey()
	{
		return m_key;
	}

	@Override
	public Object getValue() 
	{
		return m_value;
	}

	@Override
	public Object setValue(Object value) 
	{
		m_value = value;
		return m_value;
	}

}
