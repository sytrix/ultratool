package thecleaner.fonction;

public class CheckClass 
{
	public static boolean exist(String className)
	{
		try
		{
            try 
            {
				Class.forName(className);
			} 
            catch (ClassNotFoundException e) 
			{
				return false;
			}
	    }
	    catch (NoClassDefFoundError ex) 
	    {
	        return false ;
	    }
		
		return true;
	}
	
	public static boolean existWitherSkeleton()
	{
		 return exist("org.bukkit.entity.WitherSkeleton");
	}
	
	public static boolean existArmorStand()
	{
		 return exist("org.bukkit.entity.ArmorStand");
	}
	
	public static boolean existLlama()
	{
		 return exist("org.bukkit.entity.Llama");
	}
}
