package thecleaner.fonction;

import thecleaner.UltraToolMain;
import thecleaner.file.FileConfig;

public class PluginConfig 
{
	private String m_language;
	private Boolean m_lootDefault;
	private Boolean m_lootKit;
	private Boolean m_lootXp;
	private Integer m_updateTime;
	private Integer m_spawnQuantityMax;
	private Boolean m_spawnPlayerTeleport;
	private Boolean m_spawnEntityTeleport;
	private Integer m_zoneToolId;
	private Integer m_toolEffectRadiusMax;
	
	public PluginConfig(UltraToolMain plugin)
	{
		m_language = null;
		m_lootDefault = false;
		m_lootKit = false;
		m_lootXp = false;
		m_updateTime = 300;
		m_spawnQuantityMax = 200;
		m_spawnPlayerTeleport = false;
		m_spawnEntityTeleport = false;
		m_zoneToolId = 258;
		m_toolEffectRadiusMax = 100;
		
		FileConfig.load(plugin, this);
	}
	
	public void setLanguage(String language)
	{
		m_language = language;
	}
	
	public void setLootDefault(Boolean lootDefault)
	{
		m_lootDefault = lootDefault;
	}
	
	public void setLootKit(Boolean lootKit)
	{
		m_lootKit = lootKit;
	}
	
	public void setLootXp(Boolean lootXp)
	{
		m_lootXp = lootXp;
	}
	
	public void setUpdateTime(Integer updateTime)
	{
		m_updateTime = updateTime;
	}
	
	public void setSpawnQuantityMax(Integer spawnQuantityMax)
	{
		m_spawnQuantityMax = spawnQuantityMax;
	}
	
	public void setSpawnPlayerTeleport(Boolean spawnPlayerTeleport)
	{
		m_spawnPlayerTeleport = spawnPlayerTeleport;
	}
	
	public void setSpawnEntityTeleport(Boolean spawnEntityTeleport)
	{
		m_spawnEntityTeleport = spawnEntityTeleport;
	}
	
	public void setZoneToolId(Integer zoneToolId)
	{
		m_zoneToolId = zoneToolId;
	}
	
	public void setToolEffectRadiusMax(Integer toolEffectRadiusMax)
	{
		m_toolEffectRadiusMax = toolEffectRadiusMax;
	}
	
	public String getLanguage()
	{
		return m_language;
	}
	
	public Boolean getLootDefault()
	{
		return m_lootDefault;
	}
	
	public Boolean getLootKit()
	{
		return m_lootKit;
	}
	
	public Boolean getLootXp()
	{
		return m_lootXp;
	}
	
	public Integer getUpdateTime()
	{
		return m_updateTime;
	}
	
	public Integer getSpawnQuantityMax()
	{
		return m_spawnQuantityMax;
	}
	
	public Boolean getSpawnPlayerTeleport()
	{
		return m_spawnPlayerTeleport;
	}
	
	public Boolean getSpawnEntityTeleport() 
	{
		return m_spawnEntityTeleport;
	}
	
	public Integer getZoneToolId()
	{
		return m_zoneToolId;
	}
	
	public Integer getToolEffectRadiusMax()
	{
		return m_toolEffectRadiusMax;
	}
}
