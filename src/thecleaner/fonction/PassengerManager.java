package thecleaner.fonction;

import org.bukkit.entity.Entity;

public class PassengerManager {
	
	@SuppressWarnings("deprecation")
	static public void setEntityPassenger(Entity vehicle, Entity driver) {
		if(CheckMethod.existMultiplePassenger())
		{
			vehicle.addPassenger(driver);
		}
		else
		{
			vehicle.setPassenger(driver);
		}
	}
	
	@SuppressWarnings("deprecation")
	static public void clearEntityPassenger(Entity vehicle) {
		if(CheckMethod.existMultiplePassenger())
		{
			vehicle.getPassengers().clear();
		}
		else
		{
			vehicle.setPassenger(null);
		}
	}
	
	@SuppressWarnings("deprecation")
	static public boolean haveEntityPassenger(Entity vehicle) {
		if(CheckMethod.existMultiplePassenger())
		{
			return (vehicle.getPassengers().size() > 0);
		}
		else
		{
			return (vehicle.getPassenger() != null);
		}
	}
	
}
