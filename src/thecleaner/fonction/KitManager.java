package thecleaner.fonction;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class KitManager 
{
	public static ItemStack[] getKit(LivingEntity livingEntity)
	{
		EntityEquipment entityEquipment = livingEntity.getEquipment();
		ItemStack[] equipment = new ItemStack[6];
		
		if(entityEquipment != null)
		{
			equipment[0] = entityEquipment.getHelmet();
			equipment[1] = entityEquipment.getChestplate();
			equipment[2] = entityEquipment.getLeggings();
			equipment[3] = entityEquipment.getBoots();
			
			if(CheckMethod.existDoubleHand()) {
				equipment[4] = entityEquipment.getItemInMainHand();
				equipment[5] = entityEquipment.getItemInOffHand();
			} else {
				equipment[4] = KitManager.getEntityItemInHand(entityEquipment);
				equipment[5] = null;
			}
		}
		else if(livingEntity instanceof Player)
		{
			PlayerInventory playerInv = ((Player)livingEntity).getInventory();
			
			if(playerInv == null)
				return null;
				
			equipment[0] = playerInv.getHelmet();
			equipment[1] = playerInv.getChestplate();
			equipment[2] = playerInv.getLeggings();
			equipment[3] = playerInv.getBoots();
			
			if(CheckMethod.existDoubleHand()) {
				equipment[4] = playerInv.getItemInMainHand();
				equipment[5] = playerInv.getItemInOffHand();
			} else {
				equipment[4] = KitManager.getPlayerItemInHand(playerInv);
				equipment[5] = null;
			}
		}
		else if(CheckClass.existArmorStand() && livingEntity instanceof ArmorStand)
		{
			ArmorStand armorStand = (ArmorStand)livingEntity;
			
			equipment[0] = armorStand.getHelmet();
			equipment[1] = armorStand.getChestplate();
			equipment[2] = armorStand.getLeggings();
			equipment[3] = armorStand.getBoots();
			equipment[4] = armorStand.getItemInHand();
			equipment[5] = null;
		}
		else
		{
			return null;
		}
		
		return equipment;
	}
	
	public static ItemStack getItemHand(LivingEntity livingEntity)
	{
		EntityEquipment entityEquipment = livingEntity.getEquipment();
		
		if(entityEquipment != null)
		{
			if(CheckMethod.existDoubleHand()) {
				return entityEquipment.getItemInMainHand();
			} else {
				return KitManager.getEntityItemInHand(entityEquipment);
			}
		}
		else if(livingEntity instanceof Player)
		{
			PlayerInventory playerInv = ((Player)livingEntity).getInventory();
			
			if(playerInv == null)
				return null;
			
			if(CheckMethod.existDoubleHand()) {
				return playerInv.getItemInMainHand();
			} else {
				return KitManager.getPlayerItemInHand(playerInv);
			}
		}
		else if(CheckClass.existArmorStand() && livingEntity instanceof ArmorStand)
		{
			ArmorStand armorStand = (ArmorStand)livingEntity;
			
			return armorStand.getItemInHand();
		}
		
		return null;
	}
	
	public static ItemStack[] getItemHands(LivingEntity livingEntity)
	{
		EntityEquipment entityEquipment = livingEntity.getEquipment();
		ItemStack[] items = new ItemStack[2];
		
		if(entityEquipment != null)
		{
			if(CheckMethod.existDoubleHand()) {
				items[0] = entityEquipment.getItemInMainHand();
				items[1] = entityEquipment.getItemInOffHand();
			} else {
				items[0] = KitManager.getEntityItemInHand(entityEquipment);
				items[1] = null;
			}
		}
		else if(livingEntity instanceof Player)
		{
			PlayerInventory playerInv = ((Player)livingEntity).getInventory();
			
			if(playerInv == null)
				return null;
			
			if(CheckMethod.existDoubleHand()) {
				items[0] = playerInv.getItemInMainHand();
				items[1] = playerInv.getItemInOffHand();
			} else {
				items[0] = KitManager.getPlayerItemInHand(playerInv);
				items[1] = null;
			}
		}
		else if(CheckClass.existArmorStand() && livingEntity instanceof ArmorStand)
		{
			ArmorStand armorStand = (ArmorStand)livingEntity;
			
			items[0] = armorStand.getItemInHand();
			items[1] = null;
		}
		
		return items;
	}
	
	public static ItemStack getHelmet(LivingEntity livingEntity)
	{
		EntityEquipment entityEquipment = livingEntity.getEquipment();
		
		if(entityEquipment != null)
		{
			return entityEquipment.getHelmet();
		}
		else if(livingEntity instanceof Player)
		{
			PlayerInventory playerInv = ((Player)livingEntity).getInventory();
			
			if(playerInv == null)
				return null;
			
			return playerInv.getHelmet();
		}
		else if(CheckClass.existArmorStand() && livingEntity instanceof ArmorStand)
		{
			ArmorStand armorStand = (ArmorStand)livingEntity;
			
			return armorStand.getHelmet();
		}
		
		return null;
	}
	
	public static boolean setKit(LivingEntity livingEntity, ItemStack[] equipment)
	{
		EntityEquipment entityEquipment = livingEntity.getEquipment();
		
		if(entityEquipment != null)
		{
			entityEquipment.setHelmet(equipment[0]);
			entityEquipment.setChestplate(equipment[1]);
			entityEquipment.setLeggings(equipment[2]);
			entityEquipment.setBoots(equipment[3]);
			
			if(CheckMethod.existDoubleHand()) {
				entityEquipment.setItemInMainHand(equipment[4]);
				entityEquipment.setItemInOffHand(equipment[5]);
			} else {
				KitManager.setEntityItemInHand(entityEquipment, equipment[4]);
			}
		}
		else if(livingEntity instanceof Player)
		{
			PlayerInventory playerInv = ((Player)livingEntity).getInventory();
			
			if(playerInv == null)
				return false;
				
			playerInv.setHelmet(equipment[0]);
			playerInv.setChestplate(equipment[1]);
			playerInv.setLeggings(equipment[2]);
			playerInv.setBoots(equipment[3]);
			
			if(CheckMethod.existDoubleHand()) {
				playerInv.setItemInMainHand(equipment[4]);
				playerInv.setItemInOffHand(equipment[5]);
			} else {
				KitManager.setPlayerItemInHand(playerInv, equipment[4]);
			}
		}
		else if(CheckClass.existArmorStand() && livingEntity instanceof ArmorStand)
		{
			ArmorStand armorStand = (ArmorStand)livingEntity;
			
			armorStand.setHelmet(equipment[0]);
			armorStand.setChestplate(equipment[1]);
			armorStand.setLeggings(equipment[2]);
			armorStand.setBoots(equipment[3]);
			armorStand.setItemInHand(equipment[4]);
		}
		else
		{
			return false;
		}
		
		return true;
	}
	
	public static boolean setItemHand(LivingEntity livingEntity, ItemStack itemStack)
	{
		EntityEquipment entityEquipment = livingEntity.getEquipment();
		
		if(entityEquipment != null)
		{
			if(CheckMethod.existDoubleHand()) {
				entityEquipment.setItemInMainHand(itemStack);
			} else {
				KitManager.setEntityItemInHand(entityEquipment, itemStack);
			}
			return true;
		}
		else if(livingEntity instanceof Player)
		{
			PlayerInventory playerInv = ((Player)livingEntity).getInventory();
			
			if(playerInv == null)
				return false;
			
			if(CheckMethod.existDoubleHand()) {
				playerInv.setItemInMainHand(itemStack);
			} else {
				KitManager.setPlayerItemInHand(playerInv, itemStack);
			}
			return true;
		}
		else if(CheckClass.existArmorStand() && livingEntity instanceof ArmorStand)
		{
			ArmorStand armorStand = (ArmorStand)livingEntity;
			
			armorStand.setItemInHand(itemStack);
		}
		
		return false;
	}
	
	public static boolean setHelmet(LivingEntity livingEntity, ItemStack itemStack)
	{
		EntityEquipment entityEquipment = livingEntity.getEquipment();
		
		if(entityEquipment != null)
		{
			entityEquipment.setHelmet(itemStack);
			return true;
		}
		else if(livingEntity instanceof Player)
		{
			PlayerInventory playerInv = ((Player)livingEntity).getInventory();
			
			if(playerInv == null)
				return false;
			
			playerInv.setHelmet(itemStack);
			return true;
		}
		else if(CheckClass.existArmorStand() && livingEntity instanceof ArmorStand)
		{
			ArmorStand armorStand = (ArmorStand)livingEntity;
			
			armorStand.setHelmet(itemStack);
			return true;
		}
		
		return false;
	}
	
	@SuppressWarnings("deprecation")
	static private void setPlayerItemInHand(PlayerInventory playerInv, ItemStack itemStack) {
		playerInv.setItemInHand(itemStack);
	}
	
	@SuppressWarnings("deprecation")
	static private void setEntityItemInHand(EntityEquipment entityEquipment, ItemStack itemStack) {
		entityEquipment.setItemInHand(itemStack);
	}
	
	@SuppressWarnings("deprecation")
	static private ItemStack getPlayerItemInHand(PlayerInventory playerInv) {
		return playerInv.getItemInHand();
	}
	
	@SuppressWarnings("deprecation")
	static private ItemStack getEntityItemInHand(EntityEquipment entityEquipment) {
		return entityEquipment.getItemInHand();
	}
}
